<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Auth\LoginComponent;
use App\Http\Livewire\Auth\RegisterComponent;
use App\Http\Livewire\Auth\ResetPasswordComponent;
use App\Http\Livewire\Auth\ForgotPasswordComponent;
use App\Http\Livewire\Dashboard\DashboardComponent;
use App\Http\Livewire\Auth\ConfirmPasswordComponent;
use App\Http\Livewire\Dashboard\Administration\Roles\RoleComponent;
use App\Http\Livewire\Dashboard\Administration\User\UserComponent;
use App\Http\Livewire\Dashboard\Parametre\Chiffres\ChiffreComponent;
use App\Http\Livewire\Dashboard\Parametre\Services\ServiceComponent;
use App\Http\Livewire\Dashboard\Prereservation\Genre\GenreComponent;
use App\Http\Livewire\Dashboard\Prereservation\Ville\VilleComponent;
use App\Http\Livewire\Dashboard\Parametre\Societes\SocietesComponent;
use App\Http\Livewire\Dashboard\Prereservation\Demande\DemandeComponent;
use App\Http\Livewire\Dashboard\Parametre\AgentSimau\AgentSimauComponent;
use App\Http\Livewire\Dashboard\Parametre\Societes\SocietesEditeComponent;
use App\Http\Livewire\Dashboard\Prereservation\Civilite\CiviliteComponent;
use App\Http\Livewire\Dashboard\Prereservation\Localite\LocaliteComponent;
use App\Http\Livewire\Dashboard\Parametre\Societes\SocietesCreateComponent;
use App\Http\Livewire\Dashboard\Prereservation\TypePiece\TypePieceComponent;
use App\Http\Livewire\Dashboard\Configuration\AppelOffre\AppelOffreComponent;
use App\Http\Livewire\Dashboard\Configuration\Partenaire\PartenaireComponent;
use App\Http\Livewire\Dashboard\Prereservation\Departement\DepartementComponent;
use App\Http\Livewire\Dashboard\Configuration\AppelOffre\AppelOffreEditComponent;
use App\Http\Livewire\Dashboard\Configuration\Partenaire\PartenaireEditComponent;
use App\Http\Livewire\Dashboard\Configuration\SlideAccueil\SlideAccueilComponent;
use App\Http\Livewire\Dashboard\Prereservation\TypeLogement\TypeLogementComponent;
use App\Http\Livewire\Dashboard\Configuration\AppelOffre\AppelOffreCreateComponent;
use App\Http\Livewire\Dashboard\Configuration\Partenaire\PartenaireCreateComponent;
use App\Http\Livewire\Dashboard\Prereservation\StatutDemande\StatutDemandeComponent;
use App\Http\Livewire\Dashboard\Configuration\SlideAccueil\SlideAccueilEditeComponent;
use App\Http\Livewire\Dashboard\Prereservation\OptionLogement\OptionLogementComponent;
use App\Http\Livewire\Dashboard\Configuration\SlideAccueil\SlideAccueilCreateComponent;
use App\Http\Livewire\Dashboard\Configuration\ConseilAdministration\ConseilAdministrationComponent;
use App\Http\Livewire\Dashboard\Prereservation\SituationMatrimoniale\SituationMatrimonialeComponent;
use App\Http\Livewire\Dashboard\Configuration\ConseilAdministration\ConseilAdministrationEditComponent;
use App\Http\Livewire\Dashboard\Configuration\ConseilAdministration\ConseilAdministrationCreateComponent;
use App\Http\Livewire\Dashboard\D20000Logement\AvantagesOffres\AvantagesOffreComponent;
use App\Http\Livewire\Dashboard\D20000Logement\AvantagesOffres\AvantagesOffreCreateComponent;
use App\Http\Livewire\Dashboard\D20000Logement\AvantagesOffres\AvantagesOffreEditComponent;
use App\Http\Livewire\Dashboard\D20000Logement\FormulaireAcquisitions\FormulaireAcquisitionComponent;
use App\Http\Livewire\Dashboard\D20000Logement\FormulaireAcquisitions\FormulaireAcquisitionCreateComponent;
use App\Http\Livewire\Dashboard\D20000Logement\FormulaireAcquisitions\FormulaireAcquisitionEditComponent;
use App\Http\Livewire\Dashboard\Prereservation\CategorieProfessionnelle\CategorieProfessionnelleComponent;
use App\Http\Livewire\Dashboard\D20000Logement\LogementSociaux\LogementSociauxComponent;
use App\Models\AvantageOffert;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', WelcomeComponent::class)->name('welcome');

// Toutes les routes pour l'authentification
Route::get('/login', LoginComponent::class)->name('login');
Route::get('/register', RegisterComponent::class)->name('register');
Route::get('/user/confirmed-password-status', ConfirmPasswordComponent::class)->name('password.confirmation');
Route::get('/forgot-password', ForgotPasswordComponent::class)->name('password.request');
Route::get('/reset-password/{token}', ResetPasswordComponent::class)->name('password.reset');


Route::middleware(['auth:sanctum',config('jetstream.auth_session'),  'verified'])->group(function () {
    Route::get('/dashboard', DashboardComponent::class)->name('dashboard');

        Route::prefix('admin-config')->group(function () {
            Route::get('/listes-slides-accueil', SlideAccueilComponent::class)->name('admin.slideaccueil-index');
            Route::get('/modification-slides-accueil/{id}', SlideAccueilEditeComponent::class)->name('admin.slideaccueil-edit');
            Route::get('/ajouter-slides-accueil', SlideAccueilCreateComponent::class)->name('admin.slideaccueil-create');

            Route::get('/listes-partenaires', PartenaireComponent::class)->name('admin.partenaire-index');
            Route::get('/modification-partenaire/{id}', PartenaireEditComponent::class)->name('admin.partenaire-edit');
            Route::get('/ajouter-partenaire', PartenaireCreateComponent::class)->name('admin.partenaire-create');

            Route::get('/listes-conseils-administration', ConseilAdministrationComponent::class)->name('admin.conseilsadministration-index');
            Route::get('/modification-conseils-administration/{id}', ConseilAdministrationEditComponent::class)->name('admin.conseilsadministration-edit');
            Route::get('/ajouter-conseils-administration', ConseilAdministrationCreateComponent::class)->name('admin.conseilsadministration-create');

            Route::get('/listes-appels-offres', AppelOffreComponent::class)->name('admin.applesoffres-index');
            Route::get('/modification-appels-offres/{id}', AppelOffreEditComponent::class)->name('admin.applesoffres-edit');
            Route::get('/ajouter-appels-offres', AppelOffreCreateComponent::class)->name('admin.applesoffres-create');


        });
        Route::prefix('admin-para')->group(function () {
            Route::get('/listes-societes', SocietesComponent::class)->name('admin.societe-index');
            Route::get('/listes-societes', SocietesComponent::class)->name('admin.societe-index');
            Route::get('/modification-societe/{id}', SocietesEditeComponent::class)->name('admin.societe-edit');
            Route::get('/ajouter-societe', SocietesCreateComponent::class)->name('admin.societe-create');

            Route::get('/listes-services', ServiceComponent::class)->name('admin.service-index');
            Route::get('/listes-chiffres', ChiffreComponent::class)->name('admin.chiffre-index');
            Route::get('/listes-agents-simau', AgentSimauComponent::class)->name('admin.agent-index');



        });
        Route::prefix('admin-log')->group(function () {
            Route::get('/listes-logements-sociaux', LogementSociauxComponent::class)->name('admin.logementsociaux-index');
            Route::get('/listes-avantages-offerts', AvantagesOffreComponent::class)->name('admin.avantagesoffert-index');
            Route::get('/modification-avantages-offerts/{id}', AvantagesOffreEditComponent::class)->name('admin.avantagesoffert-edit');
            Route::get('/ajouter-avantages-offerts', AvantagesOffreCreateComponent::class)->name('admin.avantagesoffert-create');
            Route::get('/listes-formulaires-acquisitions', FormulaireAcquisitionComponent::class)->name('admin.formulaireacquisitions-index');
            Route::get('/modification-formulaire-acquisition/{id}', FormulaireAcquisitionEditComponent::class)->name('admin.formulaireacquisitions-edit');
            Route::get('/ajouter-formulaire-acquisition', FormulaireAcquisitionCreateComponent::class)->name('admin.formulaireacquisitions-create');



        });
        Route::prefix('admin-pre')->group(function () {
            Route::get('/listes-villes', VilleComponent::class)->name('admin.ville-index');
            Route::get('/listes-departements', DepartementComponent::class)->name('admin.departement-index');
            Route::get('/listes-categories-professionnelles', CategorieProfessionnelleComponent::class)->name('admin.categorieprofessionnelle-index');
            Route::get('/listes-types-pieces', TypePieceComponent::class)->name('admin.typepiece-index');
            Route::get('/listes-civilites', CiviliteComponent::class)->name('admin.civilite-index');
            Route::get('/listes-types-logements', TypeLogementComponent::class)->name('admin.typelogement-index');
            Route::get('/listes-situations-matrimoniales', SituationMatrimonialeComponent::class)->name('admin.situationmatrimoniale-index');
            Route::get('/listes-genres', GenreComponent::class)->name('admin.genre-index');
            Route::get('/listes-statuts-demandes', StatutDemandeComponent::class)->name('admin.statutdemande-index');
            Route::get('/listes-localites', LocaliteComponent::class)->name('admin.localite-index');
            Route::get('/listes-options-logement', OptionLogementComponent::class)->name('admin.optionlogement-index');
            Route::get('/listes-demandes', DemandeComponent::class)->name('admin.demande-index');








        });
        Route::prefix('administration')->group(function () {
            Route::get('/listes-utilisateurs', UserComponent::class)->name('admin.user-index');
            Route::get('/listes-roles', RoleComponent::class)->name('admin.role-index');


        });
});


