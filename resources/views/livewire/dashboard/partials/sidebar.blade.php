<!--sidebar wrapper -->
<div wire:ignore class="sidebar-wrapper" data-simplebar="true">
    <div class="sidebar-header">
        <div>
            <img src="{{ asset('assets/dash/images/logo_sbg.png') }}" class="logo-icon" alt="logo icon">
        </div>
        <div>
            <h5 class="logo-text">SIMAU</h5>
        </div>
        <div class="toggle-icon ms-auto"><i class='bx bx-arrow-to-left'></i>
        </div>
    </div>
    <!--navigation-->
    <ul class="metismenu" id="menu" wire:ignore>
        <li>
            <a href="{{ route('dashboard') }}" class="{{ Route::currentRouteName()== 'dashboard' ? 'active' : '' }}">
                <div class="parent-icon"><i class='bx bx-home-circle'></i>
                </div>
                <div class="menu-title">Dashboard</div>
            </a>
        </li>
        <li class="{{request()->route()->getPrefix() == '/admin-config' ? 'active' : '' }}">
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-folder'></i>
                </div>
                <div class="menu-title">Configurations</div>
            </a>
            <ul class="sidebar-submenu">
              <li><a href="{{route('admin.slideaccueil-index')}}" class="{{ Route::currentRouteName()== 'admin.slideaccueil-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Sildes Accueils</a></li>
              <li><a href="{{route('admin.partenaire-index')}}" class="{{ Route::currentRouteName()== 'admin.partenaire-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Partenaires</a></li>
              <li><a href="{{route('admin.conseilsadministration-index')}}" class="{{ Route::currentRouteName()== 'admin.conseilsadministration-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Conseils Administrations</a></li>
              <li><a href="{{route('admin.applesoffres-index')}}" class="{{ Route::currentRouteName()== 'admin.applesoffres-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Apples Offres</a></li>




            </ul>
        </li>
        <li class="{{request()->route()->getPrefix() == '/admin-para' ? 'active' : '' }}">
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-folder'></i>
                </div>
                <div class="menu-title">Paramètres</div>
            </a>
            <ul class="sidebar-submenu">
              <li><a href="{{route('admin.societe-index')}}" class="{{ Route::currentRouteName()== 'admin.societe-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Sociétés</a></li>
              <li><a href="{{route('admin.service-index')}}" class="{{ Route::currentRouteName()== 'admin.service-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Services</a></li>
              <li><a href="{{route('admin.chiffre-index')}}" class="{{ Route::currentRouteName()== 'admin.chiffre-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Chiffres</a></li>
              <li><a href="{{route('admin.agent-index')}}" class="{{ Route::currentRouteName()== 'admin.agent-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Agent SIMAU</a></li>




            </ul>
        </li>
        <li class="{{request()->route()->getPrefix() == '/admin-log' ? 'active' : '' }}">
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-folder'></i>
                </div>
                <div class="menu-title">20 000 Logements</div>
            </a>
            <ul class="sidebar-submenu">
              <li><a href="{{route('admin.logementsociaux-index')}}" class="{{ Route::currentRouteName()== 'admin.logementsociaux-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>logements sociaux</a></li>
              <li><a href="{{route('admin.avantagesoffert-index')}}" class="{{ Route::currentRouteName()== 'admin.avantagesoffert-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Avantages Offerts</a></li>
              <li><a href="{{route('admin.formulaireacquisitions-index')}}" class="{{ Route::currentRouteName()== 'admin.formulaireacquisitions-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Formulaires acquisitions</a></li>


            </ul>
        </li>
        <li class="{{request()->route()->getPrefix() == '/admin-pre' ? 'active' : '' }}">
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-folder'></i>
                </div>
                <div class="menu-title">Pré-reservation</div>
            </a>
            <ul class="sidebar-submenu">
              <li><a href="{{route('admin.ville-index')}}" class="{{ Route::currentRouteName()== 'admin.ville-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Villes</a></li>
              <li><a href="{{route('admin.departement-index')}}" class="{{ Route::currentRouteName()== 'admin.departement-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Departements</a></li>
              <li><a href="{{route('admin.categorieprofessionnelle-index')}}" class="{{ Route::currentRouteName()== 'admin.categorieprofessionnelle-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Catégories Pro</a></li>
              <li><a href="{{route('admin.typepiece-index')}}" class="{{ Route::currentRouteName()== 'admin.typepiece-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Types Piéces</a></li>
              <li><a href="{{route('admin.civilite-index')}}" class="{{ Route::currentRouteName()== 'admin.civilite-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Civilités</a></li>
              <li><a href="{{route('admin.typelogement-index')}}" class="{{ Route::currentRouteName()== 'admin.typelogement-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Types Logements</a></li>
              <li><a href="{{route('admin.genre-index')}}" class="{{ Route::currentRouteName()== 'admin.genre-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Genres</a></li>
              <li><a href="{{route('admin.statutdemande-index')}}" class="{{ Route::currentRouteName()== 'admin.statutdemande-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Statuts Demandes</a></li>
              <li><a href="{{route('admin.localite-index')}}" class="{{ Route::currentRouteName()== 'admin.localite-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Localités</a></li>
              <li><a href="{{route('admin.optionlogement-index')}}" class="{{ Route::currentRouteName()== 'admin.optionlogement-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Options Logements</a></li>
              <li><a href="{{route('admin.demande-index')}}" class="{{ Route::currentRouteName()== 'admin.demande-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Demandes</a></li>




            </ul>
        </li>
        <li class="{{request()->route()->getPrefix() == '/administration' ? 'active' : '' }}">
            <a href="javascript:;" class="has-arrow">
                <div class="parent-icon"><i class='bx bx-folder'></i>
                </div>
                <div class="menu-title">Administration</div>
            </a>
            <ul class="sidebar-submenu">
              <li><a href="{{route('admin.user-index')}}" class="{{ Route::currentRouteName()== 'admin.user-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Utilisateurs</a></li>
              <li><a href="{{route('admin.role-index')}}" class="{{ Route::currentRouteName()== 'admin.role-index' ? 'active' : '' }}"><i class="bx bx-right-arrow-alt"></i>Rôles</a></li>


            </ul>
        </li>
    </ul>
    <!--end navigation-->
</div>
<!--end sidebar wrapper -->
