
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                @if ($this->ville_id)
                <h5 class="modal-title">Modifier une ville</h5>
                @else
                <h5 class="modal-title">Ajouter une ville</h5>
                @endif
                <button type="button" class="btn-close btn-danger" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent='storeVille'>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
						<label for="validationCustom02" class="form-label">Libellé ville</label>
					    <input class="form-control mb-3" type="text" placeholder="Nom de la ville" wire:model.lazy='libelle'>
                    </div>
                    <div class="col-md-12" wire:ignore>
						<label class="form-label">Choisir le departement lié</label>
						<select  class="single-select" wire:model.lazy='departement_id' id="departement_id">
							<option value="">Veillez choisir un departement</option>
                            @foreach ($departements as $departement)
							<option value="{{ $departement->id }}">{{ $departement->Libelle }}</option>
                            @endforeach
						</select>
                    </div>
                </div>
            </div>
            <div class="modal-footer float-md-start">
                @if ($this->ville_id)
                <button type="submit" class="btn btn-outline-primary btn-sm">Modifier</button>
                @else
                <button type="submit" class="btn btn-outline-primary btn-sm">Ajouter</button>
                @endif
                <button type="button" wire:click.prevent='resetInputFields' class="btn btn-outline-danger btn-sm" data-bs-dismiss="modal">Annuler</button>
            </div>
            </form>
        </div>
    </div>
</div>
