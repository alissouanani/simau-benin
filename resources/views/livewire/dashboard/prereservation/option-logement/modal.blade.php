
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                @if ($this->option_logement_id)
                <h5 class="modal-title">Modifier une option de logement</h5>
                @else
                <h5 class="modal-title">Ajouter une option de logement</h5>
                @endif
                <button type="button" class="btn-close btn-danger" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent='storeOptionLogement'>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
						<label for="validationCustom02" class="form-label">Libellé de l'option de logement</label>
					    <input class="form-control mb-3" type="text" placeholder="Nom de la l'option de logement" wire:model.lazy='libelle'>
                    </div>
                    <div class="col-md-12" wire:ignore>
						<label class="form-label">Choisir un type de logement</label>
						<select  class="single-select" wire:model.lazy='type_logement_id' id="type_logement_id">
							<option value="">Veillez choisir une option de logement</option>
                            @foreach ($typeLogements as $typeLogement)
							<option value="{{ $typeLogement->id }}">{{ $typeLogement->libelle }}</option>
                            @endforeach
						</select>
                    </div>
                </div>
            </div>
            <div class="modal-footer float-md-start">
                @if ($this->option_logement_id)
                <button type="submit" class="btn btn-outline-primary btn-sm">Modifier</button>
                @else
                <button type="submit" class="btn btn-outline-primary btn-sm">Ajouter</button>
                @endif
                <button type="button" wire:click.prevent='resetInputFields' class="btn btn-outline-danger btn-sm" data-bs-dismiss="modal">Annuler</button>
            </div>
            </form>
        </div>
    </div>
</div>
