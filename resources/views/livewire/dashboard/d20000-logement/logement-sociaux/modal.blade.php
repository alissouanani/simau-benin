
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                @if ($this->logement_sociaux_id)
                <h5 class="modal-title">Modifier un logement social</h5>
                @else
                <h5 class="modal-title">Ajouter un logement social</h5>
                @endif
                <button type="button" class="btn-close btn-danger" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent='storeLogementSociaux'>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label class="form-label" for="image">Image</label>:</label>
                        <input class="form-control mb-3" id="image" type="file" accept=".jpg, .png, image/jpeg, image/png" wire:model="image">
                        @if ($this->logement_sociaux_id)

                        @else
                        @if ($image)
                        <img src="{{$image->temporaryUrl()}}" class="px-2" width="100">
                        @endif
                        @endif
                    </div>
                    <div wire:ignore class="col-md-12 mt-3">
                        <label class="form-label" for="description">Description</label>:</label>
                        <textarea id="mytextarea" name="mytextarea" wire:model="description">Hello, World!</textarea>

                    </div>
                </div>
            </div>
            <div class="modal-footer float-md-start">
                @if ($this->logement_sociaux_id)
                <button type="submit" class="btn btn-outline-primary btn-sm">Modifier</button>
                @else
                <button type="submit" class="btn btn-outline-primary btn-sm">Ajouter</button>
                @endif
                <button type="button" wire:click.prevent='resetInputFields' class="btn btn-outline-danger btn-sm" data-bs-dismiss="modal">Annuler</button>
            </div>
            </form>
        </div>
    </div>
</div>
