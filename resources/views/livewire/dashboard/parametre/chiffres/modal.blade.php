
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                @if ($this->chiffre_id)
                <h5 class="modal-title">Modifier un chiffre</h5>
                @else
                <h5 class="modal-title">Ajouter une chiffre</h5>
                @endif
                <button type="button" class="btn-close btn-danger" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent='storeChiffre'>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
						<label for="validationCustom02" class="form-label">Chiffres</label>
					    <input class="form-control mb-3" type="text" placeholder="chiffre" wire:model.lazy='chiffre'>
                    </div>
                    <div wire:ignore class="col-md-12 mt-3">
                        <label class="form-label" for="image">Description</label>:</label>
                        <textarea id="mytextarea" name="mytextarea" wire:model="description">Hello, World!</textarea>

                    </div>
                </div>
            </div>
            <div class="modal-footer float-md-start">
                @if ($this->chiffre_id)
                <button type="submit" class="btn btn-outline-primary btn-sm">Modifier</button>
                @else
                <button type="submit" class="btn btn-outline-primary btn-sm">Ajouter</button>
                @endif
                <button type="button" wire:click.prevent='resetInputFields' class="btn btn-outline-danger btn-sm" data-bs-dismiss="modal">Annuler</button>
            </div>
            </form>
        </div>
    </div>
</div>
