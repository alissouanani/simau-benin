
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                @if ($this->agent_id)
                <h5 class="modal-title">Modifier {{ $this->name }}</h5>
                @else
                <h5 class="modal-title">Ajouter un agent</h5>
                @endif
                <button type="button" class="btn-close btn-danger" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent='storeAgent'>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
						<label for="validationCustom02" class="form-label">Nom de l'agent</label>
					    <input class="form-control mb-3" type="name" placeholder="le nom de l'agent" wire:model.lazy='name'>
                    </div>
                    <div class="col-md-12">
						<label for="validationCustom02" class="form-label">Adresse Mail</label>
					    <input class="form-control mb-3" type="email" placeholder="Votre mail" wire:model.lazy='email'>
                    </div>
                </div>
            </div>
            <div class="modal-footer float-md-start">
                @if ($this->agent_id)
                <button type="submit" class="btn btn-outline-primary btn-sm">Modifier</button>
                @else
                <button type="submit" class="btn btn-outline-primary btn-sm">Ajouter</button>
                @endif
                <button type="button" wire:click.prevent='resetInputFields' class="btn btn-outline-danger btn-sm" data-bs-dismiss="modal">Annuler</button>
            </div>
            </form>
        </div>
    </div>
</div>
