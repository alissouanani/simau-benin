
<!-- Modal -->
<div wire:ignore.self class="modal fade" id="exampleVerticallycenteredModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                @if ($this->service_id)
                <h5 class="modal-title">Modifier un service</h5>
                @else
                <h5 class="modal-title">Ajouter une service</h5>
                @endif
                <button type="button" class="btn-close btn-danger" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form wire:submit.prevent='storeService'>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label class="form-label" for="icon">Icon</label>:</label>
                        <input class="form-control mb-3" id="icon" type="file" accept=".jpg, .png, image/jpeg, image/png" wire:model="icon">
                        @if ($icon)
                        <img src="{{$icon->temporaryUrl()}}" class="px-2" width="100">
                        @endif
                    </div>
                    <div class="col-md-12">
						<label for="validationCustom02" class="form-label">libelle</label>
					    <input class="form-control mb-3" type="text" placeholder=" libelle" wire:model.lazy='libelle'>
                    </div>
                    <div wire:ignore class="col-md-12 mt-3">
                        <label class="form-label" for="image">Description</label>:</label>
                        <textarea id="mytextarea" name="mytextarea" wire:model="description">Hello, World!</textarea>

                    </div>
                </div>
            </div>
            <div class="modal-footer float-md-start">
                @if ($this->service_id)
                <button type="submit" class="btn btn-outline-primary btn-sm">Modifier</button>
                @else
                <button type="submit" class="btn btn-outline-primary btn-sm">Ajouter</button>
                @endif
                <button type="button" wire:click.prevent='resetInputFields' class="btn btn-outline-danger btn-sm" data-bs-dismiss="modal">Annuler</button>
            </div>
            </form>
        </div>
    </div>
</div>
