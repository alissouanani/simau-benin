@section('styles')
<link href="{{ asset('assets/dash/plugins/datatable/css/dataTables.bootstrap5.min.css') }}" rel="stylesheet" />
@endsection
<div>
    <!--start page wrapper -->
		<div wire:ignore class="page-wrapper">
			<div wire:ignore class="page-content">
				<!--breadcrumb-->
				<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
					<div class="breadcrumb-title pe-3">Appels Offres</div>
					<div class="ps-3">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb mb-0 p-0">
								<li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="bx bx-home-alt"></i></a></li>
								<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Configuration</a></li>
								<li class="breadcrumb-item active" aria-current="page">Listes des Appels Offres</li>
							</ol>
						</nav>
					</div>
					<div class="ms-auto">
						<div class="btn-group">
							<a href="{{ route('admin.applesoffres-create') }}" class="btn btn-outline-primary">Ajouter</a>
						</div>
					</div>
				</div>
				<!--end breadcrumb-->
				<h6 class="mb-0 text-uppercase">Liste des Appels Offres </h6>
				<hr/>
				<div class="card">
					<div class="card-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success">{{Session::get('message')}}</div>
                        @endif
						<div wire:ignore class="table-responsive">
							<table id="example" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>ID</th>
										<th>Fichier</th>
										<th>Titre</th>
										<th>Date de Debut</th>
										<th>Date de Fin</th>
                                        <th>Date d'activitée</th>
										<th>Date de clôtures</th>
                                        {{-- <th>Activites</th> --}}
                                        <th>Statuts</th>
										<th>Descriptions</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach ($appelOffres as $appelOffre)
                                    <tr>
										<td>{{ $i++ }}</td>
										<td>
                                            @empty($appelOffre->file)
                                            Pas de fichier
                                            @else
                                            Télécharger le fichier<a href="{{asset('storage/PDFAppelOffres')}}/{{$appelOffre->file}}"> ICI</a>
                                            @endempty
                                        </td>
										<td class="text-wrap">{{ $appelOffre->titre }}</td>

										<td>{{ $appelOffre->dateDebut }}</td>

										<td>{{ $appelOffre->dateFin }}</td>

										<td>{{ $appelOffre->dateActivite }}</td>

                                        <td>{{ $appelOffre->dateCloture }}</td>

										{{-- <td>{{ $appelOffre->activite }}</td> --}}

										<td>{{ $appelOffre->statut }}</td>

										<td class="text-wrap">{!! $appelOffre->description !!}</td>

                                        <td>
                                            <a style="margin-right: 12px; margin-left:15px" href="{{route('admin.applesoffres-edit',['id' => $appelOffre->id])}}" data-bs-toggle="tooltip" data-bs-placement="top" title="Modifier"> <i class="lni lni-pencil-alt text-primary"></i> </a>
                                            <a data-bs-toggle="tooltip" data-bs-placement="top" title="Supprimer" wire:click.prevent="deleteAppelOffre({{ $appelOffre->id }})"> <i class="lni lni-trash text-danger"></i> </a>
                                        </td>
									</tr>
                                    @endforeach
                                </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--end page wrapper -->
</div>

@section('scripts')
<script src="{{ asset('assets/dash/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/dash/plugins/datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dash/plugins/datatable/js/dataTables.bootstrap5.min.js') }}"></script>

	<script>
		$(document).ready(function() {
			$('#example').DataTable();
		  } );
	</script>
	<script>
		$(document).ready(function() {
			var table = $('#example2').DataTable( {
				lengthChange: false,
				buttons: [ 'copy', 'excel', 'pdf', 'print']
			} );

			table.buttons().container()
				.appendTo( '#example2_wrapper .col-md-6:eq(0)' );
		} );
	</script>
	<script>
		$('.single-select').select2({
			theme: 'bootstrap4',
			width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
			placeholder: $(this).data('placeholder'),
			allowClear: Boolean($(this).data('allow-clear')),
		});
		$('.multiple-select').select2({
			theme: 'bootstrap4',
			width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
			placeholder: $(this).data('placeholder'),
			allowClear: Boolean($(this).data('allow-clear')),
		});
	</script>
    <script>
        $(function () {
            $('[data-bs-toggle="popover"]').popover();
            $('[data-bs-toggle="tooltip"]').tooltip();
        })
    </script>

@endsection

