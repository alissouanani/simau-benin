<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieTypeLogementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorietypelogements')->insert([

            [
                'libelle'=>"Logements collectifs",
                'isDelete'=> 0,
            ],
            [
                'libelle'=>"Logements individuels",
                'isDelete'=> 0,
            ],

        ]);
    }
}
