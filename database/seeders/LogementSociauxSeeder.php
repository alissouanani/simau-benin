<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LogementSociauxSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logementsociauxs')->insert([

            [
                'description' => '
                <p>
                Les logements sociaux sont proposés en premier logement aux ménages à revenus intermédiaires. Sur ce programme la typologie D de logement individuel basique est de type social avec une surface habitable de 71.50 m² et une parcelle de 123,6 m².
                </p>
                <p>
                Aussi, l’Etat béninois subventionne une partie du Programme notamment : le coût du foncier et de la viabilisation ainsi que les frais financiers correspondant aux délais de construction.
                </p>
                <div >
                <p>
                Spécialement pour les logements de type D, l’Etat subventionne en plus du foncier et de la viabilisation primaire, toutes les autres charges, de sorte que l\'acquéreur ne paye que le coût de construction du logement (Génie-Civil).
                </p>
                <p>Ainsi, pour le logement D, l’etat prend en charge :</p>
                <ul>
                    <li>le foncier </li>
                    <li>les études techniques et contrôle des travaux  </li>
                    <li>la viabilisation primaire </li>
                    <li>la viabilisation secondaire et tertiaire  </li>
                    
                </ul>
                </div>
                ',
                'image'=>"upload/about-about.jpg",
                'isDelete'=> 0,
            ],

        ]);
    }
}
