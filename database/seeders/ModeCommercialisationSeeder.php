<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModeCommercialisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modecommercialisations')->insert([

            [
                'titre'=>"Modes de commercialisation des logements sociaux de type D",
                'photo'=>"upload/26.jpg",
                'titreDescription' => "
                    <p>
                    L’unique option retenue pour la commercialisation des logements sociaux de type D est la  location-accession avec ou sans apport initial.
                    </p><br>
                    <p>
                    L’objectif principal de cette option est de permettre aux acquéreurs de devenir propriétaire en payant le montant d’un loyer appelé redevance sur une durée maximale de vingt-cinq (25) ans.
                    </p><br>
                    <p>Le processus de la location-accession est résumé comme suit :
                    </p><br>
                    <ul>
                        <li>
                        Le Candidat intéressé par le Programme remplira une fiche de pré-réservation de logements (à déposer à la SImAU ou inscription en ligne).
                        </li>
                        <li>
                            Le Candidat sélectionné sur la base des critères du Programme sera invité à fournir des pièces pour la constitution d’un dossier accompagné des frais de réservation d’un montant non remboursable de Cinquante Mille (50.000) francs CFA.
                        </li>
                        <li>
                            Après analyse du dossier du Candidat avec satisfaction, il sera invité à signer un contrat préliminaire qui devra comporter toutes les mentions prescrites par la loi.
                        </li>
                        <li>
                        Suivront les phases d’établissement et de signature du contrat de location-accession.
                        </li>
                        <li>
                            Interviendra la phase de réception des clés du logement après paiement de la 1ière redevance et du montant mensuel des charges.
                        </li>
                        <li>
                        En cas de désistement volontaire, le logement est libéré. Il devient disponible pour un nouvel acquéreur.
                        </li>
                        <li>
                            En cas de défaillance du locataire accédant, la SImAU engage à son encontre une procédure d’expulsion et attribue le logement à un nouveau Candidat après remise en état.
                        </li>

                    </ul>
                    <p>
                    <span><strong>NB : </strong></span>L’option d’achat peut être levée à tout moment par le locataire par paiement au comptant du capital restant dû à la date de référence.
                    </p>
                ",

                'titre1'=>"Modes de commercialisation des logements économiques (types A, B, C et E)",
                "description1"=>"
               <p>
              Deux (02) options sont retenues pour la commercialisation des logements économiques de type A, B, C et E à savoir la location accession et la vente directe.
              </p>
              <h5 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;1er Cas : La location accession avec ou sans apport initial</h5>
              <p>
              L’objectif principal de cette option est de permettre aux acquéreurs de devenir propriétaire en payant le montant d’un loyer appelé redevance sur une durée maximale de dix-sept (17) ans. <br>Le processus de la location-accession est résumé comme suit :
              </p>
              <p>
                <ul>
                  <li>Le Candidat intéressé par le Programme remplira une fiche de pré-réservation de logements (à déposer à la SImAU ou inscription en ligne).</li>
                  <li>Le Candidat sélectionné sur la base des critères du Programme sera invité à fournir des pièces pour la constitution d’un dossier accompagné des frais de réservation d’un montant non remboursable de Cent Mille (100.000) francs CFA.</li>
                  <li>Après analyse du dossier du Candidat avec satisfaction, il sera invité à signer un contrat préliminaire qui devra comporter toutes les mentions prescrites par la loi.</li>
                  <li>Suivront les phases d’établissement et de signature du contrat de location-accession.</li>
                  <li>Interviendra la phase de réception des clés du logement après paiement de la 1ière redevance et du montant mensuel des charges.</li>
                  <li>En cas de désistement volontaire, le logement est libéré. Il devient disponible pour un nouvel acquéreur.</li>
                  <li>En cas de défaillance du locataire accédant, la SImAU engage à son encontre une procédure d’expulsion et attribue le logement à un nouveau Candidat après remise en état.</li>
                </ul>
              </p>
              <p class=\"has-text-simau-green\">NB : L’option d’achat peut être levée à tout moment par le locataire par paiement au comptant du capital restant dû à la date de référence.</p>
              <br>

                ",
                'titre2'=>"2ème Cas : La vente directe",
                "description2"=>"
              <p>
                Elle intervient dans le cadre d’une acquisition au comptant sur les ressources du candidat. <br>Dans ce cas, le processus d’acquisition est le suivant :
              </p>
              <p>
                <ul>
                  <li>Le Candidat intéressé par le Programme remplira une fiche de pré-réservation de logements (inscription en ligne).</li>
                  <li>Le Candidat sélectionné sur la base des critères du Programme sera invité à fournir des pièces pour la constitution d’un dossier accompagné des frais de réservation d’un montant non remboursable de Cent Mille (100.000) francs CFA.</li>
                  <li>Après analyse du dossier du Candidat avec satisfaction, il sera invité à signer d’abord un contrat de réservation, puis un contrat de vente clés en main à la livraison du logement.</li>
                </ul>
              </p>
              <br>
                ",
                'titre3'=>"Autres conditions et modalités pour la commercialisation",
                'description3'=>"

              <p>
                <ul>
                  <li>Adhésion des locataires et des acquéreurs au Règlement Général de copropriété prévue pour le programme</li>
                  <li>Dépôt de garantie de 3 mois pour les contrats de location accession à l’exception des contrats conclus par les fonctionnaires et salariés.</li>
                  <li>Droits d’enregistrement gratuits sur tous les actes conclus dans le cadre du Programme.</li>
                  <li>Emoluments des Notaires fixés à la somme forfaitaire de Cinquante Mille (50 000) FCFA par acte</li>
                </ul>
              </p>
                ",
                'image2'=>"upload/26.jpg",
                'isDelete'=> 0,
            ],

        ]);
    }
}
