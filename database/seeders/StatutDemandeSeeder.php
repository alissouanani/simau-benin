<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatutDemandeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statut_demandes')->insert([

            [
                'libelle' => 'En attente',
            ],
            [
                'libelle' => 'En cours',
            ],
            [
                'libelle' => 'Traitée',
            ],


        ]);
    }
}
