<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DepartementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departements')->insert([

            [
                'Libelle' => 'ATLANTIQUE',
                'pay_id' => 1,
            ],

            [
                'Libelle' => 'LITTORAL',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'OUEME',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'PLATEAU',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'ZOU',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'COLLINES',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'MONO',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'COUFFO',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'ATACORA',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'DONGA',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'BORGOU',
                'pay_id' => 1,
            ],
            [
                'Libelle' => 'ALIBORI',
                'pay_id' => 1,
            ],



        ]);
    }
}
