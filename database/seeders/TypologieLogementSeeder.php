<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypologieLogementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('typologielogements')->insert([

            [
                'grandTitre'=>"Types de logement",
                'fiche'=>"upload/Fiche_PLANSAPPARTEMENTTypeAetPointdeOFFRE20 000LogementsActualiséeau.pdf",
                'video'=>"pdJxe_T7i9o",
                'grandDescription' => "
                Les typologies des logements qui sont proposées dans le projet « 20 000 logements » ont été étudiés pour répondre aux besoins identifiés lors des études d’opportunité. Tous les logements sont de type F4 c’est-à-dire, un appartement familial de quatre pièces (3 chambres, séjour). Il permet de loger une famille avec un ou plusieurs enfants en fonction du nombre de chambres. Les superficies des logements répondent aux besoins des futurs occupants et de leur mode de vie, qu’il s’agisse d’un appartement ou d’une villa individuelle. La SImAU a ainsi réalisé une classification dans six (6) types de logements à savoir :
                ",
                'petitTitre'=>"Type A : R+3 (8 appartements avec une surface habitable de 81.26 m²)",
                'petitDescription'=>"8 appartements avec une surface habitable de 81.26m²",
                'photo'=>"upload/type_A.PNG",
                'categorietypelogement_id'=>1,
                'isDelete'=> 0,
            ],
            [
                'grandTitre'=>"Les types de logement",
                'fiche'=>"upload/FichePLANSAPPARTEMENTTypeBetPointdeOFFRE20 000LogementsActualiséeau.pdf",
                'video'=>"Zi5lFT1oUzE",
                'grandDescription' => "
                Les typologies des logements qui sont proposées dans le projet « 20 000 logements » ont été étudiés pour répondre aux besoins identifiés lors des études d’opportunité. Tous les logements sont de type F4 c’est-à-dire, un appartement familial de quatre pièces (3 chambres, séjour). Il permet de loger une famille avec un ou plusieurs enfants en fonction du nombre de chambres. Les superficies des logements répondent aux besoins des futurs occupants et de leur mode de vie, qu’il s’agisse d’un appartement ou d’une villa individuelle. La SImAU a ainsi réalisé une classification dans six (6) types de logements à savoir :
                ",
                'petitTitre'=>"Type B : R+3 (16 appartements avec une surface habitable de 91.17 m²)",
                'petitDescription'=>"8 appartements avec une surface habitable de 81.26m²",
                'photo'=>"upload/type_B.PNG",
                'categorietypelogement_id'=>1,
                'isDelete'=> 0,
            ],
            [
                'grandTitre'=>"Les types de logement",
                'fiche'=>"upload/FichePLANSAPPARTEMENTTypeC2etPointdeOFFRE20000LogementsActualiseeau.pdf",
                'video'=>"33RIn1CNzgg",
                'grandDescription' => "
                Les typologies des logements qui sont proposées dans le projet « 20 000 logements » ont été étudiés pour répondre aux besoins identifiés lors des études d’opportunité. Tous les logements sont de type F4 c’est-à-dire, un appartement familial de quatre pièces (3 chambres, séjour). Il permet de loger une famille avec un ou plusieurs enfants en fonction du nombre de chambres. Les superficies des logements répondent aux besoins des futurs occupants et de leur mode de vie, qu’il s’agisse d’un appartement ou d’une villa individuelle. La SImAU a ainsi réalisé une classification dans six (6) types de logements à savoir :
                ",
                'petitTitre'=>"Type C2 : R+2 (6 appartements avec une surface habitable de 83.98 m²)",
                'petitDescription'=>"8 appartements avec une surface habitable de 81.26m²",
                'photo'=>"upload/type_C2.jpg",
                'categorietypelogement_id'=>1,
                'isDelete'=> 0,
            ],
            [
                'grandTitre'=>"Les types de logement",
                'fiche'=>"upload/FichePLANSAPPARTEMENTTypeC3etPointdeOFFRE20000LogementsActualiseeau.pdf",
                'video'=>"PDc7FbQQJ1I",
                'grandDescription' => "
                Les typologies des logements qui sont proposées dans le projet « 20 000 logements » ont été étudiés pour répondre aux besoins identifiés lors des études d’opportunité. Tous les logements sont de type F4 c’est-à-dire, un appartement familial de quatre pièces (3 chambres, séjour). Il permet de loger une famille avec un ou plusieurs enfants en fonction du nombre de chambres. Les superficies des logements répondent aux besoins des futurs occupants et de leur mode de vie, qu’il s’agisse d’un appartement ou d’une villa individuelle. La SImAU a ainsi réalisé une classification dans six (6) types de logements à savoir :
                ",
                'petitTitre'=>"Type C3 : R+3 (8 appartements avec une surface habitable de 83.98 m²)",
                'petitDescription'=>"8 appartements avec une surface habitable de 81.26m²",
                'photo'=>"upload/type_C3.PNG",
                'categorietypelogement_id'=>1,
                'isDelete'=> 0,
            ],
            [
                'grandTitre'=>"Les types de logement",
                'fiche'=>"upload/FichePLANSVILLATypeDetPointdeOFFRE20000LogementsActualiseeau.pdf",
                'video'=>"EMsXjziCVt8",
                'grandDescription' => "
                Les typologies des logements qui sont proposées dans le projet « 20 000 logements » ont été étudiés pour répondre aux besoins identifiés lors des études d’opportunité. Tous les logements sont de type F4 c’est-à-dire, un appartement familial de quatre pièces (3 chambres, séjour). Il permet de loger une famille avec un ou plusieurs enfants en fonction du nombre de chambres. Les superficies des logements répondent aux besoins des futurs occupants et de leur mode de vie, qu’il s’agisse d’un appartement ou d’une villa individuelle. La SImAU a ainsi réalisé une classification dans six (6) types de logements à savoir :
                ",
                'petitTitre'=>"Type D",
                'petitDescription'=>"Type D : logement individuel basique de type social avec une surface habitable de 71.50 m² et une parcelle de 123,6 m²)",
                'photo'=>"assets/images/work4.jpg",
                'categorietypelogement_id'=>2,
                'isDelete'=> 0,
            ],
            [
                'grandTitre'=>"Les types de logement    ",
                'fiche'=>"upload/FichePLANSVILLATypeEetPointdeOFFRE20000LogementsActualiseeau.pdf",
                'video'=>"DMU65Jvm8Ls",
                'grandDescription' => "
                Les typologies des logements qui sont proposées dans le projet « 20 000 logements » ont été étudiés pour répondre aux besoins identifiés lors des études d’opportunité. Tous les logements sont de type F4 c’est-à-dire, un appartement familial de quatre pièces (3 chambres, séjour). Il permet de loger une famille avec un ou plusieurs enfants en fonction du nombre de chambres. Les superficies des logements répondent aux besoins des futurs occupants et de leur mode de vie, qu’il s’agisse d’un appartement ou d’une villa individuelle. La SImAU a ainsi réalisé une classification dans six (6) types de logements à savoir :
                ",
                'petitTitre'=>"Type E",
                'petitDescription'=>"Type E : logement individuel avec dépendances et garage, de type économique, d’une surface habitable de 102.65 m² et d’une parcelle de 264m²",
                'photo'=>"assets/images/work2compresser.jpg",
                'categorietypelogement_id'=>2,
                'isDelete'=> 0,
            ],

        ]);
    }
}
