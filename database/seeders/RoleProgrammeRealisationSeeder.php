<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleProgrammeRealisationSeeder extends Seeder
{
    public $listeRolesWithPermissions = [
        'ProgrammeRealisation' => [
            'permissions' => [
                'index_programme_realisation',
                'create_programme_realisation',
                'update_programme_realisation',
                'delete_programme_realisation',
                'index_societe',
            ]
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->listeRolesWithPermissions as $key => $value) {
            $role =  Role::create(['name' => $key, 'libelle' => 'Programme réalisation']);
            foreach ($value['permissions'] as  $permission) {
                $permission = Permission::where('name', $permission)->first();
                $role->givePermissionTo($permission);
            }
        }
    }
}
