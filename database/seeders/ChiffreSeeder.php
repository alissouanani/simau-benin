<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ChiffreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('chiffres')->insert([

            [
                'chiffre' => '5',
                'description' => 'Grands projets du PAG 2016-2021 en cours de réalisation',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '3 000',
                'description' => 'Emplois directs créés ',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '50',
                'description' => 'Partenaires de référence dans le monde',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '8 000',
                'description' => 'Logements sociaux et économiques en cours de construction',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '5',
                'description' => 'Grands projets du PAG 2016-2021 en cours de réalisation',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '3 000',
                'description' => 'Emplois directs créés ',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '50',
                'description' => 'Partenaires de référence dans le monde',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '8 000',
                'description' => 'Logements sociaux et économiques en cours de construction',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '5',
                'description' => 'Grands projets du PAG 2016-2021 en cours de réalisation',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '3 000',
                'description' => 'Emplois directs créés ',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '50',
                'description' => 'Partenaires de référence dans le monde',
                'isDelete' => 0,
            ],
            [
                'chiffre' => '8 000',
                'description' => 'Logements sociaux et économiques en cours de construction',
                'isDelete' => 0,
            ],

        ]);
    }
}
