<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Database\Seeders\ChiffreSeeder;
use Database\Seeders\ServiceSeeder;
use Database\Seeders\SocieteSeeder;
use Database\Seeders\ActualiteSeeder;
use Database\Seeders\UserTableSeeder;
use Database\Seeders\CategoriesAppelSeeder;
use Database\Seeders\CategorieActualiteSeeder;
use App\Http\Controllers\PaiementDAOController;
use Database\Seeders\CategorieActionnaireSeeder;
use Database\Seeders\CategorieTypeLogementSeeder;
use Database\Seeders\CategorieAdministrationSeeder;
use Database\Seeders\CategorieProjetStandardSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CategorieAdministrationSeeder::class);
        // $this->call(PermissionSeeder::class);
        $this->call(CategoriesAppelSeeder::class);
        $this->call(CategorieActionnaireSeeder::class);
        $this->call(CategorieProjetStandardSeeder::class);
        $this->call(CategorieActualiteSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ChiffreSeeder::class);
        $this->call(CategorieTypeLogementSeeder::class);
        $this->call(SocieteSeeder::class);
        $this->call(Slide_accueilSeeder::class);
        // $this->call(ActualiteSeeder::class);
        $this->call(PartenaireSeeder::class);
        $this->call(ProgrammeRealisationSeeder::class);
        $this->call(RecrutementSeeder::class);
        $this->call(Appel_offreSeeder::class);
        $this->call(Projet_standardSeeder::class);
        $this->call(ConseilAdministrationSeeder::class);
        //$this->call(CategoriesFaitTableSeeder::class);
        $this->call(LocaliteSeeder::class);
        $this->call(ActiviteTableSeeder::class);
        $this->call(PaysDepartementsVilleSeeder::class);
        $this->call(TypePieceSeeder::class);
        //$this->call(DepartementSeeder::class);
        //$this->call(VilleSeeder::class);
        $this->call(GenreSeeder::class);
        $this->call(ActionnaireSeeder::class);
        $this->call(SituationMatrimonialSeeder::class);
        $this->call(CiviliteSeeder::class);
        $this->call(StatutDemandeSeeder::class);
        $this->call(TypeLogementSeeder::class);
        $this->call(CategorieProfessionSeeder::class);
        $this->call(ProfessionSeeder::class);
        $this->call(ModePaiementSeeder::class);
        $this->call(OptionLogementSeeder::class);
        $this->call(SourcePaiementSeeder::class);
        $this->call(AvantageOffertSeeder::class);
        $this->call(FormulaireAcquisitionSeeder::class);
        $this->call(LogementSociauxSeeder::class);
        $this->call(ModeCommercialisationSeeder::class);
        $this->call(TypologieLogementSeeder::class);
        $this->call(ProgrammeNationalSeeder::class);
        $this->call(PresentationProjetSeeder::class);
        // $this->call(AdminSeeder::class);
        // $this->call(RoleActualiteSeeder::class);
        // $this->call(RoleAppelOffreSeeder::class);
        // $this->call(RoleConseilAdministrationSeeder::class);
        // $this->call(RolePartenaireSeeder::class);
        // $this->call(RoleProgrammeRealisationSeeder::class);
        // $this->call(RoleRecrutementSeeder::class);
        // $this->call(RoleSlideAcceuilSeeder::class);
        // $this->call(AgentSeeder::class);
        $this->call(DemandeurSeeder::class);
        $this->call(DemandeSeeder::class);
        $this->call(TypeLogementDemandeSeeder::class);
        //$this->call(PaiementDAOController::class);
    }
}
