<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Projet_standardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projet_standards')->insert([

            [
                'slides' => 'upload/programme_logement1.PNG',
                'titre' => 'Programme logement',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => '
                <p>Dans le cadre de la réalisation de son <a href="https://beninrevele.bj" target="_blank">Plan d’Actions du Gouvernement (PAG) 2016-2021</a> , le Gouvernement du Bénin a prévu au pilier 3 « améliorer les conditions de vie des populations » et sur son axe 7 : « Développement équilibré et durable de l’espace national », des actions d’amélioration du cadre de vie, le bien-être de tous et de préserver l’environnement avec le développement de programmes immobiliers d’habitat.</p><p>De récentes études ont révélé un besoin de 320 000 logements au Bénin sur la période allant de 2010 à 2020.&nbsp;</p><p>Pour relever ce défi d’accès au logement, la SImAU entend accompagner le gouvernement dans le développement et la réalisation des programmes immobiliers d’habitat mais compte également développer ses propres projets résidentiels.&nbsp;</p><p><br></p>
                <i class="fas fa-chevron-right text-simau-red"></i><h5 class="mt-2 text-simau-green">Nos Projets&nbsp;</h5><br>
                <h6 class="text-simau-green"><i class="fas fa-arrow-right text-simau-red"></i>&nbsp;&nbsp;<a style="text-decoration: none;" href="/presentation_projet">Programme 20 000 logements&nbsp;</a></h6><br>
                <h6 class="text-simau-green"><i class="fas fa-arrow-right text-simau-red"></i>&nbsp;&nbsp;<a style="text-decoration: none;" href="#">Résidences militaires (Allada, Ouidah, Parakou)</a></h6><br>
                ',
                /* 'description' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.
                ", */
                'chefProjet' => 'XXXXX',
                //'maitreOuvrage' => 'SimAU Bénin',
                'coutProjet' => 'Huit (08) Milliards FCFA',
                'zone_intervention' => 'Allada, Ouidah et Parakou',
                //'titre_cadre_intitutionnel' => 'Maître d’ouvrage (MOA)',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel3":"Cabinet d’architecture i-Concept"},{"titre_cadre_intitutionnel4":"Entreprises","description_cadre_intitutionnel4":"Groupement d’entreprises SP Construction-Dyjesck- Comtel"},{"titre_cadre_intitutionnel5":"Autres intervenants ","description_cadre_intitutionnel5":"SBBE, SONEB, Bénin Télécom, Mairies d’Allada de Ouidah et de Parakou "},{"titre_cadre_intitutionnel6":null,"description_cadre_intitutionnel6":null},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => 'upload/FicheConstructionde_500_logementssociauxdetypeF3pourlesmilitaires-converti.pdf',
                'images' => '["upload/28.jpg","upload/29.jpg"]',
                'images_acceuil' =>  'upload/programmemogement001.jpg',
                'video' =>  '[["Construction des logements \u00e9conomiques sociaux de Ou\u00e8do: Du r\u00eave a la r\u00e9alit\u00e9","OD6w63qUgEI"],["Pr\u00e9sentation du projet des 20.000 logements sociaux et \u00e9conomiques de Ou\u00e8do","b2Av0_CV64k"],["Construction des logements sociaux: SImAU et l\'entreprise P&N Holding Group SA signent les contrats","590BODYzuhs"],["VIABILISATION DU SITE DE CONSTRUCTION DES LOGEMENTS \u00c0 OU\u00c8DO DANS LA COMMUNE D\u2019ABOMEY CALAVI.","gBLkwgXbTPA"],["CONSTRUCTION DE 20.000 LOGEMENTS SOCIAUX \u00c0 OU\u00c8DO DANS LA COMMUNE D\u2019ABOMEY-CALAVI","L6sltf_iOTQ"]]',
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Assembl\u00e9e Nationale du B\u00e9nin"],["Assistance \u00e0 la ma\u00eetrise d\u2019ouvrage","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","KERE ARCHITECTURE"],["Bureau de Contr\u00f4le Technique","APAVE CI"],["Entreprises","CSCEC (Chine)"],["Autres intervenants","ECOPLAN"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 1,
                'url' =>  'programme_logement',
                'dateDemarrage' =>  '2020',
                'duree' =>  '05 ans',
                'etatAvancement' =>  'En cours',
            ],
            [
                'slides' => 'upload/assemblee_nationale.PNG',
                'titre' => 'Construction du nouveau siège de l’Assemblée Nationale ',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => "
                Le Gouvernement, à travers le Conseil des Ministres du 16 mai 2018, a pris acte de la volonté de la majorité des députés d’arrêter les travaux en cours du siège de l’Assemblée Nationale en vue de construire un nouvel édifice sur un autre site des suites des irrégularités constatées par l’audit commis à cet effet.
                Suite à cette décision et en exécution des instructions du Chef de l’État, le Ministère du Cadre de Vie et du Développement Durable en collaboration avec la Mairie de Porto-Novo a identifié le domaine de l’ex-Gendarmerie à proximité de l’actuel siège de l’Assemblée  Nationale pour  construire  un nouveau bâtiment pour le Parlement Béninois dans sa capitale administrative Porto-Novo.

                ",
                /* 'description' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.
                ", */
                'chefProjet' => 'XXXXX',
                //'maitreOuvrage' => 'SimAU Bénin',
                'coutProjet' => '25 milliards FCFA',
                'zone_intervention' => 'Porto-Novo',
                //'titre_cadre_intitutionnel' => 'Maître d’ouvrage (MOA)',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Assemblée Nationale du Bénin"},{"titre_cadre_intitutionnel2":"Assistance à la maîtrise d’ouvrage  ","description_cadre_intitutionnel2":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel3":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel3":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel4":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel4":"KERE ARCHITECTURE"},{"titre_cadre_intitutionnel5":"Bureau de Contrôle Technique ","description_cadre_intitutionnel5":"APAVE CI "},{"titre_cadre_intitutionnel6":"Entreprises ","description_cadre_intitutionnel6":"CSCEC (Chine)"},{"titre_cadre_intitutionnel7":"Autres intervenants ","description_cadre_intitutionnel7":"ECOPLAN"}]',
                'ficheProjet' => 'upload/FicheConstructiondunouveausiegedelAssembleeNationale-converti.pdf',
                'images' => '["upload/assemble_national011.PNG","upload/assemble_nationale01.PNG","upload/assemble_nationale02.PNG","upload/assemble_nationale03.PNG","upload/assemble_nationale04.PNG","upload/nat01.PNG","upload/nat02.PNG","upload/nat03.PNG","upload/nat04.PNG","upload/nat05.PNG","upload/nat06.PNG","upload/nat07.PNG"]',
                'video' =>  '[["Nouveau si\u00e8ge de l\'assembl\u00e9e nationale : les travaux de construction lanc\u00e9s","WXl1soyp1IE"],["Futur si\u00e8ge du parlement : Tonato s\u00e9duit les d\u00e9put\u00e9s","H24sTimBBkI"],["POINT DE LA CEREMONIE DE REMISE DE SITE DU NOUVEAU SIEGE DE L\'ASSEMBL\u00c9E NATIONALE DU B\u00c9NIN","583e9pNZelM"],["Assembl\u00e9e nationale : la construction d\'un nouveau si\u00e8ge act\u00e9e","2_tJcOGpA5I"]]',
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Assembl\u00e9e Nationale du B\u00e9nin"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","KERE ARCHITECTURE"],["Entreprises","CSCEC (Chine), ECOPLAN"],["Bureau de Contr\u00f4le Technique","APAVE CI"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 2,
                'images_acceuil' =>  'upload/assemble_national011.PNG',
                'url' =>  'nouveau_siege_assemblee',
                'chefProjet' => 'XXXXX',
                'dateDemarrage' =>  '01/04/2021',
                'duree' =>  '30 mois',
                'etatAvancement' =>  'En cours',
            ],
            [
                'slides' => 'upload/admin01.PNG',
                'titre' => 'Construction de la Cité administrative',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => "
                Dans le cadre du Programme d’Action du Gouvernement (PAG) de 2016 à 2021, le ministère du cadre de vie et du développement durable a initié la construction d’une cité administrative qui vise à mettre un terme définitif aux baux pour loger les services de l’Etat. Ce projet s’inscrit dans le cadre de la construction de bâtiments administratifs regroupant les services publics afin d’optimiser leur fonctionnement, développer le patrimoine bâti de l’Etat et réaliser des économies d’échelle.
                ",
                /* 'description' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.
                ", */
                'chefProjet' => 'XXXXX',
                //'maitreOuvrage' => 'SimAU Bénin',
                'coutProjet' => '69,8 milliards FCFA',
                'zone_intervention' => 'Ahossougbéta, commune d’Abomey-Calavi, département de l’Atlantique',
                //'titre_cadre_intitutionnel' => 'Maître d’ouvrage (MOA)',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD)  ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE)  ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Bureau de Contrôle Technique  ","description_cadre_intitutionnel4":"SOCOTEC AFRICA"},{"titre_cadre_intitutionnel5":"Entreprises ","description_cadre_intitutionnel5":"Sélection en cours"},{"titre_cadre_intitutionnel6":"Autres intervenants ","description_cadre_intitutionnel6":"AERAMR Conseil"},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => 'upload/FicheConstructiondelaCiteAdministrative-converti.pdf',
                'images' => '["upload/admin08.PNG","upload/admin11.PNG","upload/admin15.PNG","upload/admin05.PNG","upload/admin21.PNG","upload/admin25.PNG","upload/admin02.PNG","upload/admin03.PNG"]',
                'video' =>  '[["Maquette 2mm\/m : Cit\u00e9 Administrative de Cotonou B\u00c9NIN","hNNOlgkT2Zs"],["Seance avec PAP cit\u00e9 administrative Ahossougb\u00e9ta","Y2fU1toDZcM"]]',
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","Cabinet Koffi & Diabat\u00e9 Architectes"],["Entreprises","AERAMR Conseil"],["Bureau de Contr\u00f4le Technique","SOCOTEC AFRICA"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 2,
                'images_acceuil' =>  'upload/cite_admin01.jpg',
                'url' =>  'cite_administratif',
                'dateDemarrage' =>  '2021',
                'duree' =>  '24 mois',
                'etatAvancement' =>  'En appel d’offres',
            ],
            [
                'slides' => 'upload/mini01.PNG',
                'titre' => 'Construction de la Cité ministérielle',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => "
                Pour le fonctionnement de l’administration béninoise notamment les Ministères, l’Etat fait une dépense énorme dans les baux des immeubles dans lesquels la plupart est logée. Le Gouvernement actuel projette donc la construction et l’exploitation d’une cité ministérielle à Cadjèhoun dans le 12ème arrondissement de Cotonou. Cette cité sera construite sur un domaine de 5 Ha et logera 20 Ministères.
                ",
                /* 'description' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.
                ", */
                'chefProjet' => 'XXXXX',
                //'maitreOuvrage' => 'SimAU Bénin',
                'coutProjet' => '65,5 milliards CFA',
                'zone_intervention' => 'Cotonou',
                //'titre_cadre_intitutionnel' => 'Maître d’ouvrage (MOA)',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD)  ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE)  ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Supervision  ","description_cadre_intitutionnel4":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel5":"Bureau de Contrôle Technique ","description_cadre_intitutionnel5":"SOCOTEC AFRICA"},{"titre_cadre_intitutionnel6":"Entreprises  ","description_cadre_intitutionnel6":"Arabian Construction Company (ACC)"},{"titre_cadre_intitutionnel7":"Autres intervenants ","description_cadre_intitutionnel7":"DSID BENIN"}]',
                'ficheProjet' => 'upload/Fiche-ConstructiondelaciteeMinistérielleconverti.pdf',
                'images' => '[ "upload/mini09.PNG", "upload/mini06.PNG", "upload/mini03.PNG", "upload/mini08.PNG", "upload/mini11.PNG"]',
                'video' =>  '[["Projet de construction d\u2019une Cit\u00e9 minist\u00e9rielle \u00e0 Cotonou","pmriOmcpglY"],["Pr\u00e9sentation de site du Projet de construction d\u2019une Cit\u00e9 minist\u00e9rielle \u00e0 Cotonou","pV3HyPXCn20"]]',
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","Cabinet Koffi & Diabat\u00e9 Architectes"],["Entreprises","Arabian Construction Company (ACC), DSID BENIN"],["Bureau de Contr\u00f4le Technique","SOCOTEC AFRICA"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 2,
                'images_acceuil' =>  'upload/cite_ministerielle.jpg',
                'url' =>  'cite_ministeriel',
                'dateDemarrage' =>  '01/02/2021',
                'duree' =>  '24 mois',
                'etatAvancement' =>  'En cours',
            ],
            [
                'slides' => 'upload/pole_aggro_alimentaire.PNG',
                'titre' => 'Construction du pôle agroalimentaire de l’agglomération du Grand Nokoué',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => "
                Avec le développement démographique, urbain et économique, et la volonté de dynamiser le centre-ville de Cotonou, il est nécessaire de repenser la stratégie des flux de produits agroalimentaires, essentiellement concentré sur le marché de Dantokpa. Il est important de créer une véritable plateforme agroalimentaire à l’échelle régionale intégrant : le transfert des activités de gros des produits alimentaires, périssables ou non, depuis Dantokpa et ses alentours ; la transformation des produits agricoles (anacardes, ananas, etc.). Ce projet qui porte sur la construction du pôle agroalimentaire et de l’abattoir de l'agglomération du Grand Nokoué avec toutes les infrastructures connexes, apportera des solutions sur la gestion de l’urbanisation, l’aménagement du territoire et autres.
                ",
                /* 'description' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.

                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Feugiat diam enim quis dui, posuere ut nisi ac. Odio congue egestas elit bibendum eu tellus. Vestibulum magna pellentesque dignissim erat lacinia cras odio ridiculus porta. Tincidunt id iaculis eget donec enim eget neque enim. Consequat, rhoncus commodo congue rhoncus id.
                ", */
                'chefProjet' => 'XXXXX',
                //'maitreOuvrage' => 'SimAU Bénin',
                'coutProjet' => '85 milliards CFA',
                'zone_intervention' => 'Zopah, commune d’Abomey-Calavi',
                //'titre_cadre_intitutionnel' => 'Maître d’ouvrage (MOA)',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD)  ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE)  ","description_cadre_intitutionnel3":"ARTELIA"},{"titre_cadre_intitutionnel4":"Supervision  ","description_cadre_intitutionnel4":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel5":"Bureau de Contrôle Technique ","description_cadre_intitutionnel5":"QUALICONSULT WEST AFRICA"},{"titre_cadre_intitutionnel6":"Entreprises  ","description_cadre_intitutionnel6":"SOGEA-SATOM, Groupement SP Const/Comtel/Dyjesk"},{"titre_cadre_intitutionnel7":"Autres intervenants ","description_cadre_intitutionnel7":"CREDD/BANCA/CPH"}]',
                'ficheProjet' => 'upload/FicheConstructiondupoleagroalimetairedelagglomeraiondugrandNokoue-converti.pdf',
                'images' => '["upload/pole_aggro01.PNG","upload/pole_aggro02.PNG"]',
                'video' =>  '[["Projet de Construction du P\u00f4le Agroalimentaire de l\u2019agglom\u00e9ration du Grand Nokou\u00e9 \u00e0 Abomey \u2013 Calavi","Pn9Bzl07s3w"],["Projet de Construction du P\u00f4le Agroalimentaire de l\u2019agglom\u00e9ration du Grand Nokou\u00e9 \u00e0 Abomey \u2013 Calavi","6vqgetjja_8"],["Projet de Construction du P\u00f4le Agroalimentaire de l\u2019agglom\u00e9ration du Grand Nokou\u00e9 \u00e0 Abomey \u2013 Calavi","MtA4ojDMhs0"]]',
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","ARTELIA"],["Supervision","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Bureau de Contr\u00f4le Technique","QUALICONSULT WEST AFRICA"],["Entreprises","SOGEA-SATOM, Groupement SP Const\/Comtel\/Dyjesk"],["Autres intervenants","CREDD\/BANCA\/CPH"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 3,
                'images_acceuil' =>  'upload/pole_aggro01.PNG',
                'url' =>  'pole_agroalimentaire',
                'dateDemarrage' =>  '2021',
                'duree' =>  '24 mois',
                'etatAvancement' =>  'Non démarré',
            ],
            //AutreeeeeProjet

            [
                'slides' => 'upload/policerepublicaine01.PNG',
                'titre' => 'Construction de 11 directions départementales de la Police Republicaine',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => "Le Gouvernement, dans sa politique de sécuriser les personnes et les biens, gage d’une bonne croissance économique, a décidé de doter le Bénin d’un parc infrastructurel aux fins de permettre aux populations et aux opérateurs économiques de s’adonner à leurs activités en toute quiétude. Il a ainsi prévu dans son Programme d’Actions (PAG 2016 - 2021), la mise en place d’un lot important d’infrastructures de sécurité composé de 11 Directions Départementales de la Police Républicaine.",
                'coutProjet' => '2,7 Milliards FCFA HT',
                'zone_intervention' => 'Tous les départements du Bénin sauf le Littoral',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Entreprises","description_cadre_intitutionnel4":"null"},{"titre_cadre_intitutionnel5":"Autres intervenants ","description_cadre_intitutionnel5":"SONEB, SBEE, SBIN, etc. "},{"titre_cadre_intitutionnel6":null,"description_cadre_intitutionnel6":null},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => 'upload/FICHE_PROJET_11_DDPR.pdf',
                'images' => '["upload/policerepublicaine06.PNG","upload/policerepublicaine07.PNG"]',
                'images_acceuil' =>  'upload/commisariat002.PNG',
                'video' =>  null,
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Assembl\u00e9e Nationale du B\u00e9nin"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","KERE ARCHITECTURE"],["Entreprises","CSCEC (Chine), ECOPLAN"],["Bureau de Contr\u00f4le Technique","APAVE CI"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 5,
                'url' =>  'programme_logement',
                'chefProjet' => 'XXXXX',
                'dateDemarrage' =>  '2021',
                'duree' =>  '24 mois',
                'etatAvancement' =>  'Non démarré',
            ],

             [
                'slides' => 'upload/commissariat2.PNG',
                'titre' => 'Construction de 155 commissariats d’arrondissement',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => "Le Gouvernement, dans sa politique de sécuriser les personnes et les biens, gage d’une bonne croissance économique, a décidé de doter le Bénin d’un parc infrastructurel à fin de permettre aux populations et aux opérateurs économiques de s’adonner à leurs activités en toute quiétude. Il a ainsi prévu dans son Programme d’Actions (PAG 2016 - 2021), la mise en place d’un lot important d’infrastructures de sécurité composé de 155 commissariats d’arrondissements.",
                'coutProjet' => '19,3 milliards FCFA HT',
                'zone_intervention' => 'Toutes les 77 communes du Bénin',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Entreprises","description_cadre_intitutionnel4":"null"},{"titre_cadre_intitutionnel5":"Autres intervenants ","description_cadre_intitutionnel5":"SONEB, SBEE, SBIN, etc."},{"titre_cadre_intitutionnel6":null,"description_cadre_intitutionnel6":null},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => 'upload/FICHE_PROJET_155_Commissariats.pdf',
                'images' => null,
                'images_acceuil' =>  'upload/republiq002.jpg',
                'video' =>  null,
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Assembl\u00e9e Nationale du B\u00e9nin"],["Assistance \u00e0 la ma\u00eetrise d\u2019ouvrage","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","KERE ARCHITECTURE"],["Entreprises","CSCEC (Chine), ECOPLAN"],["Bureau de Contr\u00f4le Technique","APAVE CI"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 5,
                'url' =>  'programme_logement',
                'chefProjet' => 'XXXXX',
                'dateDemarrage' =>  '2022',
                'duree' =>  '24 mois',
                'etatAvancement' =>  'Non démarré',
            ],

            [
                'slides' => 'upload/stade01.PNG',
                'titre' => 'Construction du pôle commercial & restructuration du Stade GMK',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Premier projet: 20 000 logements du PAG 2016-2021</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => "Dans le cadre de la délocalisation future du marché Dantokpa, en plus du projet de construction du pôle agroalimentaire de l’agglomération du grand Nokoué à Abomey-Calavi, le Ministère du Cadre de Vie et du Développement Durable a établi un programme de restructuration complète du site du Stade GMK à Cotonou.
                Ce projet comprend la rénovation du Stade et du Palais des Sports et la démolition des autres équipements existants pour la construction d’un Centre Commercial, de Halles Commerciales et d’un ensemble d’équipements sportifs (Piscine Olympique et Tennis).
                ",
                'coutProjet' => '30 Milliard FCFA HT',
                'zone_intervention' => 'Cotonou',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Entreprises","description_cadre_intitutionnel4":"null"},{"titre_cadre_intitutionnel5":"Autres intervenants ","description_cadre_intitutionnel5":"SONEB, SBEE, SBIN, etc."},{"titre_cadre_intitutionnel6":null,"description_cadre_intitutionnel6":null},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => 'upload/FICHE_PROJET_Stade_GMK.pdf',
                'images' => '["upload/stade02.PNG","upload/stade04.PNG","upload/stade05.PNG","upload/stade06.PNG","upload/stade08.PNG"]',
                'images_acceuil' =>  'upload/stade02.PNG',
                'video' =>  null,
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Assembl\u00e9e Nationale du B\u00e9nin"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","KERE ARCHITECTURE"],["Entreprises","CSCEC (Chine), ECOPLAN"],["Bureau de Contr\u00f4le Technique","APAVE CI"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 3,
                'url' =>  'programme_logement',
                'chefProjet' => 'XXXXX',
                'dateDemarrage' =>  '2021',
                'duree' =>  '30 mois',
                'etatAvancement' =>  'Non démarré',
            ],

            /*[
                'slides' => 'upload/28.jpg',
                'titre' => 'CONSTRUCTION DU QUARTIER GENERAL DE LA POLICE REPUBLICAINE',
                'resume' => "Le Gouvernement, dans sa politique de sécuriser les personnes et les biens, gage d’une bonne croissance économique, a décidé de doter le Bénin d’un parc infrastructurel aux fins de permettre aux populations et aux opérateurs économiques de s’adonner à leurs activités en toute quiétude. Il a ainsi prévu dans son Programme d’Actions (PAG 2016 - 2021), la mise en place d’un lot important d’infrastructures de sécurité composé d’un quartier Général de la Police.",
                'coutProjet' => '5.6 Milliards f CFA HT',
                'zone_intervention' => 'Etat béninois',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Entreprises","description_cadre_intitutionnel4":"null"},{"titre_cadre_intitutionnel5":"Autres intervenants ","description_cadre_intitutionnel5":"SONEB, SBEE, SBIN, etc."},{"titre_cadre_intitutionnel6":null,"description_cadre_intitutionnel6":null},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => '["upload/FICHE_PROJET_QGPR.pdf"]',
                'images' => '["upload/28.jpg","upload/29.jpg"]',
                'images_acceuil' =>  'upload/project2.jpg',
                'menu' => 0,
                'isDelete' => 0,
                'categorie_projet_standard_id' => 6,
                'url' =>  'programme_logement',
            ],

             [
                'slides' => 'upload/27.jpg',
                'titre' => 'CONSTRUCTION DU CENTRE SPORTIF ET CULTUREL UNAFRICA',
                'resume' => "Dans la perspective de dynamiser les ressources artisanales, culturelles et sportives, l’Etat du Bénin « Maître d’Ouvrage » a pour projet la construction du Complexe Sportif et Culturel UNAFRICA. Ce projet situé au cœur de la ville de Cotonou sur le Boulevard Saint Michel, vise à faire de ce lieu un espace de convivialité et de rencontre pour tous ceux qui sont passionnés de l’art, la culture et le sport. Un espace dédié à l’avenir du pays et à la formation d’une nouvelle génération. Il sera réalisé précisément, dans l’enceinte de l’ancien site du centre de la promotion artisanale.",
                'coutProjet' => '30 Milliards f CFA HT',
                'zone_intervention' => 'Etat béninois',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Entreprises","description_cadre_intitutionnel4":"null"},{"titre_cadre_intitutionnel5":"Autres intervenants ","description_cadre_intitutionnel5":"SONEB, SBEE, SBIN, etc. "},{"titre_cadre_intitutionnel6":null,"description_cadre_intitutionnel6":null},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => '["upload/FICHE_PROJET_CPA-UNAFRICA.pdf"]',
                'images' => '["upload/28.jpg","upload/29.jpg"]',
                'images_acceuil' =>  'upload/project2.jpg',
                'menu' => 0,
                'isDelete' => 0,
                'categorie_projet_standard_id' => 6,
                'url' =>  'programme_logement',
            ], */
            [
                'slides' => 'upload/militaire01.PNG',
                'titre' => 'Construction de 500 logements sociaux de type F3 pour les militaires',
                'premiere_realisation' => "
                    <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Première Réalisation</h4>
                    <p>Le Gouvernement a confié à la SImAU, la construction de 20 000 logements sociaux et économiques, projet qui figure au PAG 2016-2021.</p>
                ",
                'perspective' => "
                <h4 class=\"mt-2 text-simau-green\"><i class=\"fas fa-arrow-right text-simau-red\"></i>&nbsp;&nbsp;Perspective</h4>
                <p>La SImAU est en négociation pour la conclusion d’autres partenariats pour la réalisation d’autres projets avec le Gouvernement du Bénin mais aussi des acteurs privés. Au titre des projets avancés, on peut citer les résidences militaires (Allada, Ouidah, Parakou).</p>
                ",
                'resume' => '
                <p><span style="font-size: 12.8px;">Le projet de construction de 500 logements militaires, est une composante importante de la modernisation des infrastructures des Forces Armées béninoises. En effet, dans son Programme d’action, le Gouvernement béninois a entrepris la modernisation des équipements et des infrastructures de l’armée qui se traduit à travers diverses réalisations dont par exemple la construction de la caserne d’Allada.</span></p><p><span style="font-size: 12.8px;">C’est donc dans ce contexte que le Gouvernement a initié la construction de logements pour les Forces de Sécurité et de Défense (FDS) afin de pallier les besoins induits par la construction de nouvelles infrastructures militaires.</span></p><p><span style="font-size: 12.8px;">Le projet comprend la viabilisation secondaire et tertiaire des sites et la construction de 200 logements&nbsp; à Allada, 200 à Ouidah et&nbsp; 100 logements à Parakou.</span></p><div><br></div>

                ',
                'coutProjet' => 'Huit (08) Milliards FCFA',
                'zone_intervention' => 'Allada, Ouidah, Parakou',
                'description_cadre_intitutionnel' => '[{"titre_cadre_intitutionnel1":"Maître d’ouvrage (MOA) ","description_cadre_intitutionnel1":"Ministère du Cadre de Vie et du Développement durable (MCVDD)"},{"titre_cadre_intitutionnel2":"Maîtrise d’ouvrage déléguée (MOD) ","description_cadre_intitutionnel2":"Société Immobilière et d’Aménagement urbain (SImAU)"},{"titre_cadre_intitutionnel3":"Maître d’œuvre (MOE) ","description_cadre_intitutionnel3":"Cabinet Koffi & Diabaté Architectes"},{"titre_cadre_intitutionnel4":"Entreprises","description_cadre_intitutionnel4":"null"},{"titre_cadre_intitutionnel5":"Autres intervenants ","description_cadre_intitutionnel5":"SONEB, SBEE, SBIN, etc."},{"titre_cadre_intitutionnel6":null,"description_cadre_intitutionnel6":null},{"titre_cadre_intitutionnel7":null,"description_cadre_intitutionnel7":null}]',
                'ficheProjet' => 'upload/militaire01.pdf',
                'images' => null,
                'images_acceuil' =>  'upload/militaire02.PNG',
                'video' =>  null,
                'menu' => 1,
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","Cabinet d\u2019architecture i-Concept"],["Entreprises","Groupement D\u2019Entreprises SP Construction-Dyjesck- Comtel, SBBE, SONEB, B\u00e9nin T\u00e9l\u00e9com, Mairies d\u2019Allada de Ouidah et de Parakou"],["Bureau de Contr\u00f4le Technique","Sans objet"]]',
                'isDelete' => 0,
                'categorie_projet_standard_id' => 1,
                'url' =>  'programme_logement',
                'chefProjet' => 'XXXXX',
                'dateDemarrage' =>  '2021',
                'duree' =>  '02 ans',
                'etatAvancement' =>  'Etudes en cours',
            ],

        ]);
    }
}
