<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class Slide_accueilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('slide_accueils')->insert([

            [
                'image' => 'upload/20210702115038.jpg',
                'titre' => 'SImAU S.A, votre partenaire pour l\'immobilier',
                'description' => 'La Société Immobilière et d’Aménagement Urbain (SImAU) est créée le 19 mai 2017 dans le cadre d’un partenariat entre l’Etat, les banques et établissements financiers, les sociétés d’assurance et un partenaire de référence.',
                'dateDebut' => '2021-07-02',
                'dateFin' => '2021-07-08',
                'isDelete' => 0,
            ],
            [
                'image' => 'upload/20210702115038.jpg',
                'titre' => 'SImAU S.A, votre partenaire pour l\'immobilier',
                'description' => 'La Société Immobilière et d’Aménagement Urbain (SImAU) est créée le 19 mai 2017 dans le cadre d’un partenariat entre l’Etat, les banques et établissements financiers, les sociétés d’assurance et un partenaire de référence.',
                'dateDebut' => '2021-07-05',
                'dateFin' => '2021-07-08',
                'isDelete' => 0,
            ],
        ]);
    }
}
