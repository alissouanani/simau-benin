<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RecrutementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('recrutements')->insert([

            [
                'poste' => 'INGÉNIEUR(E) DES TRAVAUX PUBLICS (effectif : 7)',
                'files' => '["upload\fiche-de-poste-ingnieur-en-genie-civil.pdf"]',
                'description' => "
                        Sous la responsabilité du Directeur de Projet, vous
                        élaborez des projets d’ouvrage et de construction et
                        étudiez les procédés techniques, les modes constructifs,
                        les coûts. Vous réalisez l’étude d’exécution des travaux et
                        effectuez le suivi technique et économique du chantier et
                        coordonnez une équipe multi corps sur un projet.

                        Profils recherchés:

                        Avoir au minimum ou un diplôme supérieur en génie civil (BAC+5) ou tout autre diplôme équivalent,
                        Avoir au moins trois (03) ans d’expérience professionnelle pertinente, au sein d’une société de BTP ou toute structure équivalente,
                ",
                'activite'=>null,
                'created_at' =>'2021-07-19 14:15:10',
                //'activite'=>'[{"activite":"7","libelle":"Cl\u00f4ture","date":"2021-07-30","description":"Description de la cl\u00f4ture","path":["20210716105103.pdf","20210716105103.pdf","20210716105103.pdf","20210716105103.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]},{"activite":"6","libelle":"En cours de publication","date":"2021-07-29","description":"Description de la publication","path":["20210716104919.pdf","20210716104919.pdf","20210716104919.pdf","20210716104919.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]}]',
                'isDelete' => 0,
            ],
            [
                'poste' => 'ASSISTANT PRINCIPAL MAITRISE D’OUVRAGE BTP (effectif : 1)',
                'description' => "
                        Sous la responsabilité du Directeur de Projet, vous préparez, planifiez
                        et supervisez les conducteurs de chantiers dans la réalisation de travaux
                        neufs ou de maintenance dans le domaine du bâtiment, des travaux publics
                        et des infrastructures.

                        Profils recherchés:

                        Avoir au minimum ou un diplôme supérieur en génie civil (BAC+3) ou tout autre diplôme équivalent,
                        Avoir au moins trois (03) ans d’expérience professionnelle pertinente, au sein d’une société de BTP ou toute structure équivalente,
                ",
                'files' => '["upload\fiche-de-poste-assistant-matrise-ouvrage-btp.pdf"]',
                'activite'=>null,
                'created_at' =>'2021-07-19 14:15:10',
                //'activite'=>'[{"activite":"7","libelle":"Cl\u00f4ture","date":"2021-07-30","description":"Description de la cl\u00f4ture","path":["20210716105103.pdf","20210716105103.pdf","20210716105103.pdf","20210716105103.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]},{"activite":"6","libelle":"En cours de publication","date":"2021-07-29","description":"Description de la publication","path":["20210716104919.pdf","20210716104919.pdf","20210716104919.pdf","20210716104919.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]}]',
                'isDelete' => 0,
            ],
            [
                'poste' => 'ASSISTANT PRINCIPAL MAITRISE D’OUVRAGE BTP (effectif : 1)',
                'description' => "
                        Sous la responsabilité du Directeur de Projet, vous préparez, planifiez
                        et supervisez les conducteurs de chantiers dans la réalisation de travaux
                        neufs ou de maintenance dans le domaine du bâtiment, des travaux publics
                        et des infrastructures.

                        Profils recherchés:

                        Avoir au minimum ou un diplôme supérieur en génie civil (BAC+3) ou tout autre diplôme équivalent,
                        Avoir au moins trois (03) ans d’expérience professionnelle pertinente, au sein d’une société de BTP ou toute structure équivalente,
                ",
                'files' => '["upload\fiche-de-poste-assistant-matrise-ouvrage-btp.pdf"]',
                'activite'=>null,
                'created_at' =>'2021-07-19 14:15:10',
                //'activite'=>'[{"activite":"7","libelle":"Cl\u00f4ture","date":"2021-07-30","description":"Description de la cl\u00f4ture","path":["20210716105103.pdf","20210716105103.pdf","20210716105103.pdf","20210716105103.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]},{"activite":"6","libelle":"En cours de publication","date":"2021-07-29","description":"Description de la publication","path":["20210716104919.pdf","20210716104919.pdf","20210716104919.pdf","20210716104919.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]}]',
                'isDelete' => 0,
            ],
            [
                'poste' => 'ASSISTANT PRINCIPAL MAITRISE D’OUVRAGE BTP (effectif : 1)',
                'description' => "
                        Sous la responsabilité du Directeur de Projet, vous préparez, planifiez
                        et supervisez les conducteurs de chantiers dans la réalisation de travaux
                        neufs ou de maintenance dans le domaine du bâtiment, des travaux publics
                        et des infrastructures.

                        Profils recherchés:

                        Avoir au minimum ou un diplôme supérieur en génie civil (BAC+3) ou tout autre diplôme équivalent,
                        Avoir au moins trois (03) ans d’expérience professionnelle pertinente, au sein d’une société de BTP ou toute structure équivalente,
                ",
                'files' => '["upload\fiche-de-poste-assistant-matrise-ouvrage-btp.pdf"]',
                'activite'=>null,
                'created_at' =>'2021-07-19 14:15:10',
                //'activite'=>'[{"activite":"7","libelle":"Cl\u00f4ture","date":"2021-07-30","description":"Description de la cl\u00f4ture","path":["20210716105103.pdf","20210716105103.pdf","20210716105103.pdf","20210716105103.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]},{"activite":"6","libelle":"En cours de publication","date":"2021-07-29","description":"Description de la publication","path":["20210716104919.pdf","20210716104919.pdf","20210716104919.pdf","20210716104919.pdf"],"intitule":["Intitul\u00e9 du document 1","Intitul\u00e9 du document 2","Intitul\u00e9 du document 3","Intitul\u00e9 du document 3"]}]',
                'isDelete' => 0,
            ],
            [
                'poste' => 'ASSISTANT PRINCIPAL MAITRISE D’OUVRAGE BTP (effectif : 1)',
                'description' => "
                        Sous la responsabilité du Directeur de Projet, vous préparez, planifiez
                        et supervisez les conducteurs de chantiers dans la réalisation de travaux
                        neufs ou de maintenance dans le domaine du bâtiment, des travaux publics
                        et des infrastructures.

                        Profils recherchés:

                        Avoir au minimum ou un diplôme supérieur en génie civil (BAC+3) ou tout autre diplôme équivalent,
                        Avoir au moins trois (03) ans d’expérience professionnelle pertinente, au sein d’une société de BTP ou toute structure équivalente,
                ",
                'files' => '["upload\fiche-de-poste-assistant-matrise-ouvrage-btp.pdf"]',
                'activite'=>'[{"activite":"8","libelle":"Cl\u00f4ture","date":null,"dateCreation":"2021-07-21","description":"Plac\u00e9 sous la tutelle du Minist\u00e8re du Plan et du D\u00e9veloppement, le Programme National de D\u00e9veloppement Communautaire est\r\norganis\u00e9 par l\u2019Arr\u00eat\u00e9 N\u00b00025 du 11 Avril 2013. Le PNDC est un portefeuille de projets et un laboratoire d\u2019exp\u00e9rimentation des\r\nstrat\u00e9gies nationales de d\u00e9veloppement communautaire. Il intervient dans les zones o\u00f9 les indicateurs \u00e9conomiques et socio\u00e9conomiques sont les plus d\u00e9favorables.\r\nFaisant suite \u00e0 la volont\u00e9 minist\u00e9rielle d\u2019opti","path":["20210719103825.pdf"],"intitule":["Avis de cl\u00f4ture du recrutement"]},{"activite":"7","libelle":"En cours de publication","date":"2021-07-20T16:00","dateCreation":"2021-07-21","description":"Plac\u00e9 sous la tutelle du Minist\u00e8re du Plan et du D\u00e9veloppement, le Programme National de D\u00e9veloppement Communautaire est\r\norganis\u00e9 par l\u2019Arr\u00eat\u00e9 N\u00b00025 du 11 Avril 2013. Le PNDC est un portefeuille de projets et un laboratoire d\u2019exp\u00e9rimentation des\r\nstrat\u00e9gies nationales de d\u00e9veloppement communautaire. Il intervient dans les zones o\u00f9 les indicateurs \u00e9conomiques et socio\u00e9conomiques sont les plus d\u00e9favorables.\r\nFaisant suite \u00e0 la volont\u00e9 minist\u00e9rielle d\u2019opti","path":["20210719103646.pdf"],"intitule":["Avis de recrutement"]}]',
                'created_at' =>'2021-07-19 14:15:10',
                'isDelete' => 0,
            ],
        ]);
    }
}
