<?php

namespace Database\Seeders;

use App\Models\TypeLogementDemande;
use Illuminate\Database\Seeder;

class TypeLogementDemandesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TypeLogementDemande::factory()->count(10)->create();
    }
}
