<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SourcePaiementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sourcepaiements')->insert([

            [
                'libelle' => 'Salaires et primes ',
                'isDelete'=> 0,
            ],
            [
                'libelle' => 'Cautions',
                'isDelete'=> 0,
            ],
            [
                'libelle' => 'Crédit bancaire ',
                'isDelete'=> 0,
            ],
            [
                'libelle' => 'Sponsor ',
                'isDelete'=> 0,
            ],
            [
                'libelle' => 'Autre revenu ',
                'isDelete'=> 0,
            ],


        ]);
    }
}
