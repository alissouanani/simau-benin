<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleConseilAdministrationSeeder extends Seeder
{
    public $listeRolesWithPermissions = [
        'ConseilAdministration' => [
            'permissions' => [
                'index_conseil_administration',
                'create_conseil_administration',
                'update_conseil_administration',
                'delete_conseil_administration',
                'index_societe',
            ]
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->listeRolesWithPermissions as $key => $value) {
            $role =  Role::create(['name' => $key, 'libelle' => 'Conseil d\'administration']);
            foreach ($value['permissions'] as  $permission) {
                $permission = Permission::where('name', $permission)->first();
                $role->givePermissionTo($permission);
            }
        }
    }
}
