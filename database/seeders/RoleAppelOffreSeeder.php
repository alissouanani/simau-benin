<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAppelOffreSeeder extends Seeder
{
    public $listeRolesWithPermissions = [
        'AppelOffre' => [
            'permissions' => [
                'index_appel_offre',
                'create_appel_offre',
                'update_appel_offre',
                'delete_appel_offre',
                'index_societe',
            ]
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->listeRolesWithPermissions as $key => $value) {
            $role =  Role::create(['name' => $key, 'libelle' => 'Appel d\'offre']);
            foreach ($value['permissions'] as  $permission) {
                $permission = Permission::where('name', $permission)->first();
                $role->givePermissionTo($permission);
            }
        }
    }
}
