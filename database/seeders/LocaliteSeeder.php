<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocaliteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('localites')->insert([

            [
                'libelle' => 'Ouèdo (Abomey-Calavi) ',
            ],
            [
                'libelle' => 'Porto-Novo',
            ],
            [
                'libelle' => 'Parakou',
            ],


        ]);
    }
}
