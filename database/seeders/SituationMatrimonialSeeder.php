<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SituationMatrimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('situation_matrimoniales')->insert([

            [
                'Libelle' => 'Célibataire',
            ],
            [
                'Libelle' => 'Marié(e)',
            ],
            [
                'Libelle' => 'Divorcé(e)',
            ],
            [
                'Libelle' => 'veuf (e)',
            ],
            [
                'Libelle' => 'Union libre ',
            ],


        ]);
    }
}
