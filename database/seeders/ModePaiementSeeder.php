<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModePaiementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mode_paiements')->delete();
        DB::table('mode_paiements')->insert([

            [
                'libelle' => 'Achat direct ',
            ],
            [
                'libelle' => 'Location accession',
            ],
            [
                'libelle' => 'Autre ',
            ],


        ]);
    }
}
