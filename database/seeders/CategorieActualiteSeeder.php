<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CategorieActualite;

class CategorieActualiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'ACTUALITE',
            "APPEL D'OFFRE",
            "RECRUTEMENT",
            "VIE DES PROJETS",
        ];

        foreach ($categories as $cat) {
            CategorieActualite::create(['libelle' => $cat]);
        }
    }
}
