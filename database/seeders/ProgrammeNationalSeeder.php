<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgrammeNationalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programmenationals')->insert([

            [
                'description' => "
                <p>
                Le projet 20 000 logements ambitionne de construire des logements économiques et sociaux dans 14 villes du Bénin.
                </p><br>
                <p>
                Ces nouvelles villes permettent des modes d'aménagement nouveaux au Bénin, marqués par la création d’un espace urbain moderne à travers le pays et qui tient compte du respect et de la préservation de chaque environnement local et surtout la création d’Eco-quartier avec des villes durables caractérisées par :
                </p><br>
                <ul>
                    <li>
                    Des quartiers résidentiels agrémentés de commerces et ce dans toutes les communes concernées
                    </li>
                    <li>
                    De nouveaux espaces mixte pour toutes les classes sociales avec des logements sociaux et économiques
                    </li>
                    <li>
                        L’utilisation d’une solution d’assainissement semi-collectif et individuel adapté pour un entretien facile et économique
                    </li>
                    <li>
                    Des pistes et gares dédiées aux zemidjans, des lignes de bus, des parcours piétons pour faciliter le déplacement des habitants et de nouvelles voies pour une connexion facile avec Cotonou
                    </li>
                    <li>
                    Dans sa phase de démarrage, le projet va viser la construction de 3 nouvelles villes dans les communes d’Abomey-Calavi, Porto-Novo et Parakou. Ces 3 premiers sites se caractérisent par une intégration dans les Communes d’accueil avec les particularités :
                        <ul>
                            <li>
                            Nouvelle ville de Ouèdo à 40 km de Cotonou, non loin de l’emplacement de la nouvelle cité administrative Ahossougbèta, au carrefour des routes inter-états (Cotonou-Lomé, Cotonou-Parakou), etc.
                            </li>
                            <li>
                            Nouvelle ville de Porto-Novo situé à Djègan-Kpèvi dans le 5ième Arrondissement
                            </li>
                            <li>
                            Nouvelle ville de Parakou située à Béyérou, dans le 1er arrondissement
                            </li>
                        </ul>
                    </li>
                </ul>
                ",
                'titre'=>"Le programme 20 000 logements ambitionne d'offrir des logements économiques et sociaux dans 14 villes du Bénin.",
                'image'=>"upload/projet-national.png",
                'informationSuplementaire'=>'[{"titre":"Pahou150","description1":"Occaecat culpa id culpa commodo consectetur proident ea cillum deserunt Lorem esse et incididunt mollit.","photo":"upload/1.png"}] '/* '[
                    {"titre":"Pahou150",
                    "description1":"Occaecat culpa id culpa commodo consectetur proident ea cillum deserunt Lorem esse et incididunt mollit.",
                    "photo":"upload/26.jpg"
                    },
                    ]' */,
                'isDelete'=> 0,
            ],

        ]);
    }
}
