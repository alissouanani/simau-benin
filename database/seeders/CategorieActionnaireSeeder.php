<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieActionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorie_actionnaires')->insert([


            [
                'libelle' => 'Banques commerciales (19.50%)',
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Compagnies d\'assurance (20.00%)',
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Autres (28.00%)',
                'isDelete' => 0,
            ],

        ]);
    }
}
