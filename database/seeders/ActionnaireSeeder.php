<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actionnaires')->insert([

            [
                'nom' => 'CORIS BANK INTERNATIONNAL SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/logo-coris-bank.jpg',
                'site' => 'https://benin.coris.bank/',
            ],
            [
                'nom' => 'BANQUE AFRICAINE POUR L’INDUSTRIE ET LE COMMERCE SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/BIIC-Benin.jpg',
                'site' => 'https://www.biic-bank.com/',
            ],
            [
                'nom' => 'BANQUE SAHELO-SAHARIENNE POUR L’INVESTISSEMENT ET LE COMMERCE SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/bsic.jpg',
                'site' => 'https://bsicbank.com',
            ],
            [
                'nom' => 'UNITED BANK FOR AFRICA BENIN  SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/logo-uba-bank.jpg',
                'site' => 'https://www.ubabenin.com/',
            ],
            [
                'nom' => 'ORABANK BENIN  SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/logo-orabank.jpg',
                'site' => 'https://www.orabank.net/',
            ],
            [
                'nom' => 'BGFI BANK BENIN  SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/logo-bgfi-bank.jpg',
                'site' => 'https://benin.groupebgfibank.com/',
            ],
            /* [
                'nom' => 'BANQUE INTERNATIONALE DU BENIN SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/internationnal1.png',
                'site' => 'javascript:void(0);',
            ], */
            [
                'nom' => 'ECOBANK - BENIN SA',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/ecobank-logo-vector.png',
                'site' => 'https://ecobank.com/',
            ],
            [
                'nom' => 'BANK OF AFRICA BENIN',
                'categorie_actionnaire_id' => 1,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/logo-boa-benin.jpg',
                'site' => 'https://www.boabenin.com/',
            ],
            [
                'nom' => 'L’AFRICAINE DES ASSURANCES SA',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/affricainedesassurance.webp',
                'site' => 'https://www.africaine-assur.com/',
            ],
            [
                'nom' => 'ATLANTIQUE ASSURANCES BENIN  SA',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/logo-banque-atlantique.jpg',
                'site' => 'https://www.atlantiqueassurances.bj/',
            ],
            [
                'nom' => 'NSIA ASSURANCES  SA',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/unnamed.jpg',
                'site' => 'https://www.groupensia.com/',
            ],
            [
                'nom' => 'SOCIETE AFRICAINE D’ASSURANCES ET DE REASSURANCES DU BENIN  SA',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/saar-assurances-compagnie-assurances-benin.jpg',
                'site' => 'javascript:void(0);',
            ],
            [
                'nom' => 'L’AFRICAINE VIE BENIN',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/logoaavieblack.png',
                'site' => 'https://lafricaineviebenin.com/',
            ],
            [
                'nom' => 'ATLANTIQUE ASSURANCES VIE',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/Atlantique-assurances-côte-divoire.jpg',
                'site' => 'javascript:void(0);',
            ],
            [
                'nom' => 'NSIA VIE Assurances SA',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/Tout-est-fait-dans-la-legalite_ng_image_full.jpg',
                'site' => 'javascript:void(0);',
            ],
            [
                'nom' => 'SUNU ASSURANCES VIE BENIN SA',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/sunu_images.png',
                'site' => 'http://benin.vie.sunu-group.com/',
            ],
            [
                'nom' => 'LA GENERALE DES ASSURANCES DU BENIN  SA',
                'categorie_actionnaire_id' => 2,
                'pourcentage' => null,
                'isDelete' => 0,
                'logo' => 'upload/gabbenin.jpg',
                'site' => 'https://gabassurance.com/',
            ],/*
            [
                'nom' => 'Banques commerciales',
                'categorie_actionnaire_id' => 3,
                'pourcentage' => 19.5,
                'isDelete' => 0,
                'logo' => 'upload/istockphoto-1022981602-170667a.jpg',
            ],
            [
                'nom' => 'Compagnies d\'assurance',
                'categorie_actionnaire_id' => 3,
                'pourcentage' => 20,
                'isDelete' => 0,
                'logo' => 'upload/ORIGINAL-auto-300x300.png',
            ], */
            /* [
                'nom' => 'ARISE MAURITIUS',
                'categorie_actionnaire_id' => 3,
                'pourcentage' => 30,
                'isDelete' => 0,
                'logo' => 'upload/air-mauritius_350_200_65.jpg',
                'site' => 'javascript:void(0);',
            ],
            [
                'nom' => 'ETAT',
                'categorie_actionnaire_id' => 3,
                'pourcentage' => 2.5,
                'isDelete' => 0,
                'logo' => 'upload/etatbenin.png',
                'site' => 'https://www.gouv.bj/',
            ], */
            [
                'nom' => 'CNSS',
                'categorie_actionnaire_id' => 3,
                'pourcentage' => 18,
                'isDelete' => 0,
                'logo' => 'upload/csm_CNSS_oct_3e24b9b99c.jpg',
                'site' => 'https://cnss.bj/',
            ],
            [
                'nom' => 'BOAD',
                'categorie_actionnaire_id' => 3,
                'pourcentage' => 10,
                'isDelete' => 0,
                'logo' => 'upload/logo_boad.jpg',
                'site' => 'https://www.boad.org/',
            ],

        ]);
    }
}
