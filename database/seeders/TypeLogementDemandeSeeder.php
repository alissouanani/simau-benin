<?php

namespace Database\Seeders;

use App\Models\TypeLogementDemande;
use Illuminate\Database\Seeder;

class TypeLogementDemandeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TypeLogementDemande::factory()->count(1000)->create();
    }
}
