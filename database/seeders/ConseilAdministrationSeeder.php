<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConseilAdministrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('conseil_administrations')->insert([

            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'RIZWAN',
                'prenom' => 'Haider',
                'poste' => 'Président',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],

            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'DAGNON',
                'prenom' => 'Johannès',
                'poste' => 'Représentant de l\'État Béninois',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'SARAKA',
                'prenom' => 'Alain',
                'poste' => 'Représentant Permanent ARISE MAURITIUS',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'DAHOUI',
                'prenom' => 'Philippe',
                'poste' => 'Représentant Permanent Caisse Nationale de Sécurité Sociale',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],

            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'DAOUDA',
                'prenom' => 'Berte',
                'poste' => 'Représentant Banque Ouest Africaine de Développement',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'AYIBATIN',
                'prenom' => 'Didier',
                'poste' => 'Représentant Banque Internationale pour l\'Industrie et le Commerce',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'ELEGBEDE',
                'prenom' => 'Kenneth',
                'poste' => 'Représentant de l\'Africaine des assurances',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'AGBOTA',
                'prenom' => 'Fabrice',
                'poste' => 'Représentant Permanent NSIA ASSURANCES BENIN',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'CISSE',
                'prenom' => 'Sadio',
                'poste' => 'Représentant Permanent Bank of Africa Benin',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'HOUSSOU',
                'prenom' => 'Moïse Achille',
                'poste' => 'Administrateur',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'ABOUDOU',
                'prenom' => 'Mohamed Bachir',
                'poste' => 'Administrateur',
                'categorie_administration_id' => 1,
                'isDelete' => 0,
            ],


            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'Houssou',
                'prenom' => 'Moïse Achille',
                'poste' => 'Directeur Général',
                'categorie_administration_id' => 2,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'HOUEDE',
                'prenom' => 'Fabrice',
                'poste' => 'Directeur Juridique',
                'categorie_administration_id' => 2,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'FANDOHAN',
                'prenom' => 'Sidney',
                'poste' => 'Directeur administratif et financier',
                'categorie_administration_id' => 2,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'GBENAKPON',
                'prenom' => 'Charles',
                'poste' => 'Directeur Technique par Intérim / Directeur de Projets',
                'categorie_administration_id' => 2,
                'isDelete' => 0,
            ],
            [
                'photo' => 'upload/unnamed.png',
                'nom' => 'MAFORIKAN',
                'prenom' => 'Isabelle',
                'poste' => 'Directrice des projets',
                'categorie_administration_id' => 2,
                'isDelete' => 0,
            ],

        ]);
    }
}
