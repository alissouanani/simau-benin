<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieProjetStandardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorie_projet_standards')->insert([

            [
                'id'=>1,
                'libelle' => 'Programme logements',
                'isDelete' => 0,
            ],
            [
                'id'=>2,
                'libelle' => 'Projets bâtiment administratif',
                'isDelete' => 0,
            ],
            [
                'id'=>3,
                'libelle' => 'Projets infrastructure de commerce',
                'isDelete' => 0,
            ],
            [
                'id'=>4,
                'libelle' => 'Autres projets',
                'isDelete' => 0,
            ],
            [
                'id'=>5,
                'libelle' => 'Projets infrastructure de sécurité et de défense',
                'isDelete' => 0,
            ],
            /* [
                'id'=>6,
                'libelle' => 'Projets infrastructures des arts et loisirs',
                'isDelete' => 0,
            ], */

        ]);
    }
}
