<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActiviteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activites')->insert([
            //Appel Offre
            [
                'libelle' => 'Lancement',
                'categorieActivite' => 0,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Report du dépôt des offres',
                'categorieActivite' => 0,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Analyse en cours',
                'categorieActivite' => 0,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Publication des Résultats',
                'categorieActivite' => 0,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Réponse aux questions des soumissionnaires',
                'categorieActivite' => 0,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Avis d\'attribution définitive',
                'categorieActivite' => 0,
                'isDelete' => 0,
            ],



            //for recrutement
            [
                'libelle' => 'En cours de publication',
                'categorieActivite' => 1,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Clôture',
                'categorieActivite' => 1,
                'isDelete' => 0,
            ],

        ]);
    }
}
