<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            [
                'id' =>1,
                'nom' => 'KPOSSA',
                'prenom' => 'Nabil',
                'email' => 'nabilkpossa51@gmail.com',
                'password_default' => 123456,
                'isDelete' => 0,
            ],

            [
                'id' =>2,
                'nom' => 'DJIMA',
                'prenom' => 'Auspicia',
                'email' => 'auspiciadjima@gmail.com',
                'password_default' => 123456,
                'isDelete' => 0,
            ],
            [
                'id' =>3,
                'nom' => 'ABLO',
                'prenom' => 'DONALD',
                'email' => 'ablo3dv@gmail.com',
                'password_default' => 123456,
                'isDelete' => 0,
            ],

        ]);



        DB::table('users')->insert([

            [
                'nom' => 'admin1',
                'prenom' => 'admin1',
                'email' => 'admin1@simau.com',
                'password' => bcrypt('P@ssw0rd'),
                'isDelete' => 0,
            ],
            [
                'nom' => 'admin2',
                'prenom' => 'admin2',
                'email' => 'admin2@simau.com',
                'password' => bcrypt('P@ssw0rd'),
                'isDelete' => 0,
            ],
            [
                'nom' => 'admin',
                'prenom' => 'admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('password'),
                'isDelete' => 0,
            ],

        ]);

        DB::table('model_has_roles')->insert([
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 1,
            ],
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 2,
            ],
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 3,
            ],
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 6,
            ],
        ]);
    }
}
