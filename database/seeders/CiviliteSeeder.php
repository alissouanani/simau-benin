<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CiviliteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('civilites')->insert([

            [
                'Libelle' => 'Madame',
            ],
            [
                'Libelle' => 'Mademoiselle',
            ],
            [
                'Libelle' => 'Monsieur',
            ],


        ]);
    }
}
