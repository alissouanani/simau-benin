<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OptionLogementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('option_logements')->insert([

            [
                'libelle' => 'RDC',
                'type_logement_id' => 1,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'RDC',
                'type_logement_id' => 2,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'RDC',
                'type_logement_id' => 3,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'RDC',
                'type_logement_id' => 4,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'RDC',
                'type_logement_id' => 5,
                'isDelete' => 0,
            ],
            [
                'libelle' => '1er étage',
                'type_logement_id' => 1,
                'isDelete' => 0,
            ],
            [
                'libelle' => '1er étage',
                'type_logement_id' => 2,
                'isDelete' => 0,
            ],
            [
                'libelle' => '1er étage',
                'type_logement_id' => 3,
                'isDelete' => 0,
            ],
            [
                'libelle' => '1er étage',
                'type_logement_id' => 4,
                'isDelete' => 0,
            ],
            [
                'libelle' => '1er étage',
                'type_logement_id' => 5,
                'isDelete' => 0,
            ],
            [
                'libelle' => '2ème étage',
                'type_logement_id' => 1,
                'isDelete' => 0,
            ],
            [
                'libelle' => '2ème étage',
                'type_logement_id' => 2,
                'isDelete' => 0,
            ],
            [
                'libelle' => '2ème étage',
                'type_logement_id' => 3,
                'isDelete' => 0,
            ],
            [
                'libelle' => '2ème étage',
                'type_logement_id' => 4,
                'isDelete' => 0,
            ],
            [
                'libelle' => '2ème étage',
                'type_logement_id' => 5,
                'isDelete' => 0,
            ],
            [
                'libelle' => '3ième étage',
                'type_logement_id' => 1,
                'isDelete' => 0,
            ],
            [
                'libelle' => '3ième étage',
                'type_logement_id' => 2,
                'isDelete' => 0,
            ],
            [
                'libelle' => '3ième étage',
                'type_logement_id' => 3,
                'isDelete' => 0,
            ],
            [
                'libelle' => '3ième étage',
                'type_logement_id' => 4,
                'isDelete' => 0,
            ],
            [
                'libelle' => '3ième étage',
                'type_logement_id' => 5,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Extrémité',
                'type_logement_id' => 1,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Extrémité',
                'type_logement_id' => 2,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Extrémité',
                'type_logement_id' => 3,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Extrémité',
                'type_logement_id' => 4,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Extrémité',
                'type_logement_id' => 5,
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Autre, à préciser ',
                'type_logement_id' => null,
                'isDelete' => 0,
            ],


        ]);
    }
}
