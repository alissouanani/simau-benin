<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesAppelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorieappels')->insert([

            [
                'libelle' => 'DAO ouvert',
                'isDelete' => 0,
            ],
            [
                'libelle' => 'DAO payant',
                'isDelete' => 0,
            ],

        ]);
    }
}
