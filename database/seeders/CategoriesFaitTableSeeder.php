<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesFaitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categoriefaits')->insert([

            [
                'libelle' => 'Lancement',
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Grève',
                'isDelete' => 0,
            ],
            [
                'libelle' => 'Résultat Depoiellement',
                'isDelete' => 0,
            ],

        ]);
    }
}
