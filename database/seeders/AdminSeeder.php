<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminSeeder extends Seeder
{
    public $listeRolesWithPermissions = [
        'Admin' => [
            'permissions' => [
                'index_slide_acceuil',
                'create_slide_acceuil',
                'update_slide_acceuil',
                'delete_slide_acceuil',
                'index_actualite',
                'create_actualite',
                'update_actualite',
                'delete_actualite',
                'index_partenaire',
                'create_partenaire',
                'update_partenaire',
                'delete_partenaire',
                'index_programme_realisation',
                'create_programme_realisation',
                'update_programme_realisation',
                'delete_programme_realisation',
                'index_conseil_administration',
                'create_conseil_administration',
                'update_conseil_administration',
                'delete_conseil_administration',
                'index_recrutement',
                'create_recrutement',
                'update_recrutement',
                'delete_recrutement',
                'index_appel_offre',
                'create_appel_offre',
                'update_appel_offre',
                'delete_appel_offre',
                'index_projet_standard',
                'create_projet_standard',
                'update_projet_standard',
                'delete_projet_standard',
                'index_societe',
                'create_societe',
                'index_service',
                'create_service',
                'update_service',
                'delete_service',
                'index_chiffre',
                'create_chiffre',
                'update_chiffre',
                'delete_chiffre',
                'index_presentation_projet',
                'create_presentation_projet',
                'update_presentation_projet',
                'delete_presentation_projet',
                'index_envergure_projet',
                'create_envergure_projet',
                'update_envergure_projet',
                'delete_envergure_projet',
                'index_avantage_offert',
                'create_avantage_offert',
                'update_avantage_offert',
                'delete_avantage_offert',
                'index_type_logement',
                'create_type_logement',
                'update_type_logement',
                'delete_type_logement',
                'index_logement_sociaux',
                'create_logement_sociaux',
                'update_logement_sociaux',
                'delete_logement_sociaux',
                'index_formule_acquisition',
                'create_formule_acquisition',
                'update_formule_acquisition',
                'delete_formule_acquisition',
                'index_mode_commercialisation',
                'create_mode_commercialisation',
                'update_mode_commercialisation',
                'delete_mode_commercialisation',
                'index_logement_disponible',
                'create_logement_disponible',
                'update_logement_disponible',
                'delete_logement_disponible',
            ]
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->listeRolesWithPermissions as $key => $value) {
            $role =  Role::create(['name' => $key, 'libelle' => 'Administrateur']);
            foreach ($value['permissions'] as  $permission) {
                $permission = Permission::where('name', $permission)->first();
                $role->givePermissionTo($permission);
            }
        }
    }
}
