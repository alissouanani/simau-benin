<?php

namespace Database\Seeders;

use App\Models\Profession;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('professions')->insert([

            /* [
                'Libelle' => 'agent de l’etat',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ], */
            [
                'Libelle' => 'Agent du secteur privé ',
                'categorie_professionnelle_id'=> 2,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Artisan(e)',
                'categorie_professionnelle_id'=> 3,
                'isDelete'=> 0,
            ],
            /* [
                'Libelle' => 'Couturier',
                'categorie_professionnelle_id'=> 3,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Menuisier',
                'categorie_professionnelle_id'=> 3,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Macon',
                'categorie_professionnelle_id'=> 3,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Ferrailleur',
                'categorie_professionnelle_id'=> 3,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Peintre',
                'categorie_professionnelle_id'=> 3,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Jardinier',
                'categorie_professionnelle_id'=> 3,
                'isDelete'=> 0,
            ], */
            [
                'Libelle' => 'Agriculteur',
                'categorie_professionnelle_id'=> 4,
                'isDelete'=> 0,
            ],

            [
                'Libelle' => 'commercant',
                'categorie_professionnelle_id'=> 6,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'A préciser',
                'categorie_professionnelle_id'=> 7,
                'isDelete'=> 0,
            ],
            [
                'Libelle' =>  'Agent de l\'Etat : A1',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' =>  'Agent de l\'Etat : A2',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : A3',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : B1',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : B2',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : B3',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : C1',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : C2',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : C3',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : D1',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : D2',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],
            [
                'Libelle' => 'Agent de l\'Etat : D3',
                'categorie_professionnelle_id'=> 1,
                'isDelete'=> 0,
            ],

        ]);

        $profession =['Agent général d’assurance',/* 'Analyste programmeur', 'Animateur',*/'Architecte ',/* 'Assistant scolaire', */
        /* 'Assistant technique', 'Attaché de presse','Audit et conseil','Auto-école ',*/'Avocat ',/* 'Barman','Boulanger'
        ,'Carreleur','Chargé de sécurité','Charpentier','Chef de projet','Chirurgien-dentiste', 'Coach','Coach sportif'
        ,'Coffreur','Coiffeur(euse)','Commissaire aux comptes',*/'Comptable',/* 'Concepteur de logiciel'
        , *//* 'Conducteur de véhicule administratif','Conseiller', */'Consultant', 'Contrôleur technique à la construction'
        ,'Coordinateur de travaux',/*'Cuisinier(ère)','Décorateur ', */'Designer','Dessinateur','Diététicien ',/* 'Documentaliste','Ebéniste','Educateur sportif', 'Electricien','Enseignant','Expert ',*/'Expert géomètre ','Expert-comptable ','Formateur',/* 'Forgeron','Frigoriste' ,*/'Géomètre','Huissier de justice','Psychologue',/* 'Greffier','Guide interpète','hôtesse d\'accueil','Iman','Infirmier libéral ','Infographiste', */'Informaticien','Ingénieur',/* 'Instituteur','Interprète','Jardinier','Joueur professionnel ', */'Maçon','Marketeur','Masseur-kinésithérapeute ', 'Mécanique Auto', 'Médecin ',/* 'Ménagère','Menuisier','Musicien', */'Notaire ',/* 'Organisateur de foires et salons','Orthophoniste ','Orthoptiste ','Ostéopathe','Pasteur','Paysagiste','Peintre','Pilote','Prête', */'Professeur','Programmeur',/* 'Psychanalyste','Psychosociologue','Psychothérapeute','Rédacteur', */'Presse',/* 'Relations publiques','Sage-femme','Soudeur','Sportif professionnel','Statisticien', */'Styliste','Taximan',/* 'Technicien du sol', */'Topographe','Traducteur','Transitaire','Urbaniste','Vétérinaire','Vitrier','Zémidjan'];
        foreach ($profession as $item) {
            Profession::create([
                'Libelle' => $item,
                'categorie_professionnelle_id'=> 5,
            ]);
    }
}
}
