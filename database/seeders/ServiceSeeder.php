<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('services')->insert([

            [
                //'icon' => 'upload/20210702153805.jpg',
                'libelle' => 'Vente de logement',
                'description' => 'Vente de logement',
                'isDelete' => 0,
            ],
            [
                //'icon' => 'upload/20210702153805.jpg',
                'libelle' => 'Location de logement',
                'description' => 'Location de logement',
                'isDelete' => 0,
            ],

        ]);
    }
}
