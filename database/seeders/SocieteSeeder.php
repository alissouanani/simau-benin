<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocieteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('societes')->delete();
        DB::table('societes')->insert([

            [
                'logo' => 'Societelogo/logo.png',
                'nom' => 'SImAU S.A',
                'adresse' => '<p class="p-1">Immeuble NSIA, 1066 Boulevard Saint Michel, Carré n°253, face au CPA, <br style="line-height: 0.1;">Cotonou - Bénin 01 BP 9148 Cotonou.</p>
                                <p class="p-1" style="width: max-content;">Email: info@simaubenin.com</p>
                                <p class="p-1" style="width: max-content;">Téléphones: +229 21 31 54 01 | +229 91 36 33 33</p>
                                <p class="p-1" style="width: max-content;">RCCM:  RB/COT/ 17 B 19689 – IFU:  3201710089538 </p>',
                'telephone' => '+229 21 31 54 01 <br> +229 91 36 33 33',
                'description' => "Votre partenaire pour l'immobilier",
                'description_apropos' => 'A l’initiative de l’Etat, la SImAU a été créée le 19 mai 2017 dans le cadre d’un partenariat avec les banques, les établissements financiers, les sociétés d’assurance. Société Anonyme avec Conseil d’Administration au capital social de Cinq Milliards (5 000 000 000) de Francs CFA, elle a pour objet, toutes les activités se rapportant à la promotion immobilière, l’assistance à maitrise d’ouvrage et la maitrise d’ouvrage déléguée, la construction et l’aménagement urbain.',
                'image_centrale_actualite' => 'upload/news-landing.jpg',
                'image_centrale_apropos' => 'upload/about-landing.jpg',
                'image_centrale_recrutement' => 'upload/recruitment-landing.jpg',
                'image_apropos' => 'upload/SImAU_Pl1.svg',
                'isDelete' => 0,
            ],
        ]);
    }
}
