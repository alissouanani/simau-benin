<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VilleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('villes')->insert([

            [
                'Libelle' => 'Cotonou',
                'departement_id' => 2,

            ],
            [
                'Libelle' => 'Calavi',
                'departement_id' => 1,

            ],


        ]);
    }
}
