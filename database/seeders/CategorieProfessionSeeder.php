<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorie_professionnelles')->insert([

            [
                'Libelle' => 'Agent de l\'Etat',
            ],
            [
                'Libelle' => 'Agent(e) du secteur privé',
            ],
            [
                'Libelle' => 'Artisan(e) ',
            ],
            [
                'Libelle' => 'Agriculteur ',
            ],
            [
                'Libelle' => 'Profession libérale ',
            ],
            [
                'Libelle' => 'Commerçant(e) ',
            ],
            [
                'Libelle' => 'Autre, à préciser ',
            ],


        ]);
    }
}
