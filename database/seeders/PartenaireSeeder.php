<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class PartenaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('partenaires')->insert([

            [
                'logo' => 'upload/partner4.jpg',
                'lien' => 'https://www.boad.org/',
                'libelle' => 'BOAD',
               // 'nom' => 'BOAD',
                'description' => 'La Banque Ouest Africaine de Développement (BOAD) est l’institution commune de financement du développement des Etats de l’Union Monétaire Ouest Africaine (UMOA). Elle a été créée par Accord signé le 14 novembre 1973. La BOAD est devenue opérationnelle en 1976. Les Etats membres sont : le Bénin, le Burkina, la Côte d’Ivoire, la Guinée-Bissau, le Mali, le Niger, le Sénégal et le Togo.',
                'isDelete' => 0,
                'prestataire'=> 0,
            ],
            [
                'logo' => 'upload/logo-kd.png',
                'lien' => 'https://koffi-diabate.com/fr/accueil',
                'libelle' => 'Koffi & Diabaté',
                //'nom' => 'Koffi & Diabaté',
                'description' => ' Cabinet spécialisé dans la construction d\'édifices modernes, de qualité, tout en prenant en compte le style de vie et l’identité culturelle de leurs clients.
                Le cabinet participe à la réalisation de nombreux projets dont:  la cité administrative, la cité ministérielle, le quartier général de la police républicaine, le programme 20000 logements sociaux et économiques au Bénin.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],


            [
                'logo' => 'upload/partner3.jpg',
                'lien' => 'https://www.kerearchitecture.com/',
                'libelle' => 'KEREARCHITECTURE',
                //'nom' => 'KEREARCHITECTURE',
                'description' => 'Kéré Architecture s’appuie sur un double accent mis sur l’excellence de la conception et l’engagement social. ‎‎Le portefeuille de Kéré Architecture couvre un large éventail de projets allant des infrastructures civiques aux installations temporaires, du concept à l’exécution et à travers diverses zones ‎géographiques.‎‎',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],
            [
                'logo' => 'upload/partner2.jpg',
                'lien' => 'https://sogea-satom.com/',
                'libelle' => 'SOGEA SATOM',
                //'nom' => 'SOGEA SATOM',
                'description' => 'Acteur majeur du BTP en Afrique, Sogea-Satom, présent dans plus de vingt pays à travers son réseau de filiales, emploie plus de 15 000 collaborateurs.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],
            [
                'logo' => 'upload/téléchargement.png',
                'lien' => 'https://www.colas.com/',
                'libelle' => 'COLAS',
                //'nom' => 'COLAS',
                'description' => 'Leader mondial de la construction et de l’entretien des infrastructures de transport, qui répond aux enjeux de mobilité, d’urbanisation et d’environnement.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],


            [
                'logo' => 'upload/partner1.jpg',
                'lien' => 'https://www.gouv.bj/',
                'libelle' => 'GOUVERNEMENT DE LA REPUBLIQUE DU BENIN',
                //'nom' => 'GOUVERNEMENT DE LA REPUBLIQUE DU BENIN',
                'description' => 'Aux origines, la terre de l’actuel Bénin était occupée par plusieurs royaumes. Les plus en vue s’appelaient Danhomé (Abomey), Xogbonou (Porto-Novo), Allada, Nikki, Kouandé, Kandi… .
                Les premiers souverains d’Abomey et de Porto-Novo sont issus de la migration Adja-Fon, venue du Togo voisin (Tado). Les autres peuples proviennent de l’actuel Nigéria, Niger ou Burkina-Faso. Ainsi, le pays était jadis un foyer de civilisations anciennes et brillantes, bâties autour de ces royaumes : des cités-États.',
                'isDelete' => 0,
                'prestataire'=> 0,
                'prestataire'=> 0,
            ],






            [
                'logo' => 'upload/partner6.jpg',
                'lien' => 'https://www.biic-bank.com/',
                'libelle' => 'BIIC',
                //'nom' => 'BIIC',
                'description' => 'PLus qu\'une banque,un vrai partenaire Financier.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],

            [
                'logo' => 'upload/partner8.jpg',
                'lien' => 'https://www.ecobank.com/bj/personal-banking',
                'libelle' => 'Ecobank',
                //'nom' => 'Ecobank',
                'description' => 'Nous valorisons l\'intégrité,l\'éthique et le respect',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],
            [
                'logo' => 'upload/partner4.jpg',
                'lien' => 'https://www.boad.org/',
                'libelle' => 'BOAD',
               // 'nom' => 'BOAD',
                'description' => 'La Banque Ouest Africaine de Développement (BOAD) est l’institution commune de financement du développement des Etats de l’Union Monétaire Ouest Africaine (UMOA). Elle a été créée par Accord signé le 14 novembre 1973. La BOAD est devenue opérationnelle en 1976. Les Etats membres sont : le Bénin, le Burkina, la Côte d’Ivoire, la Guinée-Bissau, le Mali, le Niger, le Sénégal et le Togo.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],
            [
                'logo' => 'upload/logo-sbee.png',
                'lien' => 'https://www.sbee.bj/site/',
                'libelle' => 'Société Béninoise d\'Electricité et d\'Eau',
                'description' => 'Cette compagnie prit le nom de Compagnie Centrale de Distribution d’Énergie Électrique après l’indépendance d en 1960

                Février 1973, soit 18 ans après la mise en concession, l’État Dahoméen rachète la CCDEE et créa la Société Dahoméenne d’Électricité et d’Eau (SDEE)et qui  deviendra en 1975 Société Béninoise d’Électricité et d’Eau (SBEE);

                Janvier 2004: Séparation des activités Eau et Électricité et création de la Société Béninoise d’Énergie Électrique',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],
            [
                'logo' => 'upload/soneb.png',
                'lien' => 'https://www.soneb.bj/',
                'libelle' => ' Société Nationale des Eaux du Bénin',
                'description' => 'Au départ, la distribution de l’énergie électrique et de l’eau était assurée au Bénin, depuis la période de l’Indépendance jusqu’à fin décembre 2003, par une seule et unique société : LA SOCIETE BENINOISE D’ELECTRICITE ET D’EAU (SBEE).

                Mais cette société a changé plusieurs fois de dénomination pour diverses raisons.
                En effet, la convention du 30 septembre 1955 avait concédé à la COMPAGNIE COLONIALE DE DISTRIBUTION D’ENERGIE ELECTRIQUE (CCDEE) toutes les installations qui étaient en gérance sous régie. Par la même convention, la CCDEE avait aussi en charge l’adduction et la distribution de l’eau potable à Cotonou, seule ville en développement à cette époque.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],
            [
                'logo' => 'upload/WhatsAppImage2021-08-06.jpeg',
                'lien' => 'https://sbin.bj/',
                'libelle' => 'Société Béninoise d’Infrastructures Numériques S.A',
                'description' => 'Troisième opérateur de téléphonie mobile du Bénin.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ],
            /* [
                'logo' => 'upload/partner9.jpg',
                'lien' => 'https://beninrevele.bj/',
                'libelle' => 'REVEALINGBENIN',
                //'nom' => 'REVEALINGBENIN',
                'description' => 'Un programme d’investissement d’envergure pour relancer de manière durable le développement économique et social du Bénin.Le Programme d’Actions du Gouvernement (PAG) 2016-2021 lancé à Cotonou, le 16 décembre 2016, comprend des projets phares, des réformes ainsi qu’un dispositif de mise en œuvre et de suivi, pour engager la transformation structurelle de l’économie béninoise.',
                'isDelete' => 0,
                'prestataire'=> 1,
            ], */
            [
                'logo' => 'upload/logo-sbee.png',
                'lien' => 'https://www.sbee.bj/site/',
                'libelle' => 'Société Béninoise d\'Electricité et d\'Eau',
                'description' => 'Cette compagnie prit le nom de Compagnie Centrale de Distribution d’Énergie Électrique après l’indépendance d en 1960

                Février 1973, soit 18 ans après la mise en concession, l’État Dahoméen rachète la CCDEE et créa la Société Dahoméenne d’Électricité et d’Eau (SDEE)et qui  deviendra en 1975 Société Béninoise d’Électricité et d’Eau (SBEE);

                Janvier 2004: Séparation des activités Eau et Électricité et création de la Société Béninoise d’Énergie Électrique',
                'isDelete' => 0,
                'prestataire'=> 0,
            ],
            [
                'logo' => 'upload/soneb.png',
                'lien' => 'https://www.soneb.bj/',
                'libelle' => ' Société Nationale des Eaux du Bénin',
                'description' => 'Au départ, la distribution de l’énergie électrique et de l’eau était assurée au Bénin, depuis la période de l’Indépendance jusqu’à fin décembre 2003, par une seule et unique société : LA SOCIETE BENINOISE D’ELECTRICITE ET D’EAU (SBEE).

                Mais cette société a changé plusieurs fois de dénomination pour diverses raisons.
                En effet, la convention du 30 septembre 1955 avait concédé à la COMPAGNIE COLONIALE DE DISTRIBUTION D’ENERGIE ELECTRIQUE (CCDEE) toutes les installations qui étaient en gérance sous régie. Par la même convention, la CCDEE avait aussi en charge l’adduction et la distribution de l’eau potable à Cotonou, seule ville en développement à cette époque.',
                'isDelete' => 0,
                'prestataire'=> 0,
            ],
            [
                'logo' => 'upload/logo-AN.png',
                'lien' => 'https://assemblee-nationale.bj/',
                'libelle' => 'ASSEMBLEE NATIONALE DU BENIN',
                'description' => 'L’Assemblée Nationale est la deuxième Institution de l’Etat. Son histoire montre qu’elle a été très mouvementée et faite jusqu’ici d’une succession d’expériences aussi variées les unes que les autres.',
                'isDelete' => 0,
                'prestataire'=> 0,
            ],
            [
                'logo' => 'upload/csm_CNSS_oct_3e24b9b99c.jpg',
                'lien' => 'https://cnss.bj/',
                'libelle' => 'Caisse Nationale de Sécurité Sociale du Bénin',
                'description' => 'Le système de Sécurité Sociale destiné à la couverture des salariés soumis aux dispositions du code du travail, remonte aux temps coloniaux.',
                'isDelete' => 0,
                'prestataire'=> 0,
            ],

            [
                'logo' => 'upload/sbin.png',
                'lien' => 'https://www.gouv.bj/actualite/1295/societe-beninoise-infrastructures-numeriques--sbin--s.a-gouvernement-groupe-sonatel-officialisent-mise-gestion-deleguee/',
                'libelle' => 'Société Béninoise d’Infrastructures Numériques (SBIN) S.A',
                'description' => 'Le gouvernement de la République du Bénin et le groupe SONATEL ont officialisé le vendredi 21 mai 2021, la mise en gestion déléguée de la Société Béninoise d’Infrastructures Numériques (SBIN) S.A. La cérémonie de signature officielle de la convention de la gestion déléguée, a été co-présidée par le Ministre de l’Économie et des Finances, Monsieur Romuald WADAGNI, la Ministre du Numérique et de la Digitalisation, Madame Aurelie ADAM SOULE ZOUMAROU et le Conseiller Spécial du Président de la République, Monsieur Johannes DAGNON. ',
                'isDelete' => 0,
                'prestataire'=> 0,
            ],

            [
                'logo' => 'upload/andf.jpg',
                'lien' => 'https://www.andf.bj/',
                'libelle' => 'Agence Nationale du Domaine et du Foncier (ANDF) ',
                'description' => 'La loi n°2013-01 du 14 août 2013 portant Code Foncier et Domanial a créé l’Agence Nationale du Domaine et du Foncier (ANDF) qui est un établissement public à caractère technique et scientifique en l’investissant, en son article 418, « d’une mission de sécurisation et de coordination de la gestion foncière et domaniale au plan national ».',
                'isDelete' => 0,
                'prestataire'=> 0,
            ],
        ]);
    }
}
