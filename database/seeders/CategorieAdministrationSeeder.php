<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorieAdministrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorie_administrations')->insert([

            [
                'id'=>1,
                'libelle' => 'Conseil Administration',
                'isDelete' => 0,
            ],
            [
                'id'=>2,
                'libelle' => 'Equipe Direction',
                'isDelete' => 0,
            ],

        ]);
    }
}
