<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeLogementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_logements')->insert([

            [
                'libelle' => 'Appartement A -> RDC',
                'description' => 'Immeuble R+3 de 08 logements de 81,26 m2',
                'localite_id' => 1,
                'type'=> 'Type A',
                'type_id'=> 1,
            ],
            [
                'libelle' => 'Appartement A -> 1er étage',
                'description' => 'Immeuble R+3 de 08 logements de 81,26 m2',
                'localite_id' => 1,
                'type'=> 'Type A',
                'type_id'=> 1,
            ],
            [
                'libelle' => 'Appartement A -> 2ème étage',
                'description' => 'Immeuble R+3 de 08 logements de 81,26 m2',
                'localite_id' => 1,
                'type'=> 'Type A',
                'type_id'=> 1,
            ],
            [
                'libelle' => 'Appartement A -> 3ème étage',
                'description' => 'Immeuble R+3 de 08 logements de 81,26 m2',
                'localite_id' => 1,
                'type'=> 'Type A',
                'type_id'=> 1,
            ],
            [
                'libelle' => 'Appartement A -> Extrémité',
                'description' => 'Immeuble R+3 de 08 logements de 81,26 m2',
                'localite_id' => 1,
                'type'=> 'Type A',
                'type_id'=> 1,
            ],
            [
                'libelle' => 'Appartement B -> RDC',
                'description' => 'Immeuble R+3 de 16 logements de 91,17 m2',
                'localite_id' => 1,
                'type'=> 'Type B',
                'type_id'=> 2,
            ],
            [
                'libelle' => 'Appartement B -> 1er étage',
                'description' => 'Immeuble R+3 de 16 logements de 91,17 m2',
                'localite_id' => 1,
                'type'=> 'Type B',
                'type_id'=> 2,
            ],
            [
                'libelle' => 'Appartement B -> 2ème étage',
                'description' => 'Immeuble R+3 de 16 logements de 91,17 m2',
                'localite_id' => 1,
                'type'=> 'Type B',
                'type_id'=> 2,
            ],
            [
                'libelle' => 'Appartement B -> 3ème étage',
                'description' => 'Immeuble R+3 de 16 logements de 91,17 m2',
                'localite_id' => 1,
                'type'=> 'Type B',
                'type_id'=> 2,
            ],
            [
                'libelle' => 'Appartement B -> Extrémité',
                'description' => 'Immeuble R+3 de 16 logements de 91,17 m2',
                'localite_id' => 1,
                'type'=> 'Type B',
                'type_id'=> 2,
            ],
            [
                'libelle' => 'Appartement C2 -> RDC',
                'description' => 'Immeuble R+2 de 06 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C2',
                'type_id'=> 3,
            ],
            [
                'libelle' => 'Appartement C2 -> 1er étage',
                'description' => 'Immeuble R+2 de 06 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C2',
                'type_id'=> 3,
            ],
            [
                'libelle' => 'Appartement C2 -> 2ème étage',
                'description' => 'Immeuble R+2 de 06 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C2',
                'type_id'=> 3,
            ],
            [
                'libelle' => 'Appartement C2 -> 3ème étage',
                'description' => 'Immeuble R+2 de 06 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C2',
                'type_id'=> 3,
            ],
            [
                'libelle' => 'Appartement C2 -> Extrémité',
                'description' => 'Immeuble R+2 de 06 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C2',
                'type_id'=> 3,
            ],
            [
                'libelle' => 'Appartement C3 -> RDC',
                'description' => 'Immeuble R+3 de 08 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C3',
                'type_id'=> 4,
            ],
            [
                'libelle' => 'Appartement C3 -> 1er étage',
                'description' => 'Immeuble R+3 de 08 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C3',
                'type_id'=> 4,
            ],
            [
                'libelle' => 'Appartement C3 -> 2ème étage',
                'description' => 'Immeuble R+3 de 08 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C3',
                'type_id'=> 4,
            ],
            [
                'libelle' => 'Appartement C3 -> 3ème étage',
                'description' => 'Immeuble R+3 de 08 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C3',
                'type_id'=> 4,
            ],
            [
                'libelle' => 'Appartement C3 -> Extrémité',
                'description' => 'Immeuble R+3 de 08 logements de 83,98 m2',
                'localite_id' => 1,
                'type'=> 'Type C3',
                'type_id'=> 4,
            ],
            [
                'libelle' => 'Villa D -> RDC',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 1,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> 1er étage',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 1,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> 2ème étage',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 1,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> 3ème étage',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 1,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> Extrémité',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 1,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> RDC',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 3,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> 1er étage',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 3,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> 2ème étage',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 3,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> 3ème étage',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 3,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa D -> Extrémité',
                'description' => 'Bâtiment individuel d’un logement de 71,5 m2',
                'localite_id' => 3,
                'type'=> 'Type D',
                'type_id'=> 5,
            ],
            [
                'libelle' => 'Villa E',
                'description' => 'Maison individuelle de 102,65 m2 sur une parcelle de 264 m2',
                'localite_id' => 1,
                'type'=> 'Type E',
                'type_id'=> 6,
            ],
            [
                'libelle' => 'Villa E',
                'description' => 'Maison individuelle de 102,65 m2 sur une parcelle de 264 m2',
                'localite_id' => 2,
                'type'=> 'Type E',
                'type_id'=> 6,
            ],
            [
                'libelle' => 'Villa E',
                'description' => 'Maison individuelle de 102,65 m2 sur une parcelle de 264 m2',
                'localite_id' => 3,
                'type'=> 'Type E',
                'type_id'=> 6,
            ],


        ]);
    }
}
