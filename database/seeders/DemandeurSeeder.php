<?php

namespace Database\Seeders;

use App\Models\Demandeur;
use Illuminate\Database\Seeder;

class DemandeurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Demandeur::factory()->count(200)->create();
    }
}
