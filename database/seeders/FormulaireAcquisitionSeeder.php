<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormulaireAcquisitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formulaireacquisitions')->insert([

            [
                'descriptionTypologieLogementsCollectif' => '
                <p>Le Gouvernement du Bénin et son partenaire la SImAU proposent deux (2) formules d’acquisition pour faciliter l’accès aux logements économiques et sociaux à tous :</p>
                <h5 class="mt-2 text-simau-green"><i class="fas fa-arrow-right text-simau-red"></i>&nbsp;&nbsp;La location-accession</h5>
                <p>
                Cette formule permet aux locataires de devenir propriétaire en payant un loyer mensuel qui varie entre 91 000 et 406000 Fcfa, selon le type de logement.
                </p><br>
                <p>
                L’objectif principal est de permettre aux acquéreurs de devenir propriétaire en payant le montant d’un loyer convenu sur une durée maximale de 17 ans ou 25 ans selon le cas. La loi modificative sur le bail à usage d’habitation domestique au Bénin a institué la « location-accession » préconisée pour le Programme 20 000 Logements.
                </p><br>',
                "descriptionVenteDirecte"=>"
                    <p>
                    Cette formule est l’acquisition au comptant sur les ressources personnelles du candidat ou par un crédit bancaire avec un coût d’acquisition à partir de 18 100 000 Fcfa hors frais et émoluments d'actes.
                    </p><br>",
                'titre'=>"Formules d'acquisition",
                'image'=>"upload/26.jpg",
                'isDelete'=> 0,
            ],

        ]);
    }
}
