<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypePieceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_pieces')->delete();
        DB::table('type_pieces')->insert([

            [
                'Libelle' => 'Carte Nationale d\'Identité (CNI)',
            ],
            [
                'Libelle' => 'Passeport ',
            ],
            [
                'Libelle' => 'Certificat d\'Identification Personnelle (CIP) ',
            ],
            [
                'Libelle' => 'Carte LEPI',
            ],
            [
                'Libelle' => 'Carte professionnelle',
            ],

        ]);
    }
}
