<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PresentationProjetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('presentationprojets')->insert([

            [
                /* 'description' => "
                <p>Le projet consiste en la construction de 20 000 logements économiques et sociaux de type F4, pour le compte du gouvernement du Bénin, dans 14 villes dont 12 960 individuels et 7040 collectifs à travers tout le Bénin. La stratégie d’implémentation des logements repose sur la logique de création de « villes nouvelles » (Abomey-Calavi, Porto-Novo, Sème-Kpodji, Parakou, Abomey et Bohicon) et de « nouveaux quartiers » (Natitingou, Djougou, Kandi, Malanville, Pobè, Ouidah, Dassa-Zoumè, Lokossa, Azovè, Pahou, etc.). </p><br>
                <p>Les constructions ont démarré sur le site de la « nouvelle ville de Ouèdo » sur une superficie de 235 ha pour la construction de 10.849 logements avec la SImAU comme Maître d’ouvrage délégué. La commercialisation qui a été confiée à cette même société privée va démarrer dans quelques semaines par les pré-réservations </p>
                ", */
                'description' => '
                <p>
                Dans le cadre de la réalisation de son <a href="https://beninrevele.bj/" target="_blank">Plan d’Actions du Gouvernement (PAG) 2016-2021</a>,<a href="https://gouv.bj" target="_blank">le Gouvernement du Bénin</a>  a prévu au Pilier 3 « Améliorer les conditions de vie des populations » et sur l’Axe 7 : « Développement équilibré et durable de l’espace national », des actions d’amélioration du cadre de vie, de bien-être de tous et de préservation de l’environnement avec le développement de programmes immobiliers d’habitat.
            </p>
            <p>
                La SImAU est chargée de mettre en œuvre une partie de cette ambition gouvernementale à travers un Programme dénommé « 20 000 logements économiques et sociaux» qui comprend la <a href="/construction_20000_logement" target="_blank">construction de 20 000 logements</a> , leur <a href="/mode_commercialisation" target="_blank">commercialisation</a>  ainsi que la gestion de la copropriété des nouveaux espaces de vie.
            </p>
            <p>
                Ce programme va impacter l’ensemble de la population béninoise car, les logements seront construits dans <a href="/programme_national#map_redirection" target="_blank">14 villes</a>  du Bénin.
            </p>
            <p>
                La dimension sociale a été prise en compte pour donner corps à la vision du Président de la République du Bénin qui lors de son discours d’investiture le 23 mai 2021 à Porto-Novo a déclaré que : « … Ce mandat sera donc hautement social». Tenant compte de cet engagement, la SImAU propose des logements sociaux aux ménages à revenus intermédiaires à des coûts accessibles avec une subvention de l\'Etat.
            </p>
            <p>
                En conclusion, chaque Béninois à travers le Programme 20 000 logements a l’opportunité d’accéder à un logement décent avec un lot d’<a href="#Avantageoffert">avantages</a>  très intéressants.
            </p>
            <h6 class="text-simau-green" ><i class="fas fa-arrow-right text-simau-red"></i>&nbsp;Point d’exécution du Programme</h6>
            <p>
            <ul>
                <li>
                    Réalisation des traveaux de Voiries et Réseaux Divers à Ouèdo en cours d\'achèvement
                </li>
                <li>
                    Construction de plus de 8000 logements économiques et sociaux à Ouèdo en cours d\'exécution
                </li>
                <li>
                    Démarrage des traveaux de Voiries et Réseaux sur le site de Porto-Novo au dernier trimestre 2021
                </li>
                <li>
                    Démarrage des traveaux de Voiries et Réseaux sur le site de Parakou au dernier trimestre 2021
                </li>
            </ul>
            </p>
            <h6 class="text-simau-green" ><i class="fas fa-arrow-right text-simau-red"></i>&nbsp;Commercialisation</h6>
            <p>
                Le processus de commercialisation va démarrer dans les jours à venir avec le lancement de la <a href="/pre_reservation" target="_blank">Préréservation</a> ce qui permettra de recueillir les expressions de besoins en matière de logement des populations. Le processus se poursuivra avec les réservations formelles et, la SImAU communiquera sur ce point en temps opportun.
            </p>
            <h6 class="text-simau-green" ><i class="fas fa-arrow-right text-simau-red"></i>&nbsp;Les éléments à consulter avant d’effectuer la pré-réservation d\'un logement</h6>
            <p>
                Afin de bien choisir leur logement, les personnes intéressées sont invitées à consulter les éléments suivants :
            </p>
            <ul>
                <li>
                    La couverture du Programme
                </li>
                <li>
                    Les types de logements.
                </li>
                <li>
                    Les critères d’attribution.
                </li>
                <li>
                    Les formules d’acquisition.
                </li>
                <li>
                    Les modes de commercialisation.
                </li>
            </ul>
                ',
                'isDelete'=> 0,
                'titre'=> 'Programme 20 000 logements',
                'individuels'=> "12,960",
                'collectifs'=> "7,040",
                'zone_intervention'=> "Territoire national",
                'maitreOuvrage'=> "Ministère du Cadre de vie et du Développement Durable (MCVDD)",
                'maitredelegue'=> "Société Immobilière er d’Aménagement Urbain (SImAU)",
                'coutProjet'=> "385 milliards FCFA",
                'chefProjet'=> "Jean HOUSSOU",
                'ficheProjet'=> "upload/fiche20000logement.pdf",
                'video'=> '[["Pr\u00e9sentation du volet logements sociaux","FUEl0P7q_Hs"],["Pr\u00e9sentation du programme 20 000 logements","5yKQGYrV2so"],["Pr\u00e9sentation de la Nouvelle Cit\u00e9e de Ou\u00e8do","b2Av0_CV64k"],["Travaux de viabilisation VRD \u00e0 Ou\u00e8do","gBLkwgXbTPA"],["Suivi gouvernemental des travaux \u00e0 Ou\u00e8do","cOfFOaeacfY"]]',
                'cadre_institutionnel' => '[["Ma\u00eetre d\u2019ouvrage (MOA)","Minist\u00e8re du Cadre de Vie et du D\u00e9veloppement durable (MCVDD)"],["Ma\u00eetrise d\u2019ouvrage d\u00e9l\u00e9gu\u00e9e (MOD)","Soci\u00e9t\u00e9 Immobili\u00e8re et d\u2019Am\u00e9nagement urbain (SImAU)"],["Ma\u00eetre d\u2019\u0153uvre (MOE)","Cabinet Koffi & Diabat\u00e9 Architectes"],["Entreprises","FRANZETTI, Colas, IBT\/Ecore, ASEMI, PNBF\/PNHG ; GER, Cabinet IRC, SBBE, SONEB, B\u00e9nin T\u00e9l\u00e9com, Mairie d\u2019Abomey-Calavi"]]',
                'dateDemarrage' =>  '2020',
                'duree' =>  '05 ans',
                'etatAvancement' =>  'En cours',
            ],

        ]);
    }
}
