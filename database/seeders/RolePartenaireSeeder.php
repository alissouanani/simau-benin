<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePartenaireSeeder extends Seeder
{
    public $listeRolesWithPermissions = [
        'Partenaire' => [
            'permissions' => [
                'index_partenaire',
                'create_partenaire',
                'update_partenaire',
                'delete_partenaire',
                'index_societe',
            ]
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->listeRolesWithPermissions as $key => $value) {
            $role =  Role::create(['name' => $key, 'libelle' => 'Partenaire']);
            foreach ($value['permissions'] as  $permission) {
                $permission = Permission::where('name', $permission)->first();
                $role->givePermissionTo($permission);
            }
        }
    }
}
