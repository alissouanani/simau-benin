<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AvantageOffertSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('avantageofferts')->insert([

            [
                'description' => '
                <ul>
                <li>
                Une acquisition de logement neuf, aux finitions complètes dans un modèle d’urbanisation moderne
                </li>
                <li>
                Des schémas d’acquisition flexibles pour faciliter l’accès au logement à tous
                </li>
                <li>
                Une offre de logement quatre (4) pièces (un (1) salon + trois (3) chambres) très diversifiée à des prix d’achat accessibles : appartement ou villa individuelle.
                </li>
                <li>
                La garantie d’un investissement sécurisé, via l’engagement de l’Etat, porteur du projet et ses partenaires
                </li>

                <li>
                Le plaisir de vivre dans un environnement sain intégrant des espaces verts de types jardins publics, aires de jeux.
                </li>
                <li>
                Une ville entièrement aménagée avec des voiries dédiées aux voitures, cycles et piétons.
                </li>
                <li>
                Un cadre de vie pensé pour le confort des populations intégrant les équipements publics et privés (écoles, commerces, centres de santé, centres administratif, équipements sportifs et loisirs).
                </li>
                <li>

                L’acquisition de logements conçus dans une architecture durable axée sur la fonctionnalité, la modernité et le bien-vivre.
                    
                </li>
                </ul>
                ',
                'titre'=>"Avantages",
                'photo'=>"upload/projet-national.png",
                'video'=>"upload/26.jpg",
                'isDelete'=> 0,
            ],

        ]);
    }
}
