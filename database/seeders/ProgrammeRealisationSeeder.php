<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgrammeRealisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programme_realisations')->insert([

            [
                'grand_titre' => "
                Présentation du Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi
                ",
                'photo_principale' => 'upload/realisation3.png',
                'petit_titre' => 'Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi',
                'photo1' => 'upload/realisation2.png',
                'presentateur1' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo1' => 'Présentation du projet',
                'photo2' => 'upload/realisation4.png',
                'presentateur2' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo2' => 'Présentation du site',
                'photo3' => 'upload/realisation5.png',
                'presentateur3' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo3' => 'Présentation du projet',
                'description1' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada suspendisse tellus imperdiet pharetra a nisi felis nec, venenatis. Egestas lorem adipiscing non ac commodo malesuada vitae. Nibh urna, turpis in elementum non condimentum libero purus. Facilisis aenean donec id ullamcorper suspendisse. Vitae urna varius velit, venenatis, in tempus nunc. Dictum lacinia massa faucibus magna. Elementum interdum et sit dui massa elementum diam dictum. Scelerisque nec amet maecenas justo ut. Gravida lorem porta pharetra proin at tincidunt auctor. Enim ac pretium tristique sit id leo consectetur felis pharetra.',
                'isDelete' => 0,
                'titre' => 'Construction de 500 logements sociaux pour les militaires',
                'description' => 'Le projet de construction de 500 logements militaires, est une composante importante de la modernisation des infrastructures des Forces Armées béninoises. En effet, dans son Programme d’action, le Gouvernement béninois a entrepris la modernisation des équipements et des infrastructures de l’armée qui se traduit à travers diverses réalisations dont par exemple la construction de la caserne d’Allada.
                C’est donc dans ce contexte que le Gouvernement a initié la construction de logements pour les Forces de Sécurité et de Défense (FDS) afin de pallier les besoins induits par la construction de nouvelles infrastructures militaires.
                Le projet comprend la viabilisation secondaire et tertiaire des sites et la construction de 200 logements à Allada, 200 à Ouidah et 100 logements à Parakou.
                ',
                'image' =>  'upload/project2.jpg',
                'isprogramme' => 0,
            ],
            [
                'grand_titre' => "Présentation du Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo_principale' => "upload/realisation3.png",
                'petit_titre' => "Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo1' => 'upload/realisation2.png',
                'presentateur1' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo1' => 'Présentation du projet',
                'photo2' => 'upload/realisation4.png',
                'presentateur2' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo2' => 'Présentation du site',
                'photo3' => 'upload/realisation5.png',
                'presentateur3' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo3' => 'Présentation du projet',
                'description1' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada suspendisse tellus imperdiet pharetra a nisi felis nec, venenatis. Egestas lorem adipiscing non ac commodo malesuada vitae. Nibh urna, turpis in elementum non condimentum libero purus. Facilisis aenean donec id ullamcorper suspendisse. Vitae urna varius velit, venenatis, in tempus nunc. Dictum lacinia massa faucibus magna. Elementum interdum et sit dui massa elementum diam dictum. Scelerisque nec amet maecenas justo ut. Gravida lorem porta pharetra proin at tincidunt auctor. Enim ac pretium tristique sit id leo consectetur felis pharetra.
                ",
                'isDelete' => 0,
                'titre' => 'Programme 20 000 logements sociaux et économiques',
                'description' => 'Dans le cadre de la réalisation de son Plan d’Actions du Gouvernement (PAG) 2016-2021, le Gouvernement du Bénin a prévu au pilier 3 « améliorer les conditions de vie des populations » et sur son axe 7 : « Développement équilibré et durable de l’espace national », des actions d’amélioration du cadre de vie, le bien-être de tous et de préserver l’environnement avec le développement de programmes immobiliers d’habitat social et économique dans les chefs-lieux de département et dans certaines agglomérations du Bénin. C’est ainsi qu’est né le projet qui consiste à la construction de 20 000 logements (12 960 individuels et 7040 collectifs) de type économique et sociaux, dans différentes villes du Bénin qui a été confiée à l’expertise de la SImAU.',
                'image' =>  'upload/project2.jpg',
                'isprogramme' => 0,
            ],
            [
                'grand_titre' => "Présentation du Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo_principale' => "upload/realisation3.png",
                'petit_titre' => "Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo1' => 'upload/realisation2.png',
                'presentateur1' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo1' => 'Présentation du projet',
                'photo2' => 'upload/realisation4.png',
                'presentateur2' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo2' => 'Présentation du site',
                'photo3' => 'upload/realisation5.png',
                'presentateur3' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo3' => 'Présentation du projet',
                'description1' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada suspendisse tellus imperdiet pharetra a nisi felis nec, venenatis. Egestas lorem adipiscing non ac commodo malesuada vitae. Nibh urna, turpis in elementum non condimentum libero purus. Facilisis aenean donec id ullamcorper suspendisse. Vitae urna varius velit, venenatis, in tempus nunc. Dictum lacinia massa faucibus magna. Elementum interdum et sit dui massa elementum diam dictum. Scelerisque nec amet maecenas justo ut. Gravida lorem porta pharetra proin at tincidunt auctor. Enim ac pretium tristique sit id leo consectetur felis pharetra.
                ",
                'isDelete' => 0,
                'titre' => 'Construction du nouveau siège de l’Assemblée Nationale ',
                'description' => 'Le Gouvernement, à travers le Conseil des Ministres du 16 mai 2018, a pris acte de la volonté de la majorité des députés d’arrêter les travaux en cours du siège de l’Assemblée Nationale en vue de construire un nouvel édifice sur un autre site des suites des irrégularités constatées par l’audit commis à cet effet.
                Suite à cette décision et en exécution des instructions du Chef de l’État, le Ministère du Cadre de Vie et du Développement Durable en collaboration avec la Mairie de Porto-Novo a identifié le domaine de l’ex-Gendarmerie à proximité de l’actuel siège de l’Assemblée  Nationale pour  construire  un nouveau bâtiment pour le Parlement Béninois dans sa capitale administrative Porto-Novo.
                ',
                'image' =>  'upload/project2.jpg',
                'isprogramme' => 0,
            ],
            [
                'grand_titre' => "Présentation du Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo_principale' => "upload/realisation3.png",
                'petit_titre' => "Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo1' => 'upload/realisation2.png',
                'presentateur1' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo1' => 'Présentation du projet',
                'photo2' => 'upload/realisation4.png',
                'presentateur2' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo2' => 'Présentation du site',
                'photo3' => 'upload/realisation5.png',
                'presentateur3' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo3' => 'Présentation du projet',
                'description1' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada suspendisse tellus imperdiet pharetra a nisi felis nec, venenatis. Egestas lorem adipiscing non ac commodo malesuada vitae. Nibh urna, turpis in elementum non condimentum libero purus. Facilisis aenean donec id ullamcorper suspendisse. Vitae urna varius velit, venenatis, in tempus nunc. Dictum lacinia massa faucibus magna. Elementum interdum et sit dui massa elementum diam dictum. Scelerisque nec amet maecenas justo ut. Gravida lorem porta pharetra proin at tincidunt auctor. Enim ac pretium tristique sit id leo consectetur felis pharetra.
                ",
                'isDelete' => 0,
                'titre' => 'Construction de la Cité Administrative',
                'description' => 'Dans le cadre du Programme d’Action du Gouvernement (PAG) de 2016 à 2021, le ministère du cadre de vie et du développement durable a initié la construction d’une cité administrative qui vise à mettre un terme définitif aux baux pour loger les services de l’Etat. Ce projet s’inscrit dans le cadre de la construction de bâtiments administratifs regroupant les services publics afin d’optimiser leur fonctionnement, développer le patrimoine bâti de l’Etat et réaliser des économies d’échelle.',
                'image' =>  'upload/project2.jpg',
                'isprogramme' => 0,
            ],
            [
                'grand_titre' => "Présentation du Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo_principale' => "upload/realisation3.png",
                'petit_titre' => "Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo1' => 'upload/realisation2.png',
                'presentateur1' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo1' => 'Présentation du projet',
                'photo2' => 'upload/realisation4.png',
                'presentateur2' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo2' => 'Présentation du site',
                'photo3' => 'upload/realisation5.png',
                'presentateur3' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo3' => 'Présentation du projet',
                'description1' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada suspendisse tellus imperdiet pharetra a nisi felis nec, venenatis. Egestas lorem adipiscing non ac commodo malesuada vitae. Nibh urna, turpis in elementum non condimentum libero purus. Facilisis aenean donec id ullamcorper suspendisse. Vitae urna varius velit, venenatis, in tempus nunc. Dictum lacinia massa faucibus magna. Elementum interdum et sit dui massa elementum diam dictum. Scelerisque nec amet maecenas justo ut. Gravida lorem porta pharetra proin at tincidunt auctor. Enim ac pretium tristique sit id leo consectetur felis pharetra.
                ",
                'isDelete' => 0,
                'titre' => 'Construction de la Cité Ministérielle ',
                'description' => 'Pour le fonctionnement de l’administration béninoise notamment les Ministères, l’Etat fait une dépense énorme dans les baux des immeubles dans lesquels la plupart est logée. Le Gouvernement actuel projette donc la construction et l’exploitation d’une cité ministérielle à Cadjèhoun dans le 12ème arrondissement de Cotonou. Cette cité sera construite sur un domaine de 5 Ha et logera 20 Ministères. ',
                'image' =>  'upload/project2.jpg',
                'isprogramme' => 0,
            ],
            [
                'grand_titre' => "Présentation du Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo_principale' => "upload/realisation3.png",
                'petit_titre' => "Projet de Construction du Pôle Agroalimentaire de l’Agglomération du Grand Nokoué à Abomey – Calavi",
                'photo1' => 'upload/realisation2.png',
                'presentateur1' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo1' => 'Présentation du projet',
                'photo2' => 'upload/realisation4.png',
                'presentateur2' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo2' => 'Présentation du site',
                'photo3' => 'upload/realisation5.png',
                'presentateur3' => 'Hélias ZINZINDOHOUÉ',
                'titre_photo3' => 'Présentation du projet',
                'description1' => "
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada suspendisse tellus imperdiet pharetra a nisi felis nec, venenatis. Egestas lorem adipiscing non ac commodo malesuada vitae. Nibh urna, turpis in elementum non condimentum libero purus. Facilisis aenean donec id ullamcorper suspendisse. Vitae urna varius velit, venenatis, in tempus nunc. Dictum lacinia massa faucibus magna. Elementum interdum et sit dui massa elementum diam dictum. Scelerisque nec amet maecenas justo ut. Gravida lorem porta pharetra proin at tincidunt auctor. Enim ac pretium tristique sit id leo consectetur felis pharetra.
                ",
                'isDelete' => 0,
                'titre' => 'Construction du pôle agroalimentaire de l’agglomération du Grand Nokoué',
                'description' => 'Avec le développement démographique, urbain et économique, et la volonté de dynamiser le centre-ville de Cotonou, il est nécessaire de repenser la stratégie des flux de produits agroalimentaires, essentiellement concentré sur le marché de Dantokpa. Il est important de créer une véritable plateforme agroalimentaire à l’échelle régionale intégrant : le transfert des activités de gros des produits alimentaires, périssables ou non, depuis Dantokpa et ses alentours ; la transformation des produits agricoles (anacardes, ananas, etc.). Ce projet qui porte sur la construction du pôle agroalimentaire et de l’abattoir de l\'agglomération du Grand Nokoué avec toutes les infrastructures connexes, apportera des solutions sur la gestion de l’urbanisation, l\’aménagement du territoire et autres.
                ',
                'image' =>  'upload/project2.jpg',
                'isprogramme' => 0,
            ],

        ]);
    }
}
