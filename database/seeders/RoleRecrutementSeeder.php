<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleRecrutementSeeder extends Seeder
{
    public $listeRolesWithPermissions = [
        'Recrutement' => [
            'permissions' => [
                'index_recrutement',
                'create_recrutement',
                'update_recrutement',
                'delete_recrutement',
                'index_societe',
            ]
        ]
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->listeRolesWithPermissions as $key => $value) {
            $role =  Role::create(['name' => $key, 'libelle' => 'Recrutement']);
            foreach ($value['permissions'] as  $permission) {
                $permission = Permission::where('name', $permission)->first();
                $role->givePermissionTo($permission);
            }
        }
    }
}
