<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypologielogementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('typologielogements', function (Blueprint $table) {
            $table->id();
            $table->string('grandTitre')->nullable();
            $table->text('grandDescription')->nullable();
            $table->text('photo')->nullable();
            $table->text('petitTitre')->nullable();
            $table->text('petitDescription')->nullable();
            $table->text('fiche')->nullable();
            $table->text('video')->nullable();
            $table->foreignId('categorietypelogement_id')->nullable()->references('id')->on('categorietypelogements')->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('typologielogements');
    }
}
