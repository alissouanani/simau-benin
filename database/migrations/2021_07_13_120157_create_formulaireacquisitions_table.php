<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormulaireacquisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulaireacquisitions', function (Blueprint $table) {
            $table->id();
            $table->text('image')->nullable();
            $table->text('titre')->nullable();
            $table->text('descriptionTypologieLogementsCollectif')->nullable();
            $table->text('descriptionVenteDirecte')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulaireacquisitions');
    }
}
