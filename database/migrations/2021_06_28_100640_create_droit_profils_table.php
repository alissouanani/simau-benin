<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDroitProfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('droit_profils', function (Blueprint $table) {
            $table->id();
            $table->foreignId('droit_id')->nullable()->references('id')->on('droits')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('profil_id')->nullable()->references('id')->on('profils')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('droit_profils');
    }
}
