<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandes', function (Blueprint $table) {
            $table->id();
            $table->date('date_prise_charge')->nullable();
            $table->date('date_traitee')->nullable();
            $table->foreignId('mode_paiement_id')->nullable()->references('id')->on('mode_paiements')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('source_paiement_id')->nullable()->references('id')->on('sourcepaiements')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('demandeur_id')->nullable()->references('id')->on('demandeurs')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('statut_demande_id')->nullable()->references('id')->on('statut_demandes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('user_id')->nullable()->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandes');
    }
}
