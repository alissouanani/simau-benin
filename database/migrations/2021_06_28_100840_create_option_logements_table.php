<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionLogementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_logements', function (Blueprint $table) {
            $table->id();
            $table->string('libelle');
            $table->foreignId('type_logement_id')->nullable()->references('id')->on('type_logements')->onUpdate('cascade')->onDelete('cascade');
            $table->string('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_logements');
    }
}
