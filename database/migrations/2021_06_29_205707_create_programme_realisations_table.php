<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgrammeRealisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programme_realisations', function (Blueprint $table) {
            $table->id();
            $table->string('grand_titre')->nullable();
            $table->string('photo_principale')->nullable();
            $table->text('description1')->nullable();
            $table->string('petit_titre')->nullable();
            $table->string('photo1')->nullable();
            $table->text('presentateur1')->nullable();
            $table->string('titre_photo1')->nullable();
            $table->string('photo2')->nullable();
            $table->text('presentateur2')->nullable();
            $table->string('titre_photo2')->nullable();
            $table->string('photo3')->nullable();
            $table->text('presentateur3')->nullable();
            $table->string('titre_photo3')->nullable();
            $table->string('titre')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->boolean('isprogramme')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programme_realisations');
    }
}
