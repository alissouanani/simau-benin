<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionLogementDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_logement_demandes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('option_logement_id')->nullable()->references('id')->on('option_logements')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('demande_id')->nullable()->references('id')->on('demandes')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_logement_demandes');
    }
}
