<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaiementdaosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paiementdaos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('appel_offre_id')->references('id')->on('appel_offres')->onUpdate('cascade')->onDelete('cascade');
            $table->float('montant');
            $table->text('nomStructure')->nullable();
            $table->text('personneRessource')->nullable();
            $table->dateTime('datePaiement');
            $table->text('token');
            $table->string('statutPaiement');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paiementdaos');
    }
}
