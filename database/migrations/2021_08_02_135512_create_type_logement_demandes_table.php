<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeLogementDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_logement_demandes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('type_logement_id')->nullable()->references('id')->on('type_logements')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('demande_id')->nullable()->references('id')->on('demandes')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('localite_id')->nullable()->references('id')->on('localites')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_logement_demandes');
    }
}
