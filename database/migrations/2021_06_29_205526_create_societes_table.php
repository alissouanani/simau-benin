<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocietesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('societes', function (Blueprint $table) {
            $table->id();
            $table->string('logo')->nullable();
            $table->string('nom')->nullable();
            $table->text('adresse')->nullable();
            $table->string('telephone')->nullable();
            $table->text('description')->nullable();
            $table->string('image_centrale_actualite')->nullable();
            $table->string('image_centrale_apropos')->nullable();
            $table->string('image_centrale_recrutement')->nullable();
            $table->string('image_apropos')->nullable();
            $table->text('description_apropos')->nullable();
            $table->string('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('societes');
    }
}
