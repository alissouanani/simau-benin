<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresentationprojetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presentationprojets', function (Blueprint $table) {
            $table->id();
            $table->text('titre')->nullable();
            $table->text('description')->nullable();
            $table->text('individuels')->nullable();
            $table->text('collectifs')->nullable();
            $table->text('villes_concerne')->nullable();
            $table->text('maitreOuvrage')->nullable();
            $table->text('maitredelegue')->nullable();
            $table->text('coutProjet')->nullable();
            $table->text('ficheProjet')->nullable();
            $table->text('video')->nullable();
            $table->text('slides')->nullable();
            $table->text('cadre_institutionnel')->nullable();
            $table->text('chefProjet')->nullable();
            $table->text('zone_intervention')->nullable();
            $table->text('dateDemarrage')->nullable();
            $table->text('duree')->nullable();
            $table->text('etatAvancement')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presentationprojets');
    }
}
