<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModecommercialisationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modecommercialisations', function (Blueprint $table) {
            $table->id();
            $table->text('titre')->nullable();
            $table->text('photo')->nullable();
            $table->text('titreDescription')->nullable();
            $table->text('titre1')->nullable();
            $table->text('description1')->nullable();
            $table->text('titre2')->nullable();
            $table->text('description2')->nullable();
            $table->text('titre3')->nullable();
            $table->text('description3')->nullable();
            $table->text('image2')->nullable();
            $table->boolean('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modecommercialisations');
    }
}
