<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjetStandardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projet_standards', function (Blueprint $table) {
            $table->id();
            $table->text('slides')->nullable();
            $table->text('titre')->nullable();
            $table->text('resume')->nullable();
            $table->text('description')->nullable();
            $table->text('chefProjet')->nullable();
            $table->text('maitreOuvrage')->nullable();
            $table->text('coutProjet')->nullable();
            $table->text('ficheProjet')->nullable();
            $table->text('images')->nullable();
            $table->text('cadre_institutionnel')->nullable();
            $table->text('zone_intervention')->nullable();
            $table->text('titre_cadre_intitutionnel')->nullable();
            $table->text('description_cadre_intitutionnel')->nullable();
            $table->text('images_acceuil')->nullable();
            $table->string('url')->nullable();
            $table->string('blank')->nullable();
            $table->text('premiere_realisation')->nullable();
            $table->text('perspective')->nullable();
            $table->text('video')->nullable();
            $table->text('dateDemarrage')->nullable();
            $table->text('duree')->nullable();
            $table->text('etatAvancement')->nullable();
            $table->boolean('menu')->default(1);
            $table->boolean('isDelete')->default(0);
            $table->foreignId('categorie_projet_standard_id')->nullable()->references('id')->on('categorie_projet_standards');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projet_standards');
    }
}
