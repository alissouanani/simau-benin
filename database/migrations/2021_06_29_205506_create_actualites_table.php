<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActualitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actualites', function (Blueprint $table) {
            $table->id();
            $table->foreignId('categorie_actualite_id')->references('id')->on('categorie_actualites')->onUpdate('cascade')->onDelete('cascade')->default(1);
            $table->unsignedBigInteger('id_externe')->nullable()->default(0);
            $table->unsignedInteger('id_externe_position')->nullable()->default(0);
            $table->text('activite')->nullable();
            $table->text('titre')->nullable(); //grand titre
            $table->string('imagePrincipale')->nullable();
            $table->string('lien_de_la_source')->nullable();
            $table->date('date')->nullable();
            $table->longText('contenu')->nullable();
            $table->string('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actualites');
    }
}
