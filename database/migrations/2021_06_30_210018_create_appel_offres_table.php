<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppelOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appel_offres', function (Blueprint $table) {
            $table->id();
            $table->text('titre')->nullable();
            $table->text('description')->nullable();
            $table->text('files')->nullable();
            $table->text('activite')->nullable();
            $table->string('statut')->nullable();
            $table->date('dateDebut')->nullable();
            $table->date('dateFin')->nullable();
            $table->text('dateActivite')->nullable();
            $table->text('dateCloture')->nullable();
            $table->foreignId('categorieappel_id')->references('id')->on('categorieappels')->onUpdate('cascade')->onDelete('cascade');
            $table->string('isDelete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appel_offres');
    }
}
