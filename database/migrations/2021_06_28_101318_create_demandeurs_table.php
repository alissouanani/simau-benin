<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandeursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandeurs', function (Blueprint $table) {
            $table->id();
            $table->string('nom');
            $table->string('prenom');
            $table->date('date_de_naissance');
            $table->text('detail_profession')->nullable();
            //$table->string('ville_de_naissance')->nullable();
            //$table->foreignId('IDpays_naissance')->references('id')->on('pays')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('profession_id')->references('id')->on('professions')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('IDpays_nationalite')->references('id')->on('pays')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('IDpays_delivrance')->references('id')->on('pays')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('IDpays_residance')->references('id')->on('pays')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('IDdepartement_residence')->nullable()->references('id')->on('departements')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('type_piece_id')->references('id')->on('type_pieces')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('type_logement_id')->nullable()->references('id')->on('type_logements')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('categorie_pro')->references('id')->on('categorie_professionnelles')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->foreignId('localite_id')->references('id')->on('localites')->onUpdate('cascade')->onDelete('cascade');
            $table->string('Numero_piece')->nullable();
            $table->date('Date_delivrance_piece')->nullable();
            $table->date('Date_expiration_piece')->nullable();
            $table->foreignId('genre_id')->references('id')->on('genres')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('situation_matrimoniale_id')->references('id')->on('situation_matrimoniales')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('ville_id')->nullable()->references('id')->on('villes')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->string('Adresse_line1')->nullable();
            $table->string('Adresse_line2')->nullable();
            $table->string('Contacts')->nullable();
            $table->string('Emails')->nullable();
            $table->integer('nombre_annee_effectue')->nullable();
            $table->integer('nombre_annee_restant')->nullable();
            $table->foreignId('civilite_id')->references('id')->on('civilites')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandeurs');
    }
}
