<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actionnaires', function (Blueprint $table) {
            $table->id();
            $table->text('nom')->nullable();
            $table->text('description')->nullable();
            $table->string('site')->nullable();
            $table->decimal('pourcentage')->nullable();
            $table->string('logo')->nullable();
            $table->boolean('isDelete')->default();
            $table->foreignId('categorie_actionnaire_id')->nullable()->references('id')->on('categorie_actionnaires')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actionnaires');
    }
}
