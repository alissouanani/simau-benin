<?php

namespace App\Models;

use Prophecy\Call\Call;
use Illuminate\Database\Eloquent\Model;
use App\Models\CategorieProfessionnelle;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Profession extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function categorieProfessionelle ()
    {
        return $this->belongsTo(CategorieProfessionnelle::class, 'categorie_professionnelle_id');
    }
}
