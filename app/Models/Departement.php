<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function pays ()
    {
        return $this->belongsTo(Pays::class, 'pay_id');
    }
}
