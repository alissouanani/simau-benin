<?php

namespace App\Models;

use App\Models\Pays;
use App\Models\Genre;
use App\Models\Ville;
use App\Models\Civilite;
use App\Models\Localite;
use App\Models\TypePiece;
use App\Models\Departement;
use App\Models\TypeLogement;
use App\Models\SituationMatrimoniale;
use Illuminate\Database\Eloquent\Model;
use App\Models\CategorieProfessionnelle;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Demandeur extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function pays_naissance ()
    {
        return $this->belongsTo(Pays::class, 'IDpays_naissance');
    }

    public function pays_delivrance ()
    {
        return $this->belongsTo(Pays::class, 'IDpays_delivrance');
    }

    public function profession ()
    {
        return $this->belongsTo(Profession::class, 'profession_id');
    }

    public function pays_nationalite ()
    {
        return $this->belongsTo(Pays::class, 'IDpays_nationalite');
    }

    public function typePiece ()
    {
        return $this->belongsTo(TypePiece::class, 'type_piece_id');
    }

    public function genre ()
    {
        return $this->belongsTo(Genre::class, 'genre_id');
    }

    public function situationMatrimoniale ()
    {
        return $this->belongsTo(SituationMatrimoniale::class, 'situation_matrimoniale_id');
    }

    public function ville ()
    {
        return $this->belongsTo(Ville::class, 'ville_id');
    }

    public function civilite ()
    {
        return $this->belongsTo(Civilite::class, 'civilite_id');
    }

    public function pays_residence ()
    {
        return $this->belongsTo(Pays::class, 'IDpays_residance');
    }

    public function departement ()
    {
        return $this->belongsTo(Departement::class, 'IDdepartement_residence');
    }

    public function type_logement ()
    {
        return $this->belongsTo(TypeLogement::class, 'type_logement_id');
    }

    public function categorie_professionnelle()
    {
        return $this->belongsTo(CategorieProfessionnelle::class, 'categorie_pro');
    }

    public function localite()
    {
        return $this->belongsTo(Localite::class, 'localite_id');
    }

    public function demande(){
        return $this->belongsTo(Demande::class, 'id', 'demandeur_id');
    }

    public function getFullNameAttribute(){
        return $this->nom." ".$this->prenom;
    }
}
