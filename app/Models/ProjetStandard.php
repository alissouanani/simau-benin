<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjetStandard extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function categorie_projet_standard ()
    {
        return $this->belongsTo(CategorieProjetStandard::class, 'categorie_projet_standard_id');
    }
}
