<?php

namespace App\Models;

use App\Models\Demande;
use App\Models\OptionLogement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OptionLogementDemande extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function optionlogement ()
    {
        return $this->belongsTo(OptionLogement::class, 'option_logement_id');
    }

    public function demande ()
    {
        return $this->belongsTo(Demande::class, 'demande_id');
    }
}
