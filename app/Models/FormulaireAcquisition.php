<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormulaireAcquisition extends Model
{
    protected $table = 'formulaireacquisitions';
    use HasFactory;
    protected $guarded = [];
}
