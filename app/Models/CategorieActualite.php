<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorieActualite extends Model
{
    use HasFactory;
    public const ACTUALITE = 1;
    public const APPEL_OFFRE = 2;
    public const RECRUTEMENT = 3;

    protected $guarded = ['id'];
}
