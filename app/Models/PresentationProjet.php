<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PresentationProjet extends Model
{
    protected $table = 'presentationprojets';
    use HasFactory;
    protected $guarded = [];
}
