<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypologieLogement extends Model
{
    protected $table = 'typologielogements';
    use HasFactory;
    protected $guarded = [];
}
