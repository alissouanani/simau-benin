<?php

namespace App\Models;

use App\Models\User;
use App\Models\ModePaiement;
use App\Models\StatutDemande;
use App\Models\SourcePaiement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Demande extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function modepaiement ()
    {
        return $this->belongsTo(ModePaiement::class, 'mode_paiement_id');
    }

    public function source_paiement ()
    {
        return $this->belongsTo(SourcePaiement::class, 'mode_paiement_id');
    }

    public function demandeur ()
    {
        return $this->belongsTo(Demandeur::class, 'demandeur_id');
    }

    public function statutdemande ()
    {
        return $this->belongsTo(StatutDemande::class, 'statut_demande_id');
    }

    public function user ()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function option_demander(){
        return $this->belongsTo(OptionLogementDemande::class, 'id', 'demande_id');
    }
}
