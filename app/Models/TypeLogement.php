<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeLogement extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function localite ()
    {
        return $this->belongsTo(Localite::class, 'localite_id');
    }
}
