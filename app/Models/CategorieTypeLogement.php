<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorieTypeLogement extends Model
{
    protected $table = 'categorietypelogements';
    use HasFactory;
    protected $guarded = [];
}
