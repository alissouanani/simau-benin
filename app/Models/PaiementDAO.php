<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaiementDAO extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table='paiementdaos';
    public function appelOffre(){
        return $this->belongsTo(CategoriesAppel::class, 'id', 'appel_offre_id');
    }
}
