<?php

namespace App\Models;

use App\Models\TypeLogement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OptionLogement extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function type_logement ()
    {
        return $this->belongsTo(TypeLogement::class, 'type_logement_id');
    }
}
