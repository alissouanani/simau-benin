<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogementSociaux extends Model
{
    protected $table = 'logementsociauxs';
    use HasFactory;
    protected $guarded = [];
}
