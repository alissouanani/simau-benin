<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeLogementDemande extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function typeLogement ()
    {
        return $this->belongsTo(TypeLogement::class, 'type_logement_id');
    }
    public function demande ()
    {
        return $this->belongsTo(Demande::class, 'demande_id');
    }
    public function localites ()
    {
        return $this->belongsTo(Localite::class, 'localite_id');
    }
}
