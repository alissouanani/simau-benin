<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DroitProfil extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function profil ()
    {
        return $this->belongsTo(Profil::class, 'profil_id');
    }

    public function droit ()
    {
        return $this->belongsTo(Droit::class, 'droit_id');
    }
}
