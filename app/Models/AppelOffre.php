<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppelOffre extends Model
{
    use HasFactory;

    protected $guarded = [];
    public function categorieAppelOffre(){
        return $this->belongsTo(CategoriesAppel::class, 'id', 'categorieappel_id');
    }
}
