<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConseilAdministration extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function categorie_administration ()
    {
        return $this->belongsTo(CategorieAdministration::class, 'categorie_administration_id');
    }
}
