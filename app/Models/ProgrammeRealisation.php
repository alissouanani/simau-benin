<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgrammeRealisation extends Model
{
    use HasFactory;

    protected $guarded = [];
}
