<?php

namespace App\Models;

use App\Models\CategorieActualite;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Actualite extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function categorie(){
        return $this->hasOne(CategorieActualite::class, 'id', 'categorie_actualite_id');
    }
}
