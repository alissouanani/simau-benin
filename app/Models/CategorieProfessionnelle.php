<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategorieProfessionnelle extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $table = "categorie_professionnelles";
}
