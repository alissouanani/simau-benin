<?php

namespace App\Http\Livewire\Dashboard\Configuration\ProgrammeRealisation;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ProgrammeRealisation;

class ProgrammeRealisationComponent extends Component
{
    use WithFileUploads;
    public $titre;
    public $image;
    public $description;
    public $programme_realisation_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteProgrammeRealisations'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'titre',
        'programme_realisation_id',
        'image',
        'description',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->programme_realisation_id) {
            $this->validateOnly($fields, [
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validateOnly($fields, [
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',
            ]);
        }
    }
    public function storeProgrammeRealisation()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->programme_realisation_id) {
            $this->validate([
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validate([
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',
            ]);
        }

        $myProgrammeRealisation = new ProgrammeRealisation();
        // verification du variable programme_realisation_id pour savoir si c'est un enregistrement ou  modification

        if ($this->programme_realisation_id) {
            $myProgrammeRealisation = ProgrammeRealisation::findOrFail($this->programme_realisation_id);
        }
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameicon = time() . '.' . $this->image->extension();
        $pathicon = $this->image->storeAs(
            'ProgrammeRealisationImage',
            $filenameicon,
            'public'
        );
        $myProgrammeRealisation->image = $pathicon;
        $myProgrammeRealisation->titre = $this->titre;
        $myProgrammeRealisation->description = $this->description;

        $myProgrammeRealisation->save();

        // verification du variable programme_realisation_id pour envoir message d'enregistrement ou  de modification

        if ($this->programme_realisation_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeProgrammeRealisation');

        // return redirect()->route('admin.service-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->programme_realisation_id = $id;
        $myProgrammeRealisation = ProgrammeRealisation::findOrFail($this->programme_realisation_id);
        $this->titre = $myProgrammeRealisation->titre;
        $this->image = $myProgrammeRealisation->image;
        $this->description = $myProgrammeRealisation->description;

    }
    // recuperation de l'element a supprimer
    public function deleteProgrammeRealisation($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteProgrammeRealisations()
    {
        $myProgrammeRealisation = ProgrammeRealisation::findOrFail($this->deleteIdBeingRemoved);
        $myProgrammeRealisation->isDelete = 1;
        $myProgrammeRealisation->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce Programme Realisation à été supprimer']);
        // return redirect()->route('admin.service-index');

    }
    public function render()
    {
        $programmeRealisations = ProgrammeRealisation::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.programme-realisation.programme-realisation-component',[
            'programmeRealisations' => $programmeRealisations
        ]);
    }
}
