<?php

namespace App\Http\Livewire\Dashboard\Configuration\ProjetStandards;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ProjetStandard;
use App\Models\CategorieProjetStandard;

class ProjetStandardsCreateComponent extends Component
{
    use WithFileUploads;
    public $tableau_video =[], $tableau_cardre =[];
    public $titre;
    public $dateDemarrage;
    public $etatAvancement;
    public $menu;
    public $description;
    public $resume;
    public $zone_intervention;
    public $chefProjet;
    public $maitreOuvrage;
    public $coutProjet;
    public $categorie_projet_standard_id;
    public $slides;
    public $ficheProjet;
    public $duree;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
            'titre',
            'dateDemarrage',
            'duree',
            'etatAvancement',
            'menu',
            'description',
            'resume',
            'zone_intervention',
            'chefProjet',
            'maitreOuvrage',
            'coutProjet',
            'categorie_projet_standard_id',
            'slides',
            'video',
            'cadre_institutionnel',
            'ficheProjet',
        ]);

    }
    // Fonction de l'enregistrement

    public function storeProjetStandard()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'titre'=>'required',
                //'description'=>'required',
                'resume'=>'required',
                'zone_intervention'=>'required',
                'chefProjet'=>'required',
                'maitreOuvrage'=>'required',
                'coutProjet'=>'required',
            ]);

            foreach ($this->url_video as $key => $value) {

                $this->tableau_video = [$this->titre_video[$key], $this->url_video[$key]];
            }

            $tableau_video1 = $this->tableau_video;


            foreach ($this->titre_cadre_intitutionnel1 as $key => $value) {

                $this->tableau_cardre = [$this->titre_cadre_intitutionnel1[$key], $this->description_cadre_intitutionnel1[$key]];
            }

            $tableau_cardre1 = $this->tableau_cardre;

        $myProjetStandard = new ProjetStandard();
        // Modification et Stockage du pdf dans le dossier storage de public

        $filenameFichePrjot = time() . '.' . $this->image->extension();
        $pathFichePrjot = $this->image->storeAs(
            'ProjetStandardFichePrjot',
            $filenameFichePrjot,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->slides->extension();
        $pathImage = $this->slides->storeAs(
            'ProjetStandardSlides',
            $filenameImage,
            'public'
        );

        $myProjetStandard->slides = $pathImage;
        $myProjetStandard->ficheProjet = $pathFichePrjot;
        $myProjetStandard->titre = $this->titre;
        $myProjetStandard->dateDemarrage = $this->dateDemarrage;
        $myProjetStandard->duree = $this->duree;
        $myProjetStandard->etatAvancement = $this->etatAvancement;
        $myProjetStandard->menu = $this->menu;
        $myProjetStandard->description = $this->description;
        $myProjetStandard->resume = $this->resume;
        $myProjetStandard->zone_intervention = $this->zone_intervention;
        $myProjetStandard->chefProjet = $this->chefProjet;
        $myProjetStandard->maitreOuvrage = $this->maitreOuvrage;
        $myProjetStandard->coutProjet = $this->coutProjet;
        $myProjetStandard->categorie_projet_standard_id = $this->categorie_projet_standard_id;
        $myProjetStandard->video = json_encode($tableau_video1);
        $myProjetStandard->cadre_institutionnel = json_encode($tableau_cardre1);
        $myProjetStandard->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        $categorie_projet_standards = CategorieProjetStandard::where('isDelete', 0)->get();

        return view('livewire.dashboard.configuration.projet-standards.projet-standards-create-component',[
            'categorie_projet_standards' => $categorie_projet_standards,
        ]);
    }
}
