<?php

namespace App\Http\Livewire\Dashboard\Configuration\ProjetStandards;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ProjetStandard;
use App\Models\CategorieProjetStandard;

class ProjetStandardsEditeComponent extends Component
{
    use WithFileUploads;
    public $tableau_video =[], $tableau_cardre =[];
    public $titre;
    public $dateDemarrage;
    public $etatAvancement;
    public $menu;
    public $description;
    public $resume;
    public $zone_intervention;
    public $chefProjet;
    public $maitreOuvrage;
    public $coutProjet;
    public $categorie_projet_standard_id;
    public $slides;
    public $ficheProjet;
    public $duree;
    public $projet_standard_id;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
            'titre',
            'dateDemarrage',
            'duree',
            'etatAvancement',
            'menu',
            'description',
            'resume',
            'zone_intervention',
            'chefProjet',
            'maitreOuvrage',
            'coutProjet',
            'categorie_projet_standard_id',
            'slides',
            'video',
            'cadre_institutionnel',
            'ficheProjet',
        ]);

    }
    // recuperation de l'element a modifier

    public function mount($id) {
        $this->projet_standard_id = $id;
        $myProjetStandard = ProjetStandard::findOrFail($this->projet_standard_id);
        $this->ficheProjet = $myProjetStandard->ficheProjet;
        $this->etatAvancement =  $myProjetStandard->etatAvancement;
        $this->menu = $myProjetStandard->menu;
        $this->titre = $myProjetStandard->titre;
        $this->dateDemarrage = $myProjetStandard->dateDemarrage;
        $this->duree = $myProjetStandard->duree;
        $this->resume = $myProjetStandard->resume;
        $this->description = $myProjetStandard->description;
        $this->zone_intervention = $myProjetStandard->zone_intervention;
        $this->chefProjet = $myProjetStandard->chefProjet;
        $this->maitreOuvrage = $myProjetStandard->maitreOuvrage;
        $this->categorie_projet_standard_id = $myProjetStandard->categorie_projet_standard_id;
        $this->slides = $myProjetStandard->slides;
        $this->video = json_decode($myProjetStandard->video);
        $this->cadre_institutionnel = json_decode($myProjetStandard->cadre_institutionnel);

    }
    // Fonction de Modification

    public function updateProjetStandard()
    {
            $this->validate([
                'titre'=>'required',
                //'description'=>'required',
                'resume'=>'required',
                'zone_intervention'=>'required',
                'chefProjet'=>'required',
                'maitreOuvrage'=>'required',
                'coutProjet'=>'required',
            ]);

            foreach ($this->url_video as $key => $value) {

                $this->tableau_video = [$this->titre_video[$key], $this->url_video[$key]];
            }

            $tableau_video1 = $this->tableau_video;


            foreach ($this->titre_cadre_intitutionnel1 as $key => $value) {

                $this->tableau_cardre = [$this->titre_cadre_intitutionnel1[$key], $this->description_cadre_intitutionnel1[$key]];
            }

            $tableau_cardre1 = $this->tableau_cardre;
        // Modification et Stockage du pdf dans le dossier storage de public

        $myProjetStandard = ProjetStandard::findOrFail($this->projet_standard_id);
        $filenameFichePrjot = time() . '.' . $this->image->extension();
        $pathFichePrjot = $this->image->storeAs(
            'ProjetStandardFichePrjot',
            $filenameFichePrjot,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->slides->extension();
        $pathImage = $this->slides->storeAs(
            'ProjetStandardSlides',
            $filenameImage,
            'public'
        );

        $myProjetStandard->slides = $pathImage;
        $myProjetStandard->ficheProjet = $pathFichePrjot;
        $myProjetStandard->titre = $this->titre;
        $myProjetStandard->dateDemarrage = $this->dateDemarrage;
        $myProjetStandard->duree = $this->duree;
        $myProjetStandard->etatAvancement = $this->etatAvancement;
        $myProjetStandard->menu = $this->menu;
        $myProjetStandard->description = $this->description;
        $myProjetStandard->resume = $this->resume;
        $myProjetStandard->zone_intervention = $this->zone_intervention;
        $myProjetStandard->chefProjet = $this->chefProjet;
        $myProjetStandard->maitreOuvrage = $this->maitreOuvrage;
        $myProjetStandard->coutProjet = $this->coutProjet;
        $myProjetStandard->categorie_projet_standard_id = $this->categorie_projet_standard_id;
        $myProjetStandard->video = json_encode($tableau_video1);
        $myProjetStandard->cadre_institutionnel = json_encode($tableau_cardre1);
        $myProjetStandard->save();

       session()->flash('message', 'Modification effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        $categorie_projet_standards = CategorieProjetStandard::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.projet-standards.projet-standards-edite-component',[
            'categorie_projet_standards' => $categorie_projet_standards,
        ]);
    }
}
