<?php

namespace App\Http\Livewire\Dashboard\Configuration\ProjetStandards;

use App\Models\ProjetStandard;
use Livewire\Component;
use Livewire\WithPagination;

class ProjetStandardsComponent extends Component
{
    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteProjetStandards'];
    // recuperation de l'element a supprimer
    public function deleteProjetStandard($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteProjetStandards()
    {
        $myProjetStandard = ProjetStandard::findOrFail($this->deleteIdBeingRemoved);
        $myProjetStandard->isDelete = 1;
        $myProjetStandard->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Projet Standard à été supprimer']);

    }
    public function render()
    {
        $projetStandards = ProjetStandard::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.projet-standards.projet-standards-component',[
            'projetStandards' => $projetStandards,
        ]);
    }
}
