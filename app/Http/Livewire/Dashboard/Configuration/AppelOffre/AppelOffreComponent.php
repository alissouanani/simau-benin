<?php

namespace App\Http\Livewire\Dashboard\Configuration\AppelOffre;

use Livewire\Component;
use App\Models\AppelOffre;
use Livewire\WithPagination;

class AppelOffreComponent extends Component
{

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteAppelOffres'];
    // recuperation de l'element a supprimer
    public function deleteAppelOffre($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteAppelOffres()
    {
        $myAppelOffre = AppelOffre::findOrFail($this->deleteIdBeingRemoved);
        $myAppelOffre->isDelete = 1;
        $myAppelOffre->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Cet Appel Offre à été supprimer']);

    }
    public function render()
    {
        $appelOffres = AppelOffre::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.appel-offre.appel-offre-component',[
            "appelOffres" => $appelOffres,
        ]);
    }
}
