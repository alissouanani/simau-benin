<?php

namespace App\Http\Livewire\Dashboard\Configuration\AppelOffre;

use Livewire\Component;
use App\Models\AppelOffre;
use App\Models\SlideAccueil;
use Livewire\WithFileUploads;
use App\Models\CategoriesAppel;

class AppelOffreEditComponent extends Component
{
    use WithFileUploads;
    public $file;
    public $titre;
    public $activite;
    public $description;
    public $statut;
    public $dateDebut;
    public $dateFin;
    public $dateActivite;
    public $dateCloture;
    public $categorieappel_id;
    public $appel_Offres_id;



    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['file', 'titre','dateDebut', 'dateFin', 'description','categorieappel_id', 'dateCloture','statut', 'activite', 'dateActivite']);

    }
    // recuperation de l'element a modifier

    public function mount($id) {
        $this->appel_Offres_id = $id;
        $myAppelOffre = AppelOffre::findOrFail($this->appel_Offres_id);
        $this->titre = $myAppelOffre->titre;
        $this->file =  $myAppelOffre->file;
        $this->dateDebut = $myAppelOffre->dateDebut;
        $this->dateFin = $myAppelOffre->dateFin;
        $this->description = $myAppelOffre->description;
        $this->ticategorieappel_idtre = $myAppelOffre->categorieappel_id;
        $this->dateCloture =  $myAppelOffre->dateCloture;
        $this->statut = $myAppelOffre->statut;
        $this->activite = $myAppelOffre->activite;
        $this->dateActivite = $myAppelOffre->dateActivite;
    }
    // Fonction de Modification

    public function updateAppelOffre()
    {
             // $this->validate([
            //     'image' =>  'required',
            //     'titre' =>  'required',
            //     'dateDebut' =>  'required',
            //     'dateFin' =>  'required',
            //     'description' =>  'required',

            // ]);

        $myAppelOffre = AppelOffre::findOrFail($this->appel_Offres_id);
        // Modification et Stockage du pdf dans le dossier storage de public

        $filenamePDF = time() . '.' . $this->file->extension();
        $pathPDF = $this->file->storeAs(
            'PDFAppelOffres',
            $filenamePDF,
            'public'
        );
        $myAppelOffre->file = $pathPDF;
        $myAppelOffre->titre = $this->titre;
        $myAppelOffre->dateDebut = $this->dateDebut;
        $myAppelOffre->dateFin = $this->dateFin;
        $myAppelOffre->description = $this->description;
        $myAppelOffre->activite = $this->activite;
        $myAppelOffre->statut = $this->statut;
        $myAppelOffre->dateActivite = $this->dateActivite;
        $myAppelOffre->dateCloture = $this->dateCloture;
        $myAppelOffre->categorieappel_id = $this->categorieappel_id;
        $myAppelOffre->save();

        session()->flash('message', 'Modification effectué avec succès.');
        $this->resetInputFields();

    }
    public function render()
    {
        $categorieappels = CategoriesAppel::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.appel-offre.appel-offre-edit-component',[
            "categorieappels" => $categorieappels,
        ]);
    }
}
