<?php

namespace App\Http\Livewire\Dashboard\Configuration\AppelOffre;

use Livewire\Component;
use App\Models\AppelOffre;
use Livewire\WithFileUploads;
use App\Models\CategoriesAppel;

class AppelOffreCreateComponent extends Component
{
    use WithFileUploads;
    public $file;
    public $titre;
    public $activite;
    public $description;
    public $statut;
    public $dateDebut;
    public $dateFin;
    public $dateActivite;
    public $dateCloture;
    public $categorieappel_id;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['file', 'titre','dateDebut', 'dateFin', 'description','categorieappel_id', 'dateCloture','statut', 'activite', 'dateActivite']);

    }
    // Fonction de l'enregistrement
    public function storeAppelOffre()
    {
        // verification des variables lors de la l'enregistrement

            // $this->validate([
            //     'image' =>  'required',
            //     'titre' =>  'required',
            //     'dateDebut' =>  'required',
            //     'dateFin' =>  'required',
            //     'description' =>  'required',

            // ]);

        $mySlideAccueil = new AppelOffre();
        // Modification et Stockage du pdf dans le dossier storage de public

        $filenamePDF = time() . '.' . $this->file->extension();
        $pathPDF = $this->file->storeAs(
            'PDFAppelOffres',
            $filenamePDF,
            'public'
        );
        $mySlideAccueil->file = $pathPDF;
        $mySlideAccueil->titre = $this->titre;
        $mySlideAccueil->dateDebut = $this->dateDebut;
        $mySlideAccueil->dateFin = $this->dateFin;
        $mySlideAccueil->description = $this->description;
        $mySlideAccueil->activite = $this->activite;
        $mySlideAccueil->statut = $this->statut;
        $mySlideAccueil->dateActivite = $this->dateActivite;
        $mySlideAccueil->dateCloture = $this->dateCloture;
        $mySlideAccueil->categorieappel_id = $this->categorieappel_id;
        $mySlideAccueil->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        $categorieappels = CategoriesAppel::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.appel-offre.appel-offre-create-component',[
            "categorieappels" => $categorieappels,
        ]);
    }
}
