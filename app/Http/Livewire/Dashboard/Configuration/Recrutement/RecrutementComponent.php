<?php

namespace App\Http\Livewire\Dashboard\Configuration\Recrutement;

use Livewire\Component;
use App\Models\Recrutement;
use Livewire\WithPagination;

class RecrutementComponent extends Component
{
    use WithPagination;
    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteRecrutements'];
    // recuperation de l'element a supprimer
    public function deleteRecrutement($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteRecrutements()
    {
        $myRecrutement = Recrutement::findOrFail($this->deleteIdBeingRemoved);
        $myRecrutement->isDelete = 1;
        $myRecrutement->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Recrutement à été supprimer']);

    }
    public function render()
    {
        return view('livewire.dashboard.configuration.recrutement.recrutement-component');
    }
}
