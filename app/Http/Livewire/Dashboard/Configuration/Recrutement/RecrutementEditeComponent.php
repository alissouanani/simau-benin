<?php

namespace App\Http\Livewire\Dashboard\Configuration\Recrutement;

use Livewire\Component;

class RecrutementEditeComponent extends Component
{
    public function render()
    {
        return view('livewire.dashboard.configuration.recrutement.recrutement-edite-component');
    }
}
