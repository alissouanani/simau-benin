<?php

namespace App\Http\Livewire\Dashboard\Configuration\Recrutement;

use Livewire\Component;
use App\Models\Recrutement;
use Livewire\WithFileUploads;

class RecrutementCreateComponent extends Component
{
    use WithFileUploads;
    public $poste;
    public $files;
    public $description;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['poste', 'files','description']);

    }
    // Fonction de l'enregistrement

    public function storeRecrutement()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'poste' =>  'required',
                'files' =>  'required',
                'description' =>  'required',

            ]);

        $myRecrutement = new Recrutement();
        // Modification et Stockage du pdf dans le dossier storage de public

        $filenamePDF = time() . '.' . $this->files->extension();
        $pathPDF = $this->files->storeAs(
            'RecrutementPDF',
            $filenamePDF,
            'public'
        );
        $myRecrutement->files = $pathPDF;
        $myRecrutement->poste = $this->poste;
        $myRecrutement->description = $this->description;
        $myRecrutement->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.configuration.recrutement.recrutement-create-component');
    }
}
