<?php

namespace App\Http\Livewire\Dashboard\Configuration\Partenaire;

use Livewire\Component;
use App\Models\Partenaire;
use Livewire\WithFileUploads;

class PartenaireCreateComponent extends Component
{
    use WithFileUploads;
    public $logo;
    public $lien;
    public $libelle;
    public $description;
    public $prestataire = 0;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['logo', 'lien','libelle', 'prestataire', 'description']);

    }
    // Fonction de l'enregistrement

    public function storePartenaire()
    {
        // verification des variables lors de la l'enregistrement

        // dd($this->description);
        $this->validate([
            'logo' =>  'required',
            'lien' =>  'required',
            'libelle' =>  'required',
            'prestataire' =>  'required',
            'description' =>  'required',

            ]);

        $myPartenaire = new Partenaire();
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->logo->extension();
        $pathImage = $this->logo->storeAs(
            'LogoPartenaire',
            $filenameImage,
            'public'
        );
        $myPartenaire->logo = $pathImage;
        $myPartenaire->lien = $this->lien;
        $myPartenaire->libelle = $this->libelle;
        $myPartenaire->prestataire = $this->prestataire;
        $myPartenaire->description = $this->description;
        $myPartenaire->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.configuration.partenaire.partenaire-create-component');
    }
}
