<?php

namespace App\Http\Livewire\Dashboard\Configuration\Partenaire;

use App\Models\Partenaire;
use Livewire\Component;
use Livewire\WithPagination;

class PartenaireComponent extends Component
{
    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deletePartenaires'];
    // recuperation de l'element a supprimer
    public function deletePartenaire($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deletePartenaires()
    {
        $myPartenaire = Partenaire::findOrFail($this->deleteIdBeingRemoved);
        $myPartenaire->isDelete = 1;
        $myPartenaire->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Partenaire à été supprimer']);

    }
    public function render()
    {
        $partenaires = Partenaire::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.partenaire.partenaire-component',[
            "partenaires" => $partenaires,
        ]);
    }
}
