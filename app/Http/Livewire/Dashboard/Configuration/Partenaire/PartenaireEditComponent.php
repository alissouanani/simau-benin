<?php

namespace App\Http\Livewire\Dashboard\Configuration\Partenaire;

use Livewire\Component;
use App\Models\Partenaire;
use Livewire\WithFileUploads;

class PartenaireEditComponent extends Component
{
    use WithFileUploads;
    public $logo;
    public $lien;
    public $libelle;
    public $description;
    public $prestataire;
    public $partenaire_id;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['logo', 'lien','libelle', 'prestataire', 'description','partenaire_id']);

    }
    // recuperation de l'element a modifier

    public function mount($id) {

        $this->partenaire_id = $id;
        $myPartenaire = Partenaire::findOrFail($this->partenaire_id);
        $this->logo = $myPartenaire->logo;
        $this->lien =  $myPartenaire->lien;
        $this->libelle = $myPartenaire->libelle;
        $this->prestataire = $myPartenaire->prestataire;
        $this->description = $myPartenaire->description;
    }
    // Fonction de Modification

    public function updatePartenaire()
    {
            $this->validate([
                'logo' =>  'required',
                'lien' =>  'required',
                'libelle' =>  'required',
                'prestataire' =>  'required',
                'description' =>  'required',

            ]);

        $myPartenaire = Partenaire::findOrFail($this->partenaire_id);
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->logo->extension();
        $pathImage = $this->logo->storeAs(
            'LogoPartenaire',
            $filenameImage,
            'public'
        );
        $myPartenaire->logo = $pathImage;
        $myPartenaire->lien = $this->lien;
        $myPartenaire->libelle = $this->libelle;
        $myPartenaire->prestataire = $this->prestataire;
        $myPartenaire->description = $this->description;
        $myPartenaire->save();

        session()->flash('message', 'Modification effectué avec succès.');
        $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.configuration.partenaire.partenaire-edit-component');
    }
}
