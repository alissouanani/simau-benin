<?php

namespace App\Http\Livewire\Dashboard\Configuration\SlideAccueil;

use App\Models\SlideAccueil;
use Livewire\Component;
use Livewire\WithPagination;

class SlideAccueilComponent extends Component
{
    use WithPagination;
    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteSlideAccueils'];
    // recuperation de l'element a supprimer
    public function deleteSlideAccueil($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteSlideAccueils()
    {
        $mySlideAccueil = SlideAccueil::findOrFail($this->deleteIdBeingRemoved);
        $mySlideAccueil->isDelete = 1;
        $mySlideAccueil->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Slide Accueil à été supprimer']);

    }
    public function render()
    {
        $slideAccueils = SlideAccueil::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.slide-accueil.slide-accueil-component',[
            "slideAccueils" => $slideAccueils,
        ]);
    }
}
