<?php

namespace App\Http\Livewire\Dashboard\Configuration\SlideAccueil;

use Livewire\Component;
use App\Models\SlideAccueil;
use Livewire\WithFileUploads;

class SlideAccueilCreateComponent extends Component
{
    use WithFileUploads;
    public $image;
    public $titre;
    public $dateDebut;
    public $description;
    public $dateFin;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['image', 'titre','dateDebut', 'dateFin', 'description']);

    }
    // Fonction de l'enregistrement

    public function storeSlideAccueil()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'image' =>  'required',
                'titre' =>  'required',
                'dateDebut' =>  'required',
                'dateFin' =>  'required',
                'description' =>  'required',

            ]);

        $mySlideAccueil = new SlideAccueil();
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->image->extension();
        $pathImage = $this->image->storeAs(
            'SlideAccueils',
            $filenameImage,
            'public'
        );
        $mySlideAccueil->image = $pathImage;
        $mySlideAccueil->titre = $this->titre;
        $mySlideAccueil->dateDebut = $this->dateDebut;
        $mySlideAccueil->dateFin = $this->dateFin;
        $mySlideAccueil->description = $this->description;
        $mySlideAccueil->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.configuration.slide-accueil.slide-accueil-create-component');
    }
}
