<?php

namespace App\Http\Livewire\Dashboard\Configuration\SlideAccueil;

use Livewire\Component;
use App\Models\SlideAccueil;
use Livewire\WithFileUploads;

class SlideAccueilEditeComponent extends Component
{
    use WithFileUploads;
    public $image;
    public $titre;
    public $dateDebut;
    public $description;
    public $dateFin;
    public $slideAccueil_id;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['image', 'titre','dateDebut', 'dateFin', 'description']);

    }
    // recuperation de l'element a modifier

    public function mount($id) {
        $this->slideAccueil_id = $id;
        $mySlideAccueil = SlideAccueil::findOrFail($this->slideAccueil_id);
        $this->titre = $mySlideAccueil->titre;
        $this->image =  $mySlideAccueil->image;
        $this->dateDebut = $mySlideAccueil->dateDebut;
        $this->dateFin = $mySlideAccueil->dateFin;
        $this->description = $mySlideAccueil->description;
    }
    // Fonction de Modification

    public function updateSlideAccueil()
    {
            $this->validate([
                'image' =>  'required',
                'titre' =>  'required',
                'dateDebut' =>  'required',
                'dateFin' =>  'required',
                'description' =>  'required',

            ]);

        $mySlideAccueil = SlideAccueil::findOrFail($this->slideAccueil_id);
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->image->extension();
        $pathImage = $this->image->storeAs(
            'SlideAccueils',
            $filenameImage,
            'public'
        );
        $mySlideAccueil->image = $pathImage;
        $mySlideAccueil->titre = $this->titre;
        $mySlideAccueil->dateDebut = $this->dateDebut;
        $mySlideAccueil->dateFin = $this->dateFin;
        $mySlideAccueil->description = $this->description;
        $mySlideAccueil->save();

        session()->flash('message', 'Modification effectué avec succès.');
        $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.configuration.slide-accueil.slide-accueil-edite-component');
    }
}
