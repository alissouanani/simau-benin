<?php

namespace App\Http\Livewire\Dashboard\Configuration\ConseilAdministration;

use App\Models\CategorieAdministration;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ConseilAdministration;

class ConseilAdministrationCreateComponent extends Component
{
    use WithFileUploads;
    public $photo;
    public $nom;
    public $prenom;
    public $poste;
    public $categorie_administration_id;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['photo', 'nom','prenom', 'poste', 'categorie_administration_id']);

    }
    // Fonction de l'enregistrement

    public function storeConseilAdministration()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'photo' =>  'required',
                'nom' =>  'required',
                'prenom' =>  'required',
                'poste' =>  'required',
                'categorie_administration_id' =>  'required',

            ]);

        $myConseilAdministration = new ConseilAdministration();
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->photo->extension();
        $pathImage = $this->photo->storeAs(
            'LogoConseilAdministration',
            $filenameImage,
            'public'
        );
        $myConseilAdministration->photo = $pathImage;
        $myConseilAdministration->nom = $this->nom;
        $myConseilAdministration->prenom = $this->prenom;
        $myConseilAdministration->poste = $this->poste;
        $myConseilAdministration->categorie_administration_id = $this->categorie_administration_id;
        $myConseilAdministration->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        $categorie_administrations = CategorieAdministration::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.conseil-administration.conseil-administration-create-component',[
            'categorie_administrations' => $categorie_administrations,
        ]);
    }
}
