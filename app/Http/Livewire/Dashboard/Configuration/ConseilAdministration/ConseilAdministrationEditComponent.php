<?php

namespace App\Http\Livewire\Dashboard\Configuration\ConseilAdministration;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ConseilAdministration;
use App\Models\CategorieAdministration;

class ConseilAdministrationEditComponent extends Component
{
    use WithFileUploads;
    public $photo;
    public $nom;
    public $prenom;
    public $poste;
    public $categorie_administration_id;
    public $conseil_dministration_id;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['photo', 'nom','prenom', 'poste', 'categorie_administration_id','conseil_dministration_id']);

    }
    // recuperation de l'element a modifier

    public function mount($id) {

        $this->conseil_dministration_id = $id;
        $myConseilAdministration = ConseilAdministration::findOrFail($this->conseil_dministration_id);
        $this->photo = $myConseilAdministration->photo;
        $this->nom =  $myConseilAdministration->nom;
        $this->prenom = $myConseilAdministration->prenom;
        $this->poste = $myConseilAdministration->poste;
        $this->categorie_administration_id = $myConseilAdministration->categorie_administration_id;
    }
        // Fonction de Modification

    public function updateConseilAdministration()
    {
            $this->validate([
                'photo' =>  'required',
                'nom' =>  'required',
                'prenom' =>  'required',
                'poste' =>  'required',
                'categorie_administration_id' =>  'required',

            ]);

        $myConseilAdministration = ConseilAdministration::findOrFail($this->conseil_dministration_id);
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->photo->extension();
        $pathImage = $this->photo->storeAs(
            'LogoConseilAdministration',
            $filenameImage,
            'public'
        );
        $myConseilAdministration->photo = $pathImage;
        $myConseilAdministration->nom = $this->nom;
        $myConseilAdministration->prenom = $this->prenom;
        $myConseilAdministration->poste = $this->poste;
        $myConseilAdministration->categorie_administration_id = $this->categorie_administration_id;
        $myConseilAdministration->save();

        session()->flash('message', 'Modification effectué avec succès.');
        $this->resetInputFields();

    }
    public function render()
    {
        $categorie_administrations = CategorieAdministration::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.conseil-administration.conseil-administration-edit-component',[
            'categorie_administrations' => $categorie_administrations,
        ]);
    }
}
