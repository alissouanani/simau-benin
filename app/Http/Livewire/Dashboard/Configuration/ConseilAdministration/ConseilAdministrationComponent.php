<?php

namespace App\Http\Livewire\Dashboard\Configuration\ConseilAdministration;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\ConseilAdministration;

class ConseilAdministrationComponent extends Component
{
    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteConseilAdministrations'];
    // recuperation de l'element a supprimer
    public function deleteConseilAdministration($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteConseilAdministrations()
    {
        $myConseilAdministration = ConseilAdministration::findOrFail($this->deleteIdBeingRemoved);
        $myConseilAdministration->isDelete = 1;
        $myConseilAdministration->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Conseil Administration à été supprimer']);

    }
    public function render()
    {
        $conseilAdministrations = ConseilAdministration::where('isDelete', 0)->get();
        return view('livewire.dashboard.configuration.conseil-administration.conseil-administration-component',[
            "conseilAdministrations" => $conseilAdministrations,
        ]);
    }
}
