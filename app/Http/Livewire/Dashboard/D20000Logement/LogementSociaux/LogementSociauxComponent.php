<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\LogementSociaux;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\LogementSociaux;

class LogementSociauxComponent extends Component
{
    use WithFileUploads;
    public $image;
    public $description;
    public $logement_sociaux_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteLogementSociauxs'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'image',
        'logement_sociaux_id',
        'description',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->logement_sociaux_id) {
            $this->validateOnly($fields, [
                'image' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validateOnly($fields, [
                'image' => 'required',
                'description' => 'required',
            ]);
        }
    }
    public function storeLogementSociaux()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->logement_sociaux_id) {
            $this->validate([
                'image' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validate([
                'image' => 'required',
                'description' => 'required',
            ]);
        }

        $myLogementSociaux = new LogementSociaux();
        // verification du variable logement_sociaux_id pour savoir si c'est un enregistrement ou  modification

        if ($this->logement_sociaux_id) {
            $myLogementSociaux = LogementSociaux::findOrFail($this->logement_sociaux_id);
        }
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->image->extension();
        $pathImage = $this->image->storeAs(
            'ImageLogementSociaux',
            $filenameImage,
            'public'
        );
        $myLogementSociaux->image = $pathImage;
        $myLogementSociaux->description = $this->description;
        $myLogementSociaux->save();

        // verification du variable logement_sociaux_id pour envoir message d'enregistrement ou  de modification

        if ($this->logement_sociaux_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeLogementSociaux');

        return redirect()->route('admin.logementsociaux-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->logement_sociaux_id = $id;
        $myLogementSociaux = LogementSociaux::findOrFail($this->logement_sociaux_id);
        $this->image = $myLogementSociaux->image;
        $this->description = $myLogementSociaux->description;

    }
    // recuperation de l'element a supprimer
    public function deleteLogementSociaux($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteLogementSociauxs()
    {
        $myLogementSociaux = LogementSociaux::findOrFail($this->deleteIdBeingRemoved);
        $myLogementSociaux->isDelete = 1;
        $myLogementSociaux->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce Logement Social à été supprimer']);
        return redirect()->route('admin.logementsociaux-index');

    }
    public function render()
    {
        $logementSociaux = LogementSociaux::where('isDelete',0)->get();
        return view('livewire.dashboard.d20000-logement.logement-sociaux.logement-sociaux-component',[
            "logementSociaux" => $logementSociaux,
        ]);
    }
}
