<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\FormulaireAcquisitions;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\FormulaireAcquisition;

class FormulaireAcquisitionEditComponent extends Component
{
    use WithFileUploads;
    public $image;
    public $titre;
    public $descriptionTypologieLogementsCollectif;
    public $descriptionVenteDirecte;
    public $formulaire_acquisition_id;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['image', 'titre','descriptionTypologieLogementsCollectif', 'descriptionVenteDirecte','formulaire_acquisition_id']);

    }
    // recuperation de l'element a modifier

    public function mount($id) {
        $this->formulaire_acquisition_id = $id;
        $myFormulaireAcquisition = FormulaireAcquisition::findOrFail($this->formulaire_acquisition_id);
        $this->titre = $myFormulaireAcquisition->titre;
        $this->image =  $myFormulaireAcquisition->image;
        $this->descriptionTypologieLogementsCollectif = $myFormulaireAcquisition->descriptionTypologieLogementsCollectif;
        $this->descriptionVenteDirecte = $myFormulaireAcquisition->descriptionVenteDirecte;
    }
    // Fonction de Modification

    public function updateFormulaireAcquisition()
    {
            $this->validate([
                'descriptionTypologieLogementsCollectif' =>  'required',
                'titre' =>  'required',
                'image' =>  'required',
                'descriptionVenteDirecte' =>  'required',

            ]);

        $myFormulaireAcquisition = FormulaireAcquisition::findOrFail($this->formulaire_acquisition_id);
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->image->extension();
        $pathImage = $this->image->storeAs(
            'FormulaireAcquisitionImage',
            $filenameImage,
            'public'
        );

        $myFormulaireAcquisition->image = $pathImage;
        $myFormulaireAcquisition->descriptionTypologieLogementsCollectif = $this->descriptionTypologieLogementsCollectif;
        $myFormulaireAcquisition->titre = $this->titre;
        $myFormulaireAcquisition->descriptionVenteDirecte = $this->descriptionVenteDirecte;
        $myFormulaireAcquisition->save();

        session()->flash('message', 'Modification effectué avec succès.');
        $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.d20000-logement.formulaire-acquisitions.formulaire-acquisition-edit-component');
    }
}
