<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\FormulaireAcquisitions;

use App\Models\FormulaireAcquisition;
use Livewire\Component;
use Livewire\WithPagination;

class FormulaireAcquisitionComponent extends Component
{
    use WithPagination;
    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteFormulaireAcquisitions'];
    // recuperation de l'element a supprimer
    public function deleteFormulaireAcquisition($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteFormulaireAcquisitions()
    {
        $myFormulaireAcquisition = FormulaireAcquisition::findOrFail($this->deleteIdBeingRemoved);
        $myFormulaireAcquisition->isDelete = 1;
        $myFormulaireAcquisition->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Cet Formulaire Acquisition à été supprimer']);

    }
    public function render()
    {
        $formulaireAcquisitions = FormulaireAcquisition::where('isDelete', 0)->get();
        return view('livewire.dashboard.d20000-logement.formulaire-acquisitions.formulaire-acquisition-component',[
            "formulaireAcquisitions" => $formulaireAcquisitions,
        ]);
    }
}
