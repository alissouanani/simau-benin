<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\FormulaireAcquisitions;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\FormulaireAcquisition;

class FormulaireAcquisitionCreateComponent extends Component
{
    use WithFileUploads;
    public $image;
    public $titre;
    public $descriptionTypologieLogementsCollectif;
    public $descriptionVenteDirecte;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['image', 'titre','descriptionTypologieLogementsCollectif', 'descriptionVenteDirecte']);

    }
    // Fonction de l'enregistrement

    public function storeFormulaireAcquisition()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'descriptionTypologieLogementsCollectif' =>  'required',
                'titre' =>  'required',
                'image' =>  'required',
                'descriptionVenteDirecte' =>  'required',

            ]);

        $myFormulaireAcquisition = new FormulaireAcquisition();
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->image->extension();
        $pathImage = $this->image->storeAs(
            'FormulaireAcquisitionImage',
            $filenameImage,
            'public'
        );

        $myFormulaireAcquisition->image = $pathImage;
        $myFormulaireAcquisition->descriptionTypologieLogementsCollectif = $this->descriptionTypologieLogementsCollectif;
        $myFormulaireAcquisition->titre = $this->titre;
        $myFormulaireAcquisition->descriptionVenteDirecte = $this->descriptionVenteDirecte;
        $myFormulaireAcquisition->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.d20000-logement.formulaire-acquisitions.formulaire-acquisition-create-component');
    }
}
