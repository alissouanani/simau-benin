<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\AvantagesOffres;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\AvantageOffert;

class AvantagesOffreEditComponent extends Component
{
    use WithFileUploads;
    public $photo;
    public $titre;
    public $video;
    public $description;
    public $avantage_offert_id;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['photo', 'titre','video', 'description','avantage_offert_id']);

    }
    // recuperation de l'element a modifier

    public function mount($id) {
        $this->avantage_offert_id = $id;
        $myAvantageOffert = AvantageOffert::findOrFail($this->avantage_offert_id);
        $this->titre = $myAvantageOffert->titre;
        $this->photo =  $myAvantageOffert->photo;
        $this->video = $myAvantageOffert->video;
        $this->description = $myAvantageOffert->description;
    }
    // Fonction de Modification

    public function updateAvantageOffert()
    {
            $this->validate([
                'video' =>  'required',
                'titre' =>  'required',
                'photo' =>  'required',
                'description' =>  'required',

            ]);

        $myAvantageOffert = AvantageOffert::findOrFail($this->avantage_offert_id);
        // Modification et Stockage de l'image dans le dossier storage de public
        $filenameImage = time() . '.' . $this->photo->extension();
        $pathImage = $this->photo->storeAs(
            'AvantageOffertImage',
            $filenameImage,
            'public'
        );

        $myAvantageOffert->photo = $pathImage;
        $myAvantageOffert->video = $this->video;
        $myAvantageOffert->titre = $this->titre;
        $myAvantageOffert->description = $this->description;
        $myAvantageOffert->save();

        session()->flash('message', 'Modification effectué avec succès.');
        $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.d20000-logement.avantages-offres.avantages-offre-edit-component');
    }
}
