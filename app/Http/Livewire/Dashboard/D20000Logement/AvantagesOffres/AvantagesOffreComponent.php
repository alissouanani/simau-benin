<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\AvantagesOffres;

use App\Models\AvantageOffert;
use Livewire\Component;
use Livewire\WithPagination;

class AvantagesOffreComponent extends Component
{
    use WithPagination;
    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteAvantageOfferts'];
    // recuperation de l'element a supprimer
    public function deleteAvantageOffert($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteAvantageOfferts()
    {
        $myAvantageOffert = AvantageOffert::findOrFail($this->deleteIdBeingRemoved);
        $myAvantageOffert->isDelete = 1;
        $myAvantageOffert->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Cet Avantage Offert à été supprimer']);

    }
    public function render()
    {
        $avantageOfferts = AvantageOffert::where('isDelete', 0)->get();
        return view('livewire.dashboard.d20000-logement.avantages-offres.avantages-offre-component',
        [
            "avantageOfferts" => $avantageOfferts,
        ]);
    }
}
