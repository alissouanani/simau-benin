<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\AvantagesOffres;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\AvantageOffert;

class AvantagesOffreCreateComponent extends Component
{
    use WithFileUploads;
    public $photo;
    public $titre;
    public $video;
    public $description;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['photo', 'titre','video', 'description']);

    }
    // Fonction de l'enregistrement

    public function storeAvantageOffert()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'video' =>  'required',
                'titre' =>  'required',
                'photo' =>  'required',
                'description' =>  'required',

            ]);

        $myAvantageOffert = new AvantageOffert();
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->photo->extension();
        $pathImage = $this->photo->storeAs(
            'AvantageOffertImage',
            $filenameImage,
            'public'
        );

        $myAvantageOffert->photo = $pathImage;
        $myAvantageOffert->video = $this->video;
        $myAvantageOffert->titre = $this->titre;
        $myAvantageOffert->description = $this->description;
        $myAvantageOffert->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.d20000-logement.avantages-offres.avantages-offre-create-component');
    }
}
