<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\TypologieLogements;

use App\Models\TypologieLogement;
use Livewire\Component;
use Livewire\WithPagination;

class TypologieLogementComponent extends Component
{

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteTypologieLogements'];
    // recuperation de l'element a supprimer

    public function deleteTypologieLogement($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteTypologieLogements()
    {
        $myTypologieLogement = TypologieLogement::findOrFail($this->deleteIdBeingRemoved);
        $myTypologieLogement->isDelete = 1;
        $myTypologieLogement->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Typologie Logement à été supprimer']);

    }
    public function render()
    {
        $typologieLogements = TypologieLogement::wher('isDelete', 0)->get();
        return view('livewire.dashboard.d20000-logement.typologie-logements.typologie-logement-component',[
            'typologieLogements' => $typologieLogements,
        ]);
    }
}
