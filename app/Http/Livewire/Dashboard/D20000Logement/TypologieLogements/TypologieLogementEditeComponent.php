<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\TypologieLogements;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\TypologieLogement;
use App\Models\CategorieTypeLogement;

class TypologieLogementEditeComponent extends Component
{
    use WithFileUploads;
    public $grandTitre;
    public $grandDescription;
    public $video;
    public $photo;
    public $petitTitre;
    public $petitDescription;
    public $fiche;
    public $categorietypelogement_id;
    public $typologieLogement_id;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['photo', 'grandTitre','video', 'grandDescription','petitTitre','petitDescription', 'fiche','categorietypelogement_id']);

    }
    // recuperation de l'element a modifier

    public function mount($id) {
        $this->typologieLogement_id = $id;
        $myTypologieLogement = TypologieLogement::findOrFail($this->typologieLogement_id);
        $this->fiche = $myTypologieLogement->fiche;
        $this->photo =  $myTypologieLogement->photo;
        $this->video = $myTypologieLogement->video;
        $this->grandTitre = $myTypologieLogement->grandTitre;
        $this->grandDescription = $myTypologieLogement->grandDescription;
        $this->petitTitre =  $myTypologieLogement->petitTitre;
        $this->petitDescription = $myTypologieLogement->petitDescription;
        $this->categorietypelogement_id = $myTypologieLogement->categorietypelogement_id;
    }
// Fonction de Modification

    public function updateTypologieLogement()
    {
            $this->validate([
                'video' =>  'required',
                'grandTitre' =>  'required',
                'photo' =>  'required',
                'grandDescription' =>  'required',
                'petitTitre' =>  'required',
                'petitDescription' =>  'required',
                'fiche' =>  'required',
                'categorietypelogement_id' =>  'required',

            ]);
        $myTypologieLogement = TypologieLogement::findOrFail($this->typologieLogement_id);
        // Modification et Stockage du pdf dans le dossier storage de public

        $filenamefiche = time() . '.' . $this->fiche->extension();
        $pathfiche = $this->fiche->storeAs(
            'TypologieLogementfiche',
            $filenamefiche,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public
        $filenameImage = time() . '.' . $this->photo->extension();
        $pathImage = $this->photo->storeAs(
            'TypologieLogementImage',
            $filenameImage,
            'public'
        );

        $myTypologieLogement->photo = $pathImage;
        $myTypologieLogement->video = $this->video;
        $myTypologieLogement->grandTitre = $this->grandTitre;
        $myTypologieLogement->grandDescription = $this->grandDescription;
        $myTypologieLogement->fiche = $pathfiche;
        $myTypologieLogement->categorietypelogement_id = $this->categorietypelogement_id;
        $myTypologieLogement->petitDescription = $this->petitDescription;
        $myTypologieLogement->petitTitre = $this->petitTitre;
        $myTypologieLogement->save();

        session()->flash('message', 'Modification effectué avec succès.');
        $this->resetInputFields();

    }
    public function render()
    {
        $categorietypelogements = CategorieTypeLogement::where('isDelete', 0)->get();

        return view('livewire.dashboard.d20000-logement.typologie-logements.typologie-logement-edite-component',[
            'categorietypelogements' => $categorietypelogements,
        ]);
    }
}
