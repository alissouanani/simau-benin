<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\EnvergueProjet;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ProgrammeNational;

class EnvergueProjetComponent extends Component
{
    use WithFileUploads;
    public $titre;
    public $image;
    public $description;
    public $programme_national_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteProgrammeNationals'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'titre',
        'programme_national_id',
        'image',
        'description',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->programme_national_id) {
            $this->validateOnly($fields, [
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validateOnly($fields, [
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',
            ]);
        }
    }
    public function storeProgrammeNational()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->programme_national_id) {
            $this->validate([
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validate([
                'titre' => 'required',
                'image' => 'required',
                'description' => 'required',
            ]);
        }

        $myProgrammeNational = new ProgrammeNational();
        // verification du variable programme_national_id pour savoir si c'est un enregistrement ou  modification

        if ($this->programme_national_id) {
            $myProgrammeNational = ProgrammeNational::findOrFail($this->programme_national_id);
        }
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameicon = time() . '.' . $this->image->extension();
        $pathicon = $this->image->storeAs(
            'ProgrammeNationalImage',
            $filenameicon,
            'public'
        );
        $myProgrammeNational->image = $pathicon;
        $myProgrammeNational->titre = $this->titre;
        $myProgrammeNational->description = $this->description;

        $myProgrammeNational->save();

        // verification du variable programme_national_id pour envoir message d'enregistrement ou  de modification

        if ($this->programme_national_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeProgrammeNational');

        // return redirect()->route('admin.service-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->programme_national_id = $id;
        $myProgrammeNational = ProgrammeNational::findOrFail($this->programme_national_id);
        $this->titre = $myProgrammeNational->titre;
        $this->image = $myProgrammeNational->image;
        $this->description = $myProgrammeNational->description;

    }
    // recuperation de l'element a supprimer
    public function deleteProgrammeNational($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteProgrammeNationals()
    {
        $myProgrammeNational = ProgrammeNational::findOrFail($this->deleteIdBeingRemoved);
        $myProgrammeNational->isDelete = 1;
        $myProgrammeNational->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce Programme National à été supprimer']);
        // return redirect()->route('admin.service-index');

    }
    public function render()
    {
        $enverguePrjets = ProgrammeNational::where('isDelete',0)->get();
        return view('livewire.dashboard.d20000-logement.envergue-projet.envergue-projet-component',[
            'enverguePrjets' => $enverguePrjets,
        ]);
    }
}
