<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\PresentationProjet;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\PresentationProjet;

class PresentationProjetEditeComponent extends Component
{
    use WithFileUploads;
    public $tableau_video =[], $tableau_cardre =[];
    public $titre;
    public $dateDemarrage;
    public $etatAvancement;
    public $description;
    public $resume;
    public $zone_intervention;
    public $chefProjet;
    public $maitreOuvrage;
    public $coutProjet;
    public $slides;
    public $ficheProjet;
    public $duree;
    public $presentation_projet_id;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
            'titre',
            'dateDemarrage',
            'duree',
            'etatAvancement',
            'description',
            'resume',
            'zone_intervention',
            'chefProjet',
            'maitreOuvrage',
            'coutProjet',
            'slides',
            'video',
            'cadre_institutionnel',
            'ficheProjet',
        ]);

    }
    // recuperation de l'element a modifier

    public function mount($id) {
        $this->presentation_projet_id = $id;
        $myPresentationProjet = PresentationProjet::findOrFail($this->presentation_projet_id);
        $this->ficheProjet = $myPresentationProjet->ficheProjet;
        $this->etatAvancement =  $myPresentationProjet->etatAvancement;
        $this->titre = $myPresentationProjet->titre;
        $this->dateDemarrage = $myPresentationProjet->dateDemarrage;
        $this->duree = $myPresentationProjet->duree;
        $this->resume = $myPresentationProjet->resume;
        $this->description = $myPresentationProjet->description;
        $this->zone_intervention = $myPresentationProjet->zone_intervention;
        $this->chefProjet = $myPresentationProjet->chefProjet;
        $this->maitreOuvrage = $myPresentationProjet->maitreOuvrage;
        $this->slides = $myPresentationProjet->slides;
        $this->video = json_decode($myPresentationProjet->video);
        $this->cadre_institutionnel = json_decode($myPresentationProjet->cadre_institutionnel);

    }
// Fonction de Modification

    public function updatePresentationProjet()
    {
            $this->validate([
                'titre'=>'required',
                //'description'=>'required',
                'resume'=>'required',
                'zone_intervention'=>'required',
                'chefProjet'=>'required',
                'maitreOuvrage'=>'required',
                'coutProjet'=>'required',
            ]);

            foreach ($this->url_video as $key => $value) {

                $this->tableau_video = [$this->titre_video[$key], $this->url_video[$key]];
            }

            $tableau_video1 = $this->tableau_video;


            foreach ($this->titre_cadre_intitutionnel1 as $key => $value) {

                $this->tableau_cardre = [$this->titre_cadre_intitutionnel1[$key], $this->description_cadre_intitutionnel1[$key]];
            }

            $tableau_cardre1 = $this->tableau_cardre;

        $myPresentationProjet = PresentationProjet::findOrFail($this->presentation_projet_id);
        // Modification et Stockage du pdf dans le dossier storage de public

        $filenameFichePrjot = time() . '.' . $this->ficheProjet->extension();
        $pathFichePrjot = $this->ficheProjet->storeAs(
            'ProjetStandardFichePrjot',
            $filenameFichePrjot,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->slides->extension();
        $pathImage = $this->slides->storeAs(
            'ProjetStandardSlides',
            $filenameImage,
            'public'
        );

        $myPresentationProjet->slides = $pathImage;
        $myPresentationProjet->ficheProjet = $pathFichePrjot;
        $myPresentationProjet->titre = $this->titre;
        $myPresentationProjet->dateDemarrage = $this->dateDemarrage;
        $myPresentationProjet->duree = $this->duree;
        $myPresentationProjet->etatAvancement = $this->etatAvancement;
        $myPresentationProjet->menu = $this->menu;
        $myPresentationProjet->description = $this->description;
        $myPresentationProjet->resume = $this->resume;
        $myPresentationProjet->zone_intervention = $this->zone_intervention;
        $myPresentationProjet->chefProjet = $this->chefProjet;
        $myPresentationProjet->maitreOuvrage = $this->maitreOuvrage;
        $myPresentationProjet->coutProjet = $this->coutProjet;
        $myPresentationProjet->categorie_projet_standard_id = $this->categorie_projet_standard_id;
        $myPresentationProjet->video = json_encode($tableau_video1);
        $myPresentationProjet->cadre_institutionnel = json_encode($tableau_cardre1);
        $myPresentationProjet->save();

       session()->flash('message', 'Modification effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.d20000-logement.presentation-projet.presentation-projet-edite-component');
    }
}
