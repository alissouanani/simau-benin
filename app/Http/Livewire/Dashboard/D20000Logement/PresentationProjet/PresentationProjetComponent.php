<?php

namespace App\Http\Livewire\Dashboard000Logement\PresentationProjet;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\PresentationProjet;

class PresentationProjetComponent extends Component
{

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deletePresentationProjets'];
    // recuperation de l'element a supprimer

    public function deleteProjetStandard($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteProjetStandards()
    {
        $myPresentationProjet = PresentationProjet::findOrFail($this->deleteIdBeingRemoved);
        $myPresentationProjet->isDelete = 1;
        $myPresentationProjet->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Presentation Projet à été supprimer']);

    }
    public function render()
    {
        $presentationProjets = PresentationProjet::where('isDelete', 0)->get();
        return view('livewire.dashboard.20000-logement.presentation-projet.presentation-projet-component',[
            'presentationProjets' => $presentationProjets,
        ]);
    }
}
