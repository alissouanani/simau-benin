<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\PresentationProjet;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\PresentationProjet;

class PresentationProjetCreateComponent extends Component
{
    use WithFileUploads;
    public $tableau_video =[], $tableau_cardre =[];
    public $titre;
    public $dateDemarrage;
    public $etatAvancement;
    public $description;
    public $resume;
    public $zone_intervention;
    public $chefProjet;
    public $maitreOuvrage;
    public $coutProjet;
    public $slides;
    public $ficheProjet;
    public $duree;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
            'titre',
            'dateDemarrage',
            'duree',
            'etatAvancement',
            'description',
            'resume',
            'zone_intervention',
            'chefProjet',
            'maitreOuvrage',
            'coutProjet',
            'slides',
            'video',
            'cadre_institutionnel',
            'ficheProjet',
        ]);

    }
    // Fonction de l'enregistrement

    public function storePresentationProjet()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'titre'=>'required',
                //'description'=>'required',
                'resume'=>'required',
                'zone_intervention'=>'required',
                'chefProjet'=>'required',
                'maitreOuvrage'=>'required',
                'coutProjet'=>'required',
            ]);

            foreach ($this->url_video as $key => $value) {

                $this->tableau_video = [$this->titre_video[$key], $this->url_video[$key]];
            }

            $tableau_video1 = $this->tableau_video;


            foreach ($this->titre_cadre_intitutionnel1 as $key => $value) {

                $this->tableau_cardre = [$this->titre_cadre_intitutionnel1[$key], $this->description_cadre_intitutionnel1[$key]];
            }

            $tableau_cardre1 = $this->tableau_cardre;

        $myPresentationProjet = new PresentationProjet();
        // Modification et Stockage du pdf dans le dossier storage de public

        $filenameFichePrjot = time() . '.' . $this->ficheProjet->extension();
        $pathFichePrjot = $this->ficheProjet->storeAs(
            'PresentationProjetFichePrjot',
            $filenameFichePrjot,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->slides->extension();
        $pathImage = $this->slides->storeAs(
            'PresentationProjetSlides',
            $filenameImage,
            'public'
        );

        $myPresentationProjet->slides = $pathImage;
        $myPresentationProjet->ficheProjet = $pathFichePrjot;
        $myPresentationProjet->titre = $this->titre;
        $myPresentationProjet->dateDemarrage = $this->dateDemarrage;
        $myPresentationProjet->duree = $this->duree;
        $myPresentationProjet->etatAvancement = $this->etatAvancement;
        $myPresentationProjet->description = $this->description;
        $myPresentationProjet->resume = $this->resume;
        $myPresentationProjet->zone_intervention = $this->zone_intervention;
        $myPresentationProjet->maitreOuvrage = $this->maitreOuvrage;
        $myPresentationProjet->coutProjet = $this->coutProjet;
        $myPresentationProjet->video = json_encode($tableau_video1);
        $myPresentationProjet->cadre_institutionnel = json_encode($tableau_cardre1);
        $myPresentationProjet->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.d20000-logement.presentation-projet.presentation-projet-create-component');
    }
}
