<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\ModeCommercialisation;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ModeCommercialisation;

class ModeCommercialisationCreateComponent extends Component
{
    use WithFileUploads;
    public $titreDescription;
    public $titre;
    public $titre1;
    public $photo;
    public $titre2;
    public $titre3;
    public $description1;
    public $description2;
    public $description3;


    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['photo', 'titre','titre1', 'titreDescription','titre2','titre3', 'description1','description2','description3']);

    }
    // Fonction de l'enregistrement

    public function storeModeCommercialisation()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'titreDescription' =>  'required',
                'titre' =>  'required',
                'photo' =>  'required',
                'titre1' =>  'required',
                'titre2' =>  'required',
                'titre3' =>  'required',
                'description1' =>  'required',
                'description2' =>  'required',
                'description3' =>  'required',
            ]);

        $myModeCommercialisation = new ModeCommercialisation();
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameImage = time() . '.' . $this->photo->extension();
        $pathImage = $this->photo->storeAs(
            'ModeCommercialisationImage',
            $filenameImage,
            'public'
        );

        $myModeCommercialisation->photo = $pathImage;
        $myModeCommercialisation->titre = $this->titre;
        $myModeCommercialisation->titre1 = $this->titre1;
        $myModeCommercialisation->titre2 = $this->titre2;
        $myModeCommercialisation->titre3 = $this->titre3;
        $myModeCommercialisation->titreDescription = $this->titreDescription;
        $myModeCommercialisation->description1 = $this->description1;
        $myModeCommercialisation->description2 = $this->description2;
        $myModeCommercialisation->description3 = $this->description3;

        $myModeCommercialisation->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }

    public function render()
    {
        return view('livewire.dashboard.d20000-logement.mode-commercialisation.mode-commercialisation-create-component');
    }
}
