<?php

namespace App\Http\Livewire\Dashboard\D20000Logement\ModeCommercialisation;

use App\Models\ModeCommercialisation;
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\TypologieLogement;

class ModeCommercialisationComponent extends Component
{

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteModeCommercialisations'];
    // recuperation de l'element a supprimer

    public function deleteModeCommercialisation($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer

    public function deleteModeCommercialisations()
    {
        $myModeCommercialisation = ModeCommercialisation::findOrFail($this->deleteIdBeingRemoved);
        $myModeCommercialisation->isDelete = 1;
        $myModeCommercialisation->save();
        $this->dispatchBrowserEvent('deleted',['message' => 'Ce Mode Commercialisation à été supprimer']);

    }
    public function render()
    {
        $modeCommercialisations = ModeCommercialisation::where('isDelete', 0)->get();
        return view('livewire.dashboard.d20000-logement.mode-commercialisation.mode-commercialisation-component',[
            'modeCommercialisations' => $modeCommercialisations,
        ]);
    }
}
