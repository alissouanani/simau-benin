<?php

namespace App\Http\Livewire\Dashboard\Prereservation\SituationMatrimoniale;

use App\Models\SituationMatrimoniale;
use Livewire\Component;

class SituationMatrimonialeComponent extends Component
{
    public $libelle;
    public $situation_matrimoniale_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteSituationMatrimoniales'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'situation_matrimoniale_id',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->situation_matrimoniale_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        }
    }
    public function storeSituationMatrimoniale()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->situation_matrimoniale_id) {
            $this->validate([
                'libelle' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
            ]);
        }

        $myStuationMatrimoniale = new SituationMatrimoniale();
        // verification du variable situation_matrimoniale_id pour savoir si c'est un enregistrement ou  modification

        if ($this->situation_matrimoniale_id) {
            $myStuationMatrimoniale = SituationMatrimoniale::findOrFail($this->situation_matrimoniale_id);
        }
        $myStuationMatrimoniale->Libelle = $this->libelle;
        $myStuationMatrimoniale->save();

        // verification du variable situation_matrimoniale_id pour envoir message d'enregistrement ou  de modification

        if ($this->situation_matrimoniale_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeSituationMatrimoniale');

        return redirect()->route('admin.situationmatrimoniale-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->situation_matrimoniale_id = $id;
        $myStuationMatrimoniale = SituationMatrimoniale::findOrFail($this->situation_matrimoniale_id);
        $this->libelle = $myStuationMatrimoniale->Libelle;
    }
    // recuperation de l'element a supprimer
    public function deleteSituationMatrimoniale($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteSituationMatrimoniales()
    {
        $myStuationMatrimoniale = SituationMatrimoniale::findOrFail($this->deleteIdBeingRemoved);
        $myStuationMatrimoniale->isDelete = 1;
        $myStuationMatrimoniale->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Cette situation matrimoniale à été supprimer']);
        return redirect()->route('admin.situationmatrimoniale-index');

    }
    public function render()
    {
        $stuationMatrimoniales = SituationMatrimoniale::where('isDelete',0)->get();
        return view('livewire.dashboard.prereservation.situation-matrimoniale.situation-matrimoniale-component',[
            "stuationMatrimoniales" => $stuationMatrimoniales,
        ]);
    }
}
