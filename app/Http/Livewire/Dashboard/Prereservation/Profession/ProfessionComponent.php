<?php

namespace App\Http\Livewire\Dashboard\Prereservation\Profession;

use Livewire\Component;
use App\Models\Profession;
use App\Models\CategorieProfessionnelle;

class ProfessionComponent extends Component
{
    public $libelle;
    public $Categorie_professionnelle_id;
    public $profession_id;


    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteProfessions'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'profession_id',
        'Categorie_professionnelle_id'
        ]);
    }

    public function updated($fields)
    {
    // fonction verification des variables lors de la modification

        if ($this->profession_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'Categorie_professionnelle_id' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'Categorie_professionnelle_id' => 'required',
            ]);
        }
    }
    public function storeProfession()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->profession_id) {
            $this->validate([
                'libelle' => 'required',
                'Categorie_professionnelle_id' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
                'Categorie_professionnelle_id' => 'required',
            ]);
        }

        $myProfession = new Profession();
        // verification du variable profession_id pour envoir message d'enregistrement ou  de modification

        if ($this->profession_id) {
            $myProfession = Profession::findOrFail($this->profession_id);
        }
        $myProfession->Libelle = $this->libelle;
        $myProfession->Categorie_professionnelle_id = $this->Categorie_professionnelle_id;
        $myProfession->save();

        // vider les champs apres l'enregistrement ou la modification

        if ($this->profession_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }

        $this->resetInputFields();
        // mettre ajout

        $this->emit('storeProfession');

        return redirect()->route('admin.profession-index');


    }
    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->profession_id = $id;
        $myProfession = Profession::findOrFail($this->profession_id);
        $this->libelle = $myProfession->Libelle;
        $this->pay_id = $myProfession->pay_id;
    }
    // recuperation de l'element a supprimer

    public function deleteProfession($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteProfessions()
    {
        $myProfession = Profession::findOrFail($this->deleteIdBeingRemoved);
        $myProfession->isDelete = 1;
        $myProfession->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Cette profession à été supprimer']);
        return redirect()->route('admin.profession-index');

    }
    public function render()
    {
        $categorieProfessionnelles = CategorieProfessionnelle::where('isDelete', 0)->get();
        $professions = Profession::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.profession.profession-component',[
            "professions" => $professions,
            "categorieProfessionnelles" => $categorieProfessionnelles,
        ]);
    }
}
