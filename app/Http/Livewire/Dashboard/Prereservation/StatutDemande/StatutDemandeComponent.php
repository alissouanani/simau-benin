<?php

namespace App\Http\Livewire\Dashboard\Prereservation\StatutDemande;

use App\Models\StatutDemande;
use Livewire\Component;

class StatutDemandeComponent extends Component
{
    public $libelle;
    public $statut_demande_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteStatutDemandes'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'statut_demande_id',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->statut_demande_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        }
    }
    public function storeStatutDemande()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->statut_demande_id) {
            $this->validate([
                'libelle' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
            ]);
        }

        $myStatutDemande = new StatutDemande();
        // verification du variable statut_demande_id pour savoir si c'est un enregistrement ou  modification

        if ($this->statut_demande_id) {
            $myStatutDemande = StatutDemande::findOrFail($this->statut_demande_id);
        }
        $myStatutDemande->libelle = $this->libelle;
        $myStatutDemande->save();

        // verification du variable statut_demande_id pour envoir message d'enregistrement ou  de modification

        if ($this->statut_demande_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeStatutDemande');

        return redirect()->route('admin.statutdemande-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->statut_demande_id = $id;
        $myStatutDemande = StatutDemande::findOrFail($this->statut_demande_id);
        $this->libelle = $myStatutDemande->libelle;
    }
    // recuperation de l'element a supprimer
    public function deleteStatutDemande($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteStatutDemandes()
    {
        $myStatutDemande = StatutDemande::findOrFail($this->deleteIdBeingRemoved);
        $myStatutDemande->isDelete = 1;
        $myStatutDemande->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce statut de demande à été supprimer']);
        return redirect()->route('admin.statutdemande-index');

    }
    public function render()
    {
        $statutDemandes = StatutDemande::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.statut-demande.statut-demande-component',[
            "statutDemandes" => $statutDemandes,
        ]);
    }
}
