<?php

namespace App\Http\Livewire\Dashboard\Prereservation\Departement;

use App\Models\Pays;
use Livewire\Component;
use App\Models\Departement;

class DepartementComponent extends Component
{
    public $libelle;
    public $pay_id;
    public $departement_id;


    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteDepartements'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'departement_id',
        'pay_id'
        ]);
    }

    public function updated($fields)
    {
    // fonction verification des variables lors de la modification

        if ($this->departement_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'pay_id' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'pay_id' => 'required',
            ]);
        }
    }
    public function storeDepartement()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->departement_id) {
            $this->validate([
                'libelle' => 'required',
                'pay_id' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
                'pay_id' => 'required',
            ]);
        }

        $myDepartement = new Departement();
        // verification du variable departement_id pour envoir message d'enregistrement ou  de modification

        if ($this->departement_id) {
            $myDepartement = Departement::findOrFail($this->departement_id);
        }
        $myDepartement->Libelle = $this->libelle;
        $myDepartement->pay_id = $this->pay_id;
        $myDepartement->save();

        // vider les champs apres l'enregistrement ou la modification

        if ($this->dapartement_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }

        $this->resetInputFields();
        // mettre ajout

        $this->emit('storeDepartement');

        return redirect()->route('admin.departement-index');


    }
    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->departement_id = $id;
        $myDepartement = Departement::findOrFail($this->departement_id);
        $this->libelle = $myDepartement->Libelle;
        $this->pay_id = $myDepartement->pay_id;
    }
    // recuperation de l'element a supprimer

    public function deleteDepartement($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteDepartements()
    {
        $myDepartement = Departement::findOrFail($this->deleteIdBeingRemoved);
        $myDepartement->isDelete = 1;
        $myDepartement->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce departement à été supprimer']);
        return redirect()->route('admin.departement-index');

    }
    public function render()
    {
        $departements = Departement::where('isDelete', 0)->get();
        $pays = Pays::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.departement.departement-component',[
            "departements" => $departements,
            "pays" => $pays,
        ]);
    }
}
