<?php

namespace App\Http\Livewire\Dashboard\Prereservation\Demande;

use Livewire\Component;

class DemandeComponent extends Component
{
    public function render()
    {
        return view('livewire.dashboard.prereservation.demande.demande-component');
    }
}
