<?php

namespace App\Http\Livewire\Dashboard\Prereservation\TypePiece;

use App\Models\TypePiece;
use Livewire\Component;

class TypePieceComponent extends Component
{
    public $libelle;
    public $type_piece_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteTypePieces'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'type_piece_id',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->type_piece_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        }
    }
    public function storeCategorueProfessionnelle()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->type_piece_id) {
            $this->validate([
                'libelle' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
            ]);
        }

        $myTypePiece = new TypePiece();
        // verification du variable type_piece_id pour savoir si c'est un enregistrement ou  modification

        if ($this->type_piece_id) {
            $myTypePiece = TypePiece::findOrFail($this->type_piece_id);
        }
        $myTypePiece->Libelle = $this->libelle;
        $myTypePiece->save();

        // verification du variable type_piece_id pour envoir message d'enregistrement ou  de modification

        if ($this->type_piece_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeTypePiece');

        return redirect()->route('admin.typepiece-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->type_piece_id = $id;
        $myTypePiece = TypePiece::findOrFail($this->type_piece_id);
        $this->libelle = $myTypePiece->Libelle;
    }
    // recuperation de l'element a supprimer
    public function deleteCategorieProfessionnelle($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteCategorieProfessionnelles()
    {
        $myTypePiece = TypePiece::findOrFail($this->deleteIdBeingRemoved);
        $myTypePiece->isDelete = 1;
        $myTypePiece->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce type de piéce à été supprimer']);
        return redirect()->route('admin.typepiece-index');

    }
    public function render()
    {
        $typePieces = TypePiece::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.type-piece.type-piece-component',[
            "typePieces" => $typePieces,
        ]);
    }
}
