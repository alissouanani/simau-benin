<?php

namespace App\Http\Livewire\Dashboard\Prereservation\TypeLogement;

use App\Models\TypeLogement;
use Livewire\Component;

class TypeLogementComponent extends Component
{
    public $libelle;
    public $description;
    public $localite_id;
    public $typeLogement_id;



    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteTypeLogements'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'description',
        'typeLogement_id',
        'localite_id',
        ]);
    }

    public function updated($fields)
    {
    // fonction verification des variables lors de la modification

        if ($this->typeLogement_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'localite_id' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'localite_id' => 'required',
                'description' => 'required',
        ]);
        }
    }
    public function storeTypeLogement()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->typeLogement_id) {
            $this->validate([
                'libelle' => 'required',
                'localite_id' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
                'localite_id' => 'required',
                'description' => 'required',
            ]);
        }

        $myOptionLogement = new TypeLogement();
        // verification du variable typeLogement_id pour envoir message d'enregistrement ou  de modification

        if ($this->typeLogement_id) {
            $myOptionLogement = TypeLogement::findOrFail($this->typeLogement_id);
        }

        $myOptionLogement->libelle = $this->libelle;
        $myOptionLogement->description = $this->description;
        $myOptionLogement->localite_id = $this->localite_id;
        $myOptionLogement->save();

        // vider les champs apres l'enregistrement ou la modification

        if ($this->typeLogement_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }

        $this->resetInputFields();
        // mettre ajout

        $this->emit('storeTypeLogement');

        // return redirect()->route('admin.optionlogement-index');


    }
    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->typeLogement_id = $id;
        $myTypeLogement = TypeLogement::findOrFail($this->typeLogement_id);
        $this->libelle = $myTypeLogement->libelle;
        $this->description = $myTypeLogement->description;
        $this->localite_id = $myTypeLogement->localite_id;

    }
    // recuperation de l'element a supprimer

    public function deleteTypeLogement($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteTypeLogements()
    {
        $myTypeLogement = TypeLogement::findOrFail($this->deleteIdBeingRemoved);
        $myTypeLogement->isDelete = 1;
        $myTypeLogement->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce Type Logement à été supprimer']);
        // return redirect()->route('admin.optionlogement-index');

    }
    public function render()
    {
        $typeLogements = TypeLogement::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.type-logement.type-logement-component',[
            'typeLogements' => $typeLogements,
        ]);
    }
}
