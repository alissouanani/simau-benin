<?php

namespace App\Http\Livewire\Dashboard\Prereservation\Genre;

use App\Models\Genre;
use Livewire\Component;

class GenreComponent extends Component
{
    public $libelle;
    public $genre_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteGenres'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'genre_id',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->genre_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        }
    }
    public function storeGenre()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->genre_id) {
            $this->validate([
                'libelle' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
            ]);
        }

        $myGenre = new Genre();
        // verification du variable genre_id pour savoir si c'est un enregistrement ou  modification

        if ($this->genre_id) {
            $myGenre = Genre::findOrFail($this->genre_id);
        }
        $myGenre->Libelle = $this->libelle;
        $myGenre->save();

        // verification du variable genre_id pour envoir message d'enregistrement ou  de modification

        if ($this->genre_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeGenre');

        return redirect()->route('admin.genre-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->genre_id = $id;
        $myGenre = Genre::findOrFail($this->genre_id);
        $this->libelle = $myGenre->Libelle;
    }
    // recuperation de l'element a supprimer
    public function deleteGenre($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteGenres()
    {
        $myGenre = Genre::findOrFail($this->deleteIdBeingRemoved);
        $myGenre->isDelete = 1;
        $myGenre->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce genre à été supprimer']);
        return redirect()->route('admin.genre-index');

    }
    public function render()
    {
        $genres = Genre::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.genre.genre-component',[
            "genres" => $genres,
        ]);
    }
}
