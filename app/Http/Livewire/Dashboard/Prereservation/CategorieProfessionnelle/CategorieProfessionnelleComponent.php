<?php

namespace App\Http\Livewire\Dashboard\Prereservation\CategorieProfessionnelle;

use App\Models\CategorieProfessionnelle;
use Livewire\Component;

class CategorieProfessionnelleComponent extends Component
{
    public $libelle;
    public $Categorie_professionnelle_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteCategorieProfessionnelles'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'Categorie_professionnelle_id',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->Categorie_professionnelle_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        }
    }
    public function storeCategorueProfessionnelle()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->Categorie_professionnelle_id) {
            $this->validate([
                'libelle' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
            ]);
        }

        $myCategorieProfessionnelle = new CategorieProfessionnelle();
        // verification du variable Categorie_professionnelle_id pour savoir si c'est un enregistrement ou  modification

        if ($this->Categorie_professionnelle_id) {
            $myCategorieProfessionnelle = CategorieProfessionnelle::findOrFail($this->Categorie_professionnelle_id);
        }
        $myCategorieProfessionnelle->Libelle = $this->libelle;
        $myCategorieProfessionnelle->save();

        // verification du variable Categorie_professionnelle_id pour envoir message d'enregistrement ou  de modification

        if ($this->Categorie_professionnelle_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeCategorueProfessionnelle');

        return redirect()->route('admin.categorieprofessionnelle-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->Categorie_professionnelle_id = $id;
        $myCategorieProfessionnelle = CategorieProfessionnelle::findOrFail($this->Categorie_professionnelle_id);
        $this->libelle = $myCategorieProfessionnelle->Libelle;
    }
    // recuperation de l'element a supprimer
    public function deleteCategorieProfessionnelle($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteCategorieProfessionnelles()
    {
        $myCategorieProfessionnelle = CategorieProfessionnelle::findOrFail($this->deleteIdBeingRemoved);
        $myCategorieProfessionnelle->isDelete = 1;
        $myCategorieProfessionnelle->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Cette catégorie professionnelle à été supprimer']);
        return redirect()->route('admin.categorieprofessionnelle-index');
    }
    public function render()
    {
        $categorieProfessionnelles = CategorieProfessionnelle::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.categorie-professionnelle.categorie-professionnelle-component',[
            "categorieProfessionelles" => $categorieProfessionnelles,
        ]);
    }
}
