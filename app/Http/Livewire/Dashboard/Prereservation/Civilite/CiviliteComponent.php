<?php

namespace App\Http\Livewire\Dashboard\Prereservation\Civilite;

use Livewire\Component;
use App\Models\Civilite;

class CiviliteComponent extends Component
{
    public $libelle;
    public $civilite_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteCivilites'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'civilite_id',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->civilite_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        }
    }
    public function storeCivilite()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->civilite_id) {
            $this->validate([
                'libelle' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
            ]);
        }

        $myCivilite = new Civilite();
        // verification du variable myCivilite pour savoir si c'est un enregistrement ou  modification

        if ($this->civilite_id) {
            $myCivilite = Civilite::findOrFail($this->civilite_id);
        }
        $myCivilite->Libelle = $this->libelle;
        $myCivilite->save();

        // verification du variable myCivilite pour envoir message d'enregistrement ou  de modification

        if ($this->civilite_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeCivilite');

        return redirect()->route('admin.civilite-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->civilite_id = $id;
        $myCivilite = Civilite::findOrFail($this->civilite_id);
        $this->libelle = $myCivilite->Libelle;
    }
    // recuperation de l'element a supprimer
    public function deleteCivilite($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteCivilites()
    {
        $myCivilite = Civilite::findOrFail($this->deleteIdBeingRemoved);
        $myCivilite->isDelete = 1;
        $myCivilite->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Cette civilité à été supprimer']);
        return redirect()->route('admin.civilite-index');

    }
    public function render()
    {
        $civilites = Civilite::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.civilite.civilite-component',[
            "civilites" => $civilites,
        ]);
    }
}
