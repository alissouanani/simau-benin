<?php

namespace App\Http\Livewire\Dashboard\Prereservation\Ville;

use App\Models\Ville;
use Livewire\Component;
use App\Models\Departement;

class VilleComponent extends Component
{
    public $libelle;
    public $ville_id;
    public $departement_id;


    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteVilles'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'departement_id',
        ]);
    }

    public function updated($fields)
    {
    // fonction verification des variables lors de la modification

        if ($this->ville_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'departement_id' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'departement_id' => 'required',
            ]);
        }
    }
    public function storeVille()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->ville_id) {
            $this->validate([
                'libelle' => 'required',
                'departement_id' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
                'departement_id' => 'required',
            ]);
        }

        $myVille = new Ville();
        // verification du variable ville_id pour envoir message d'enregistrement ou  de modification

        if ($this->ville_id) {
            $myVille = Ville::findOrFail($this->ville_id);
        }
        $myVille->Libelle = $this->libelle;
        $myVille->departement_id = $this->departement_id;
        $myVille->save();

        // vider les champs apres l'enregistrement ou la modification

        if ($this->ville_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }

        $this->resetInputFields();
        // mettre ajout

        $this->emit('storeVille');

        return redirect()->route('admin.ville-index');


    }
    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->ville_id = $id;
        $myVille = Ville::findOrFail($this->ville_id);
        $this->libelle = $myVille->Libelle;
        $this->departement_id = $myVille->departement_id;
    }
    // recuperation de l'element a supprimer

    public function deleteVille($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteVilles()
    {
        $myVille = Ville::findOrFail($this->deleteIdBeingRemoved);
        $myVille->isDelete = 1;
        $myVille->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Cette ville à été supprimer']);
        return redirect()->route('admin.ville-index');

    }

    public function render()
    {
        $villes = Ville::where('isDelete', 0)->get();
        $departements = Departement::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.ville.ville-component',[
            "villes" => $villes,
            "departements" => $departements,
        ]);
    }
}
