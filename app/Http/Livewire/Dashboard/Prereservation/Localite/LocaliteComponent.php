<?php

namespace App\Http\Livewire\Dashboard\Prereservation\Localite;

use Livewire\Component;
use App\Models\Localite;

class LocaliteComponent extends Component
{
    public $libelle;
    public $localite_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteLocalites'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'localite_id',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->localite_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
            ]);
        }
    }
    public function storeLocalite()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->localite_id) {
            $this->validate([
                'libelle' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
            ]);
        }

        $myLocalite = new Localite();
        // verification du variable localite_id pour savoir si c'est un enregistrement ou  modification

        if ($this->localite_id) {
            $myLocalite = Localite::findOrFail($this->localite_id);
        }
        $myLocalite->libelle = $this->libelle;
        $myLocalite->save();

        // verification du variable localite_id pour envoir message d'enregistrement ou  de modification

        if ($this->localite_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeLocalite');

        return redirect()->route('admin.localite-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->localite_id = $id;
        $myLocalite = Localite::findOrFail($this->localite_id);
        $this->libelle = $myLocalite->libelle;
    }
    // recuperation de l'element a supprimer
    public function deleteLocalite($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteLocalites()
    {
        $myLocalite = Localite::findOrFail($this->deleteIdBeingRemoved);
        $myLocalite->isDelete = 1;
        $myLocalite->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Cette localité à été supprimer']);
        return redirect()->route('admin.localite-index');

    }
    public function render()
    {
        $localites = Localite::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.localite.localite-component',[
            "localites" => $localites,
        ]);
    }
}
