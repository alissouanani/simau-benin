<?php

namespace App\Http\Livewire\Dashboard\Prereservation\OptionLogement;

use App\Models\OptionLogement;
use App\Models\TypeLogement;
use Livewire\Component;
use PhpOption\Option;

class OptionLogementComponent extends Component
{
    public $libelle;
    public $option_logement_id;
    public $type_logement_id;


    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteOptionLogements'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'type_logement_id',
        ]);
    }

    public function updated($fields)
    {
    // fonction verification des variables lors de la modification

        if ($this->option_logement_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'type_logement_id' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'type_logement_id' => 'required',
            ]);
        }
    }
    public function storeOptionLogement()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->option_logement_id) {
            $this->validate([
                'libelle' => 'required',
                'type_logement_id' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
                'type_logement_id' => 'required',
            ]);
        }

        $myOptionLogement = new OptionLogement();
        // verification du variable option_logement_id pour envoir message d'enregistrement ou  de modification

        if ($this->option_logement_id) {
            $myOptionLogement = OptionLogement::findOrFail($this->option_logement_id);
        }
        $myOptionLogement->libelle = $this->libelle;
        $myOptionLogement->type_logement_id = $this->type_logement_id;
        $myOptionLogement->save();

        // vider les champs apres l'enregistrement ou la modification

        if ($this->option_logement_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }

        $this->resetInputFields();
        // mettre ajout

        $this->emit('storeOptionLogement');

        return redirect()->route('admin.optionlogement-index');


    }
    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->option_logement_id = $id;
        $myOptionLogement = OptionLogement::findOrFail($this->option_logement_id);
        $this->libelle = $myOptionLogement->libelle;
        $this->type_logement_id = $myOptionLogement->type_logement_id;
    }
    // recuperation de l'element a supprimer

    public function deleteOptionLogment($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteOptionLogements()
    {
        $myOptionLogement = OptionLogement::findOrFail($this->deleteIdBeingRemoved);
        $myOptionLogement->isDelete = 1;
        $myOptionLogement->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Cette option de logement à été supprimer']);
        return redirect()->route('admin.optionlogement-index');

    }

    public function render()
    {
        $optionLogements = OptionLogement::where('isDelete', 0)->get();
        $typeLogements = TypeLogement::where('isDelete', 0)->get();
        return view('livewire.dashboard.prereservation.option-logement.option-logement-component',[
            "optionLogements" => $optionLogements,
            "typeLogements" => $typeLogements,
        ]);
    }
}
