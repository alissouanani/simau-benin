<?php

namespace App\Http\Livewire\Dashboard\Parametre\Societes;

use App\Models\Societe;
use Livewire\Component;
use Livewire\WithPagination;

class SocietesComponent extends Component
{

        public $deleteIdBeingRemoved = null;
        protected $listeners = ['deleteConfirmation' => 'deleteSocietes'];
    // recuperation de l'element a supprimer
        public function deleteSociete($id)
        {
            $this->deleteIdBeingRemoved = $id;
            $this->dispatchBrowserEvent('show-delete-confirmation');
        }
    // suppression de l'élement recuperer

        public function deleteSocietes()
        {
            $mySociete = Societe::findOrFail($this->deleteIdBeingRemoved);
            $mySociete->isDelete = 1;
            $mySociete->save();
            $this->dispatchBrowserEvent('deleted',['message' => 'Cette société à été supprimer']);

        }
    public function render()
    {
        $societes = Societe::where('isDelete', 0)->get();
        return view('livewire.dashboard.parametre.societes.societes-component',[
            "societes" => $societes,
        ]);
    }
}
