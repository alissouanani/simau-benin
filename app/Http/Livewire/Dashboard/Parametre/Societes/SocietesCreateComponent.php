<?php

namespace App\Http\Livewire\Dashboard\Parametre\Societes;

use App\Models\Societe;
use Livewire\Component;
use Livewire\WithFileUploads;

class SocietesCreateComponent extends Component
{
    use WithFileUploads;
    public $logo;
    public $nom;
    public $adresse;
    public $description;
    public $telephone;
    public $image_centrale_actualite;
    public $image_centrale_apropos;
    public $image_centrale_recrutement;
    public $image_apropos;
    public $description_apropos;

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset(['logo', 'nom','adresse', 'telephone', 'description','image_centrale_actualite','image_centrale_apropos','image_centrale_recrutement','image_apropos','description_apropos']);

    }
    // Fonction de l'enregistrement

    public function storeSociete()
    {
        // verification des variables lors de la l'enregistrement

            $this->validate([
                'logo' =>  'required',
                'nom' =>  'required',
                'adresse' =>  'required',
                'telephone' =>  'required',
                'description' =>  'required',
                'image_centrale_actualite' =>  'required',
                'image_centrale_apropos' =>  'required',
                'image_centrale_recrutement' =>  'required',
                'image_apropos' =>  'required',
                'description_apropos' =>  'required',

            ]);

        $mySociete = new Societe();
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenamelogo = time() . '.' . $this->logo->extension();
        $pathlogo = $this->logo->storeAs(
            'Societelogo',
            $filenamelogo,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameimage_centrale_actualite = time() . '.' . $this->image_centrale_actualite->extension();
        $pathimage_centrale_actualite = $this->image_centrale_actualite->storeAs(
            'imagecentraleactualite',
            $filenameimage_centrale_actualite,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameimage_centrale_apropos = time() . '.' . $this->image_centrale_apropos->extension();
        $pathimage_centrale_apropos = $this->image_centrale_apropos->storeAs(
            'imagecentraleapropos',
            $filenameimage_centrale_apropos,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameimage_centrale_recrutement = time() . '.' . $this->image_centrale_recrutement->extension();
        $pathimage_centrale_recrutement = $this->image_centrale_recrutement->storeAs(
            'imagecentralerecrutement',
            $filenameimage_centrale_recrutement,
            'public'
        );
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameimage_apropos = time() . '.' . $this->image_apropos->extension();
        $pathimage_apropos = $this->image_apropos->storeAs(
            'imageapropos',
            $filenameimage_apropos,
            'public'
        );
        $mySociete->logo = $pathlogo;
        $mySociete->image_apropos = $pathimage_apropos;
        $mySociete->image_centrale_recrutement = $pathimage_centrale_recrutement;
        $mySociete->image_centrale_apropos = $pathimage_centrale_apropos;
        $mySociete->image_centrale_actualite = $pathimage_centrale_actualite;
        $mySociete->nom = $this->nom;
        $mySociete->adresse = $this->adresse;
        $mySociete->telephone = $this->telephone;
        $mySociete->description = $this->description;
        $mySociete->description_apropos = $this->description_apropos;

        $mySociete->save();

       session()->flash('message', 'Enregistrement effectué avec succès.');
       $this->resetInputFields();

    }
    public function render()
    {
        return view('livewire.dashboard.parametre.societes.societes-create-component');
    }
}
