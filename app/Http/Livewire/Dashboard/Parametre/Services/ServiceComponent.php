<?php

namespace App\Http\Livewire\Dashboard\Parametre\Services;

use App\Models\Service;
use Livewire\Component;
use Livewire\WithFileUploads;

class ServiceComponent extends Component
{
    use WithFileUploads;
    public $libelle;
    public $icon;
    public $description;
    public $service_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteServices'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'libelle',
        'service_id',
        'icon',
        'description',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->service_id) {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'icon' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validateOnly($fields, [
                'libelle' => 'required',
                'icon' => 'required',
                'description' => 'required',
            ]);
        }
    }
    public function storeService()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->service_id) {
            $this->validate([
                'libelle' => 'required',
                'icon' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validate([
                'libelle' => 'required',
                'icon' => 'required',
                'description' => 'required',
            ]);
        }

        $myService = new Service();
        // verification du variable service_id pour savoir si c'est un enregistrement ou  modification

        if ($this->service_id) {
            $myService = Service::findOrFail($this->service_id);
        }
        // Modification et Stockage de l'image dans le dossier storage de public

        $filenameicon = time() . '.' . $this->icon->extension();
        $pathicon = $this->icon->storeAs(
            'IconService',
            $filenameicon,
            'public'
        );
        $myService->icon = $pathicon;
        $myService->Libelle = $this->libelle;
        $myService->description = $this->description;

        $myService->save();

        // verification du variable service_id pour envoir message d'enregistrement ou  de modification

        if ($this->service_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeService');

        return redirect()->route('admin.service-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->service_id = $id;
        $myService = Service::findOrFail($this->service_id);
        $this->libelle = $myService->Libelle;
        $this->icon = $myService->icon;
        $this->description = $myService->description;

    }
    // recuperation de l'element a supprimer
    public function deleteService($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteServices()
    {
        $myService = Service::findOrFail($this->deleteIdBeingRemoved);
        $myService->isDelete = 1;
        $myService->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce service à été supprimer']);
        return redirect()->route('admin.service-index');

    }
    public function render()
    {
        $services = Service::where('isDelete', 0)->get();
        return view('livewire.dashboard.parametre.services.service-component',[
            "services" => $services,
        ]);
    }
}
