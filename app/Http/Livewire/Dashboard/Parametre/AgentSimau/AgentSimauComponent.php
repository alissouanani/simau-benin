<?php

namespace App\Http\Livewire\Dashboard\Parametre\AgentSimau;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Hash;

class AgentSimauComponent extends Component
{
    public $name;
    public $email;
    public $password = 123456789;
    public $agent_id;


    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteAgents'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'name',
        'email',
        'password',
        'agent_id',

        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->agent_id) {
            $this->validateOnly($fields, [
                'name' => 'required',
                'email' => 'required',
            ]);
        } else {
            $this->validateOnly($fields, [
                'name' => 'required',
                'email' => 'required',
            ]);
        }
    }
    public function storeAgent()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->agent_id) {
            $this->validate([
                'name' => 'required',
                'email' => 'required',

            ]);
        } else {
            $this->validate([
                'name' => 'required',
                'email' => 'required',
            ]);
        }

        $myAgent = new User();
        // verification du variable agent_id pour savoir si c'est un enregistrement ou  modification

        if ($this->agent_id) {
            $myAgent = User::findOrFail($this->agent_id);
        }

        $myAgent->name = $this->name;
        $myAgent->email = $this->email;
        $myAgent->password = Hash::make($this->password);

        $myAgent->save();

        // verification du variable agent_id pour envoir message d'enregistrement ou  de modification

        if ($this->agent_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeAgent');

        return redirect()->route('admin.agent-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->agent_id = $id;
        $myAgent = User::findOrFail($this->agent_id);
        $this->name = $myAgent->name;
        $this->email = $myAgent->email;
    }
    // recuperation de l'element a supprimer
    public function deleteAgent($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteAgents()
    {
        $myService = User::findOrFail($this->deleteIdBeingRemoved);
        $myService->isDelete = 1;
        $myService->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce agent à été supprimer']);
        return redirect()->route('admin.agent-index');

    }
    public function render()
    {
        $agents = User::where('isDelete', 0)->get();
        return view('livewire.dashboard.parametre.agent-simau.agent-simau-component',[
            'agents' => $agents,
        ]);
    }
}
