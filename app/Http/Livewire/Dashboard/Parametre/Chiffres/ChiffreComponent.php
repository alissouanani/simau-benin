<?php

namespace App\Http\Livewire\Dashboard\Parametre\Chiffres;

use App\Models\Chiffre;
use Livewire\Component;

class ChiffreComponent extends Component
{
    public $chiffre;
    public $description;
    public $chiffre_id;

    public $deleteIdBeingRemoved = null;
    protected $listeners = ['deleteConfirmation' => 'deleteServices'];

    public function resetInputFields()
    {
        // Clean errors if were visible before
        $this->resetErrorBag();
        $this->resetValidation();
        $this->reset([
        'chiffre',
        'chiffre_id',
        'description',
        ]);
    }
    // fonction verification des variables lors de la modification
    public function updated($fields)
    {
        if ($this->chiffre_id) {
            $this->validateOnly($fields, [
                'chiffre' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validateOnly($fields, [
                'chiffre' => 'required',
                'description' => 'required',
            ]);
        }
    }
    public function storeChiffre()
    {
        // verification des variables lors de la l'enregistrement

        if ($this->service_id) {
            $this->validate([
                'chiffre' => 'required',
                'description' => 'required',

            ]);
        } else {
            $this->validate([
                'chiffre' => 'required',
                'description' => 'required',
            ]);
        }

        $myService = new Chiffre();
        // verification du variable chiffre_id pour savoir si c'est un enregistrement ou  modification

        if ($this->chiffre_id) {
            $myService = Chiffre::findOrFail($this->chiffre_id);
        }

        $myService->chiffre = $this->chiffre;
        $myService->description = $this->description;

        $myService->save();

        // verification du variable chiffre_id pour envoir message d'enregistrement ou  de modification

        if ($this->chiffre_id) {
            session()->flash('message', 'Modification effectuée avec succès.');
        } else {

            session()->flash('message', 'Enregistrement effectué avec succès.');
        }
        // vider les champs apres l'enregistrement ou la modification
        $this->resetInputFields();
        // mettre ajout
        $this->emit('storeChiffre');

        return redirect()->route('admin.chiffre-index');
    }

    // recuperation de l'element a modifier
    public function getElementById($id)
    {
        $this->chiffre_id = $id;
        $myService = Chiffre::findOrFail($this->chiffre_id);
        $this->chiffre = $myService->chiffre;
        $this->description = $myService->description;

    }
    // recuperation de l'element a supprimer
    public function deleteChiffre($id)
    {
        $this->deleteIdBeingRemoved = $id;
        $this->dispatchBrowserEvent('show-delete-confirmation');
    }
    // suppression de l'élement recuperer
    public function deleteChiffres()
    {
        $myService = Chiffre::findOrFail($this->deleteIdBeingRemoved);
        $myService->isDelete = 1;
        $myService->save();
        $this->dispatchBrowserEvent('deleted', ['message' => 'Ce chiffre à été supprimer']);
        return redirect()->route('admin.chiffre-index');

    }
    public function render()
    {
        $chiffres = Chiffre::where('isDelete', 0)->get();
        return view('livewire.dashboard.parametre.chiffres.chiffre-component',[
            "chiffres" => $chiffres,
        ]);
    }
}
